package com.emts.ugbattle;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class UserTicketBalanceUpdate {
    public static void getTicketBalanceTask(final Context context) {
        final PreferenceHelper prefsHelper = PreferenceHelper.getInstance(context);
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        volleyHelper.addVolleyRequestListeners(Api.getInstance().getUserDetailUrl, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject userInfo = res.getJSONObject("user_info");
                                String ticketBalance = userInfo.getString("ticket_balance");
                                if (!TextUtils.isEmpty(ticketBalance)) {
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, ticketBalance).apply();
                                }
                                context.sendBroadcast(new Intent(MainActivity.BALANCE_UPDATE_ACTION));
                                Logger.e("sending Broadcast ***************", "\n BoroadCast Sent !!!!!!!!!!!!!!");
                            }
                        } catch (JSONException e) {
                            Logger.e("getTicketBalanceTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getTicketBalanceTask error res", e.getMessage() + " ");
                        }
                    }
                }, "getTicketBalanceTask");

    }
}
