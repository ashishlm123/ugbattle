package com.emts.ugbattle.customviews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.emts.ugbattle.helper.Utils;

/**
 * Created by User on 2017-12-26.
 */

public class AtMostRecyclerView extends RecyclerView {
    Context context;

    public AtMostRecyclerView(Context context) {
        super(context);
        this.context = context;
    }

    public AtMostRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public AtMostRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        heightSpec = MeasureSpec.makeMeasureSpec((int) Utils.pxFromDp(context, 280), MeasureSpec.AT_MOST);
        super.onMeasure(widthSpec, heightSpec);
    }
}
