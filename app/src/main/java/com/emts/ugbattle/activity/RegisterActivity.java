package com.emts.ugbattle.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.MainActivity;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Srijana on 5/30/2017.
 */

public class RegisterActivity extends AppCompatActivity {
    EditText etUserName, etPassword, etEmail, etConPassword;
    Button btnCreateAc;
    TextView errorName, errorEmail, errorPassword, errorConPass;
    PreferenceHelper prefsHelper;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.setApplicationId(getString(R.string.facebook_app_id));

        setContentView(R.layout.activity_register);

        prefsHelper = PreferenceHelper.getInstance(this);

        etUserName = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        etEmail = findViewById(R.id.et_email);
        etConPassword = findViewById(R.id.et_con_pass);
        btnCreateAc = findViewById(R.id.btn_create_ac);
        errorName = findViewById(R.id.valid_username);
        errorPassword = findViewById(R.id.valid_pass);
        errorEmail = findViewById(R.id.valid_email);
        errorConPass = findViewById(R.id.valid_con_pass);

        btnCreateAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;

                if (TextUtils.isEmpty(etUserName.getText().toString().trim())) {
                    errorName.setText(R.string.error_text);
                    errorName.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorName.setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    isValid = false;
                    errorEmail.setText(R.string.error_text);
                    errorEmail.setVisibility(View.VISIBLE);
                } else {
                    if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
                        isValid = false;
                        errorEmail.setText(R.string.enter_valid_email);
                        errorEmail.setVisibility(View.VISIBLE);
                    } else {
                        errorEmail.setVisibility(View.GONE);
                    }
                }

                if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                    isValid = false;
                    errorPassword.setText(R.string.error_text);
                    errorPassword.setVisibility(View.VISIBLE);
                } else {
                    if (etPassword.getText().toString().trim().length() < 6
                            || etPassword.getText().toString().trim().length() > 12) {
                        isValid = false;
                        errorPassword.setText("The password length must be between 6 to 12 characters.");
                        errorPassword.setVisibility(View.VISIBLE);
                    } else {
                        if (!containAlphanumeric(etPassword.getText().toString().trim())) {
                            isValid = false;
                            errorPassword.setText("The password must contains alphabet and number.");
                            errorPassword.setVisibility(View.VISIBLE);
                        } else {
                            errorPassword.setVisibility(View.GONE);
                        }
                    }
                }
                if (TextUtils.isEmpty(etConPassword.getText().toString().trim())) {
                    isValid = false;
                    errorConPass.setText(R.string.error_text);
                    errorConPass.setVisibility(View.VISIBLE);
                } else {
                    if (!etConPassword.getText().toString().trim().equalsIgnoreCase(etPassword.getText().toString().trim())) {
                        isValid = false;
                        errorConPass.setText(R.string.error_same_con);
                        errorConPass.setVisibility(View.VISIBLE);
                    } else {
                        errorConPass.setVisibility(View.GONE);
                    }
                }

                if (isValid) {
                    if (NetworkUtils.isInNetwork(RegisterActivity.this)) {
                        userSignUpTask();
                    } else {
                        AlertUtils.showSnack(getApplicationContext(), btnCreateAc,
                                getResources().getString(R.string.error_no_internet));
                    }
                } else {
                    AlertUtils.showSnack(RegisterActivity.this, etEmail,
                            getString(R.string.form_validate_message));
                }
            }
        });

        LinearLayout btnFbRegister = findViewById(R.id.btn_registerfb);
        btnFbRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(RegisterActivity.this)) {
                    fbSignIN();
                } else {
                    AlertUtils.showSnack(RegisterActivity.this, view, getString(R.string.error_no_internet));
                }
            }
        });
    }

    private void userSignUpTask() {
        AlertUtils.hideInputMethod(RegisterActivity.this, etUserName);
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(RegisterActivity.this, "Please Wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(RegisterActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("username", etUserName.getText().toString().trim());
        postMap.put("email", etEmail.getText().toString().trim());
        postMap.put("password", etPassword.getText().toString().trim());
        postMap.put("password_confirm", etConPassword.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().registerUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                pDialog.dismiss();
                                AlertUtils.showAlertMessage(RegisterActivity.this, "Register Successful",
                                        res.getString("message") + " Go to login page?", "OK",
                                        "Cancel", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    startActivity(intent);
                                                }
                                            }
                                        });
                            } else {
                                pDialog.dismiss();
                                final String errorMsg = res.getString("message");
                                AlertUtils.showAlertMessage(RegisterActivity.this, "Register Error!!!",
                                        String.valueOf(Html.fromHtml(errorMsg)));
                            }
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            Logger.e("userSignUpTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userSignUpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(RegisterActivity.this, btnCreateAc,
                                errorMsg);
                    }
                }, "userSignUpTask");
    }

    private void fbSignIN() {
        LoginManager.getInstance().logOut();

        LoginManager.getInstance().logInWithReadPermissions(RegisterActivity.this, (Arrays.asList("email",
                "public_profile")));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Logger.e("facebook login response", loginResult.toString() + " &&\n "
                                + loginResult.getAccessToken() + "\n" + loginResult.getRecentlyGrantedPermissions()
                                + "\n" + loginResult.getRecentlyDeniedPermissions() + "\n");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code
                                        try {
                                            String id = object.optString("id");
                                            String name = object.optString("name");
                                            String first_name = object.optString("first_name");
                                            String last_name = object.optString("last_name");
                                            String age = object.optString("age_range");
                                            String picture = object.optString("picture");
                                            String email = object.optString("email"); //Get null value here
                                            String gender = object.optString("gender");

                                            Logger.e("Facebook Data", id + "\n" + name + "\n" + first_name
                                                    + "\n" + last_name + "\n" + email + "\n" + age + "\n" + picture + "\n"
                                                    + gender);
                                            LoginManager.getInstance().logOut();

                                            registerWithFbTask(id, first_name, last_name, email, gender, false);

                                        } catch (Exception e) {
                                            Logger.e("facebook exception", e.getMessage());
                                            LoginManager.getInstance().logOut();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name,last_name,email,gender," +
                                "age_range");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Logger.e("fbSignIn cancel", "called");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Logger.e("fbSignIn error", exception.getMessage() + "\n" + exception.getCause());
                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void registerWithFbTask(final String fbId, final String fbFirst_name, final String lastName,
                                    final String fbEmail, final String gender, final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("social_id", fbId);
        postParams.put("reg_type", "facebook");
        postParams.put("email", fbEmail);
        postParams.put("first_name", fbFirst_name);
        postParams.put("last_name", lastName);
        if (gender != null && gender.length() > 1) {
            postParams.put("gender", gender.substring(0, 1).toUpperCase());
        }

//        String pushToken = FirebaseInstanceId.getInstance().getToken();
//        if (!TextUtils.isEmpty(pushToken)) {
//            postParams.put("push_id", pushToken);
//        }

        AlertUtils.hideInputMethod(RegisterActivity.this, etUserName);

        vHelper.addVolleyRequestListeners(Api.getInstance().socialLoginUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                loginToApp(res);
                            } else {
                                AlertUtils.showAlertMessage(RegisterActivity.this,
                                        getString(R.string.title_error), res.getString("message"));
                            }

                        } catch (JSONException e) {
                            Logger.e("loginTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userSignUpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(RegisterActivity.this, btnCreateAc,
                                errorMsg);
                    }
                }, "registerWithFbTask");
    }


    private void loginToApp(JSONObject resObj) throws JSONException {
        final JSONObject dataObj = resObj.getJSONObject("user_info");
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_ID, dataObj.getString("id")).commit();
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, dataObj.getString("username")).commit();
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, dataObj.getString("email")).commit();
        prefsHelper.edit().putString(PreferenceHelper.TOKEN, dataObj.getString("user_token")).commit();
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_IMG, dataObj.getString("profile_img")).commit();
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, dataObj.getString("ticket_balance")).commit();
        prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("userSignUpTask");
        VolleyHelper.getInstance(this).cancelRequest("registerWithFbTask");
        super.onDestroy();
    }

    //checking if the password is alphanumeric or not
    public boolean containAlphanumeric(final String str) {
        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=()!?<>';:|`~*/]).{5,13}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z]).{5,13}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }
}
