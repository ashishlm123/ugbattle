package com.emts.ugbattle.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 5/30/2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity {
    EditText etUsername;
    TextView errorMesg;
    Button btnForgotPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView tvToolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        tvToolbarTitle.setText("Forgot Password");

        etUsername = findViewById(R.id.et_username_email);
        errorMesg = findViewById(R.id.valid_mesg);
        btnForgotPassword = findViewById(R.id.btn_forgot_Pass);
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etUsername.getText().toString().trim())) {
                    errorMesg.setText(R.string.error_text);
                    errorMesg.setVisibility(View.VISIBLE);
                } else {
                    errorMesg.setVisibility(View.GONE);
                    AlertUtils.hideInputMethod(ForgotPasswordActivity.this, view);
                    forgetPasswordTask();
                }
            }
        });
    }

    private void forgetPasswordTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(ForgotPasswordActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ForgotPasswordActivity.this, "Please Wait...");
        pDialog.show();

        postMap.put("email", etUsername.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().forgetPasswordUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                etUsername.setText("");
                                final String successMsg = res.getString("message");
                                AlertUtils.showAlertMessage(ForgotPasswordActivity.this, "Success",
                                        String.valueOf(Html.fromHtml(successMsg)), "OK",
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                    onBackPressed();
                                                }
                                            }
                                        });
                            } else {
                                final String successMsg = res.getString("message");
                                AlertUtils.showAlertMessage(ForgotPasswordActivity.this, "Error",
                                        String.valueOf(Html.fromHtml(successMsg)), "OK",
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                }
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            Logger.e("forgetPasswordTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("forgetPasswordTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(ForgotPasswordActivity.this, etUsername,
                                errorMsg);
                    }
                }, "forgetPasswordTask");
    }

    @Override
    protected void onPause() {
        VolleyHelper.getInstance(this).cancelRequest("forgetPasswordTask");
        super.onPause();
    }
}
