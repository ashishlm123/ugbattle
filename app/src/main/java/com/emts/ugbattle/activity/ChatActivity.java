package com.emts.ugbattle.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.ChatAdapter;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.ChatModel;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.UserModel;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

public class ChatActivity extends AppCompatActivity {
    public static final String chattingMatchId = "chatMatchId";
    ChatAdapter chatAdapter;
    RecyclerView recyclerChat;
    LinearLayoutManager layoutManager;
    ArrayList<ChatModel> chatList;
    Toolbar toolbar;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefresh;
    TextView errorText;
    UserModel friendMdl;
    TeamModel teamModel;
    String jsonResponse = "";
    String matchId;
    String attachmentMessage = "";
    EditText etChat;

    boolean isGroupChat = false;
    boolean isSwiped = false;
    FilePickerDialog dialog;
    boolean fromPush = false;

    int limit = 40;

    PreferenceHelper prefHelper;
    Socket socketJs;
    private static final String EVENT_SEND_GROUP_MESSAGE = "send_player_message";
    private static final String EVENT_JOIN_GROUP_CHAT_ROOM = "connect_to_player_chat_room";
    private static final String EVENT_DISCONNECT_ROOM = "manual_disconnect";
    private static final String EVENT_LISTEN_GROUP_MESSAGE_INCOMING = "receive_player_message";
    private static final String EVENT_LISTEN_CONNECTION = "connect";
    private static final String EVENT_LISTEN_DISCONNECTION_FROM_PLAYER_CHAT = "you_are_disconnecting_from_player_chat_room";
    private static final String EVENT_LISTEN_DISCONNECTION_FROM_INDIVIDUAL_CHAT = "you_are_disconnecting_from_individual_message";
    private static final String EVENT_LISTEN_DISCONNECTION_FROM_TOURNAMENT_VIEWER = "you_are_disconnecting_from_tournament_viewer_listner";

    private static final String EVENT_JOIN_INDIVIDUAL_CHAT_ROOM = "connect_to_individual_chat_room";
    private static final String EVENT_SEND_INDIVIDUAL_MESSAGE = "send_individual_message";
    private static final String EVENT_LISTEN_INDIVIDUAL_MESSAGE = "receive_individual_message";

    String fileBaseUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);

        Intent intent = getIntent();
        if (intent != null) {
            friendMdl = (UserModel) intent.getSerializableExtra("friend");
            isGroupChat = friendMdl == null;
            jsonResponse = intent.getStringExtra("response");
            fromPush = intent.getBooleanExtra("fromPush", false);
            if (fromPush) {
                String notificationId = intent.getStringExtra("noti_id");
                markReadTask(notificationId);
            }
        } else {
            return;
        }

        prefHelper = PreferenceHelper.getInstance(this);

        toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Chat Room");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.e("toolbar navigation click", "clicked !!!!");
                onBackPressed();
            }
        });

        progressBar = findViewById(R.id.progress);
        errorText = findViewById(R.id.errorText);

        chatList = new ArrayList<>();

        swipeRefresh = findViewById(R.id.swipe_refresh);
        recyclerChat = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        recyclerChat.setLayoutManager(layoutManager);
        recyclerChat.setNestedScrollingEnabled(false);
        chatAdapter = new ChatAdapter(ChatActivity.this, chatList);
        chatAdapter.setGroupChat(isGroupChat);
        recyclerChat.setAdapter(chatAdapter);
        recyclerChat.setItemAnimator(null);

        if (isGroupChat) {
            try {
                teamModel = new TeamModel();

                JSONObject res = new JSONObject(jsonResponse);
                JSONObject matchInfo = res.getJSONObject("match_info");
                matchId = matchInfo.getString("match_id");
                teamModel.setGameId(matchInfo.getString("game_id"));
                teamModel.setTrnmntId(matchInfo.getString("tourn_id"));

                JSONObject myTeam = matchInfo.getJSONObject("my_team");
                teamModel.setMyTeamId(myTeam.getString("team_id"));
                teamModel.setMyTeamName(myTeam.getString("team_name"));

                teamModel.setCurrentRoundNo(Integer.parseInt(myTeam.getString("current_round_no")));

                JSONObject opponentTeam = matchInfo.getJSONObject("opponent_team");
                teamModel.setOpponentTeamId(opponentTeam.getString("team_id"));
                teamModel.setOpponentTeamName(opponentTeam.getString("team_name"));
            } catch (JSONException e) {
                Logger.e("team info json ex", e.getMessage());
            }

            if (NetworkUtils.isInNetwork(this)) {
                groupChatListingTask("0");
            } else {
                recyclerChat.setVisibility(View.GONE);
                errorText.setText("No Internet Connected !!!");
                errorText.setVisibility(View.VISIBLE);
            }
        } else {
            if (NetworkUtils.isInNetwork(ChatActivity.this)) {
                friendChatListingTask("0");
            } else {
                recyclerChat.setVisibility(View.GONE);
                errorText.setText("No Internet Connected !!!");
                errorText.setVisibility(View.VISIBLE);
            }
        }

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (chatList.size() > 0) {
                    isSwiped = true;
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        if (isGroupChat) {
                            groupChatListingTask(chatList.get(0).getMessageId());
                        } else {
                            friendChatListingTask(chatList.get(0).getMessageId());
                        }
                        Logger.e("top message id ", String.valueOf(chatList.get(0).getMessageId()));
                    } else {
                        AlertUtils.showSnack(ChatActivity.this, toolbar, getResources().getString(R.string.error_no_internet));
                        errorText.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        final ImageView btnSendMsg = findViewById(R.id.btn_send_message);
        etChat = findViewById(R.id.edt_enter_message);
        etChat.setEnabled(false);
        ImageView btnAttachFile = findViewById(R.id.btn_attach_file);
        btnAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
        ImageView btnEmoji = findViewById(R.id.btn_emoji);
        btnEmoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etChat.requestFocus();
                AlertUtils.showInputMethod(ChatActivity.this, swipeRefresh);
            }
        });
        etChat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence)) {
                    btnSendMsg.setColorFilter(getResources().getColor(R.color.chatTint));
                    btnSendMsg.setEnabled(false);
                } else {
                    btnSendMsg.setColorFilter(getResources().getColor(R.color.mainButtonColor));
                    btnSendMsg.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(etChat.getText().toString().trim())) {
                    ChatModel sendChat = new ChatModel();

                    Logger.e("chatFrag emoji", etChat.getText().toString().trim() + "   --");
                    String toServerUnicodeEncoded = StringEscapeUtils.escapeJava(etChat.getText().toString().trim());
                    Logger.e("chatFrag escapedString ", toServerUnicodeEncoded + "  --");
                    String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(toServerUnicodeEncoded);
                    Logger.e("chatFrag unescapedString", fromServerUnicodeDecoded + "  --");

                    sendChat.setMessagePreview(toServerUnicodeEncoded);
                    sendChat.setMessageTime("now");
                    sendChat.setUserId(prefHelper.getUserId());
                    sendChat.setUserName(prefHelper.getString(PreferenceHelper.APP_USER_NAME, ""));
                    sendChat.setMsgStatus(ChatModel.MESSAGE_SENDING);
                    if (isGroupChat) {
                        sendChat.setSenderTeamId(teamModel.getMyTeamId());
                        sendChat.setMyTeamId(teamModel.getMyTeamId());
                    } else {
                        sendChat.setSenderTeamId(prefHelper.getUserId());
                        sendChat.setMyTeamId(prefHelper.getUserId());
                    }
                    sendChat.setMessageType(ChatModel.MESSAGE_TYPE_TEXT);

//                    chatList.add(0, sendChat);
                    chatList.add(sendChat);
                    chatAdapter.notifyItemInserted(chatList.size() - 1);
//                    chatAdapter.notifyItemInserted(0);
                    recyclerChat.smoothScrollToPosition(chatList.size() - 1);
//                    recyclerChat.smoothScrollToPosition(0);
                    recyclerChat.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);

                    etChat.setText("");

                    //emit message
//                    emitOutgoingGroupMessage(sendChat.getMessagePreview(), ChatModel.MESSAGE_TYPE_TEXT);
                    if (isGroupChat) {
                        sendGroupMessage(sendChat.getMessagePreview(), attachmentMessage, ChatModel.MESSAGE_TYPE_TEXT, chatList.size() - 1);
                    } else {
                        sendIndividualMessage(sendChat.getMessagePreview(), attachmentMessage, ChatModel.MESSAGE_TYPE_TEXT, chatList.size() - 1);
                    }
                }
            }
        });

        initFilePickAlert();
    }

    private void initFilePickAlert() {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;

//        dialog = new FilePickerDialog(ChatActivity.this, properties, R.style.AlertDialogTheme_PickFileTheme);
        dialog = new FilePickerDialog(ChatActivity.this, properties);
        dialog.setTitle("Select a File");

        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                attachmentMessage = files[0];
                if (!TextUtils.isEmpty(attachmentMessage)) {
                    ChatModel sendChat = new ChatModel();
                    sendChat.setMessagePreview("");
                    sendChat.setMessageTime("now");
                    sendChat.setUserId(prefHelper.getUserId());
                    sendChat.setUserName(prefHelper.getString(PreferenceHelper.APP_USER_NAME, ""));
                    sendChat.setMsgStatus(ChatModel.MESSAGE_SENDING);
                    if (isGroupChat) {
                        sendChat.setSenderTeamId(teamModel.getMyTeamId());
                        sendChat.setMyTeamId(teamModel.getMyTeamId());
                    } else {
                        sendChat.setSenderTeamId(prefHelper.getUserId());
                    }
                    sendChat.setMessageType(ChatModel.MESSAGE_TYPE_ATTACHMENT);

                    chatList.add(sendChat);
                    chatAdapter.notifyItemInserted(chatList.size() - 1);
                    recyclerChat.smoothScrollToPosition(chatList.size() - 1);
                    recyclerChat.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);

                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        if (isGroupChat) {
                            sendGroupMessage("", attachmentMessage, ChatModel.MESSAGE_TYPE_ATTACHMENT, chatList.size() - 1);
                        } else {
                            sendIndividualMessage("", attachmentMessage, ChatModel.MESSAGE_TYPE_ATTACHMENT, chatList.size() - 1);
                        }
                    }
                }
            }
        });
    }

    private void sendIndividualMessage(final String message, final String attachmentMessage, String messageType, final int pos) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        HashMap<String, String> filePart = new HashMap<>();

        postParams.put("friend_user_id", friendMdl.getUserId());
        postParams.put("message_type", messageType);

        if (!TextUtils.isEmpty(message)) {
            postParams.put("message", message);
        } else {
            filePart.put("message_file", attachmentMessage);
        }

        vHelper.addMultipartRequest(Api.getInstance().sendIndividualChat, Request.Method.POST, postParams,
                filePart, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                chatList.get(pos).setMsgStatus(ChatModel.MESSAGE_SENT);
                                JSONObject msgObj = res.getJSONObject("sent_message");

                                chatList.get(pos).setMessageId(msgObj.getString("id"));
                                chatList.get(pos).setMessageTime(msgObj.getString("send_date"));
                                chatAdapter.notifyItemChanged(pos);

                                emitOutGoingIndividualMessage(response);
                            } else {
                                chatList.get(pos).setMsgStatus(ChatModel.MESSAGE_FAILED);
                                chatAdapter.notifyItemChanged(pos);
                            }
                        } catch (JSONException e) {
                            Logger.e("sendIndividualMessage json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("sendIndividualMessage error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(ChatActivity.this, toolbar,
                                errorMsg);
                    }
                }, "sendIndividualMessage");

    }

    private void sendGroupMessage(final String message, final String attachmentMessage, String messageType, final int pos) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        HashMap<String, String> filePart = new HashMap<>();

        postParams.put("match_id", matchId);
        postParams.put("team_id", teamModel.getMyTeamId());
        postParams.put("message_type", messageType);

        if (!TextUtils.isEmpty(message)) {
            postParams.put("message", message);
        } else {
            filePart.put("message_file", attachmentMessage);
        }
        postParams.put("to_team_id", teamModel.getOpponentTeamId());
//        from_team_id  (compulsary)
//        message_file (compulsary) if message type is file"

        vHelper.addMultipartRequest(Api.getInstance().sendMessage, Request.Method.POST, postParams,
                filePart, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                chatList.get(pos).setMsgStatus(ChatModel.MESSAGE_SENT);
                                JSONObject msgObj = res.getJSONObject("message_data");

                                chatList.get(pos).setMessageId(msgObj.getString("id"));
                                chatList.get(pos).setMessageTime(msgObj.getString("send_date"));
                                chatAdapter.notifyItemChanged(pos);

                                emitOutgoingGroupMessage(response);
                            } else {
                                chatList.get(pos).setMsgStatus(ChatModel.MESSAGE_FAILED);
                                chatAdapter.notifyItemChanged(pos);
                            }
                        } catch (JSONException e) {
                            Logger.e("sendGroupMessage json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("sendGroupMessage error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(ChatActivity.this, toolbar,
                                errorMsg);
                    }
                }, "sendGroupMessage");

    }

    private void friendChatListingTask(final String lastMsgId) {
        if (Integer.parseInt(lastMsgId) == 0) {
            progressBar.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);

        if (isSwiped) {
            recyclerChat.setVisibility(View.VISIBLE);
        } else {
            recyclerChat.setVisibility(View.GONE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        postParams.put("friend_user_id", friendMdl.getUserId());
        if (!lastMsgId.equals("0")) {
            postParams.put("message_smaller_than_id", lastMsgId);
        }
        postParams.put("limit", String.valueOf(limit));

        vHelper.addVolleyRequestListeners(Api.getInstance().getIndividualChat, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        fileBaseUrl = res.getString("message_file_base_url");
                        JSONArray chatArray = res.getJSONArray("individual_messages");
                        if (chatArray.length() == 0) {
                            if (!lastMsgId.equals("0")) {
//                                AlertUtils.showSnack(ChatActivity.this, toolbar, res.getString("message"));
                            }
                        } else {
                            ArrayList<ChatModel> tempMesgLists = new ArrayList<>();
                            for (int i = 0; i < chatArray.length(); i++) {
                                JSONObject eachChat = chatArray.getJSONObject(i);
                                ChatModel chatModel = new ChatModel();
                                chatModel.setMessageId(eachChat.getString("id"));
                                chatModel.setSenderTeamId(eachChat.getString("from_user_id"));
                                chatModel.setMyTeamId(prefHelper.getUserId());
                                String userName = eachChat.getString("username");

                                if (userName.equalsIgnoreCase("null") || TextUtils.isEmpty(userName)) {
                                    chatModel.setUserName(eachChat.getString("full_name"));
                                } else {
                                    chatModel.setUserName(userName);
                                }

                                String messageType = eachChat.getString("message_type");
                                chatModel.setMessageType(messageType);

                                String message = eachChat.getString("message");
                                chatModel.setMessagePreview(message);

                                if (messageType.equals(ChatModel.MESSAGE_TYPE_ATTACHMENT) && !TextUtils.isEmpty(message)) {
                                    chatModel.setMessagePreview(fileBaseUrl + message);
                                }

                                chatModel.setMessageTime(eachChat.getString("send_date"));
                                chatModel.setMsgStatus(ChatModel.MESSAGE_SENT);
                                tempMesgLists.add(chatModel);
                            }
                            if (lastMsgId.equals("0")) {
                                chatList.addAll(tempMesgLists);
                            } else {
//                                        Collections.reverse(tempMesgLists);
                                chatList.addAll(0, tempMesgLists);
                            }
                        }
                        if (chatList.size() < 1) {
                            if (lastMsgId.equals("0")) {
                                recyclerChat.setVisibility(View.GONE);
                                errorText.setText("Start a conversation");
                                errorText.setVisibility(View.VISIBLE);
                            } else {
//                                AlertUtils.showSnack(ChatActivity.this, recyclerChat, "No more messages");
                            }
                        } else {
                            chatAdapter.notifyDataSetChanged();
                            recyclerChat.setVisibility(View.VISIBLE);
                        }

                    } else {
                        if (lastMsgId.equals("0")) {
                            recyclerChat.setVisibility(View.GONE);
                            errorText.setText(res.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } else {
//                            AlertUtils.showSnack(ChatActivity.this, recyclerChat, res.getString("message"));
                        }
                    }
                    etChat.setEnabled(true);
                    connectToServerJs();

                } catch (JSONException e) {
                    Logger.e("friendChatListingTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {

            }
        }, "friendChatListingTask");


    }

    private void groupChatListingTask(final String lastMsgId) {
        if (Integer.parseInt(lastMsgId) == 0) {
            progressBar.setVisibility(View.VISIBLE);
        }
        errorText.setVisibility(View.GONE);

        if (isSwiped) {
            recyclerChat.setVisibility(View.VISIBLE);
        } else {
            recyclerChat.setVisibility(View.GONE);
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("match_id", matchId);
        postMap.put("team_id", teamModel.getMyTeamId());
        postMap.put("from_team_id", teamModel.getOpponentTeamId());
        if (!lastMsgId.equals("0")) {
            postMap.put("message_smaller_than_id", lastMsgId);
        }
        postMap.put("limit", String.valueOf(limit));

        vHelper.addVolleyRequestListeners(Api.getInstance().getAllChats, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefresh.setRefreshing(false);
                        Logger.printLongLog("response", response);
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                fileBaseUrl = res.getString("message_file_base_url");
                                JSONArray chatArray = res.getJSONArray("chat_messages");
                                if (chatArray.length() == 0) {
                                    if (!lastMsgId.equals("0")) {
//                                        AlertUtils.showSnack(ChatActivity.this, toolbar, res.getString("message"));
                                    }
                                } else {
                                    ArrayList<ChatModel> tempMesgLists = new ArrayList<>();
                                    for (int i = 0; i < chatArray.length(); i++) {
                                        JSONObject eachChat = chatArray.getJSONObject(i);
                                        ChatModel chatModel = new ChatModel();
                                        chatModel.setMessageId(eachChat.getString("id"));
                                        chatModel.setSenderTeamId(eachChat.getString("from_team_id"));
                                        chatModel.setMyTeamId(teamModel.getMyTeamId());
                                        String userName = eachChat.getString("username");
                                        if (userName.equalsIgnoreCase("null") || TextUtils.isEmpty(userName)) {
                                            chatModel.setUserName(eachChat.getString("full_name"));
                                        } else {
                                            chatModel.setUserName(userName);
                                        }
                                        String messageType = eachChat.getString("message_type");
                                        chatModel.setMessageType(messageType);

                                        String message = eachChat.getString("message");
                                        chatModel.setMessagePreview(message);

                                        if (messageType.equals(ChatModel.MESSAGE_TYPE_ATTACHMENT) && !TextUtils.isEmpty(message)) {
                                            chatModel.setMessagePreview(fileBaseUrl + message);
                                        }

                                        chatModel.setMessageTime(eachChat.getString("send_date"));
                                        chatModel.setMsgStatus(ChatModel.MESSAGE_SENT);
                                        tempMesgLists.add(chatModel);
                                    }

                                    if (lastMsgId.equals("0")) {
                                        chatList.addAll(tempMesgLists);
                                    } else {
//                                        Collections.reverse(tempMesgLists);
                                        chatList.addAll(0, tempMesgLists);
                                    }
                                }

                                if (chatList.size() < 1) {
                                    if (lastMsgId.equals("0")) {
                                        recyclerChat.setVisibility(View.GONE);
                                        errorText.setText("Start a conversation");
                                        errorText.setVisibility(View.VISIBLE);
                                    } else {
//                                        AlertUtils.showSnack(ChatActivity.this, recyclerChat, "No more messages");
                                    }
                                } else {
                                    chatAdapter.notifyDataSetChanged();
                                    recyclerChat.setVisibility(View.VISIBLE);
                                }
                            } else {
                                if (lastMsgId.equals("0")) {
                                    recyclerChat.setVisibility(View.GONE);
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                } else {
//                                    AlertUtils.showSnack(ChatActivity.this, recyclerChat, res.getString("message"));
                                }
                            }
                            etChat.setEnabled(true);
                            connectToServerJs();
                        } catch (JSONException e) {
                            Logger.e("groupChatListingTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("groupChatListingTask error res", e.getMessage() + " ");
                        }
                        if (lastMsgId.equalsIgnoreCase("0")) {
                            errorText.setText(errorMsg);
                            errorText.setVisibility(View.VISIBLE);
                            recyclerChat.setVisibility(View.GONE);
                        } else {
                            AlertUtils.showSnack(ChatActivity.this, toolbar, errorMsg);
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                }, "groupChatListingTask");
    }

    private void connectToServerJs() {
        try {
            if (socketJs != null) {
                if (socketJs.connected()) {
                    socketJs.off();
                    socketJs.disconnect();
                }
            }
//            String nodeServerHostUrl = (Api.getInstance().socketUrl);
            IO.Options opts = new IO.Options();
//            opts.forceNew = true;
//            opts.reconnection = true;
//        opts.query = "auth_token=" + authToken;
            opts.transports = new String[]{WebSocket.NAME};
//            socketJs = IO.socket(nodeServerHostUrl, opts);

            String nodeServerHostUrl = prefHelper.getString(PreferenceHelper.NODE_SEVER, Api.getInstance().nodeUrl)
                    + ":" + prefHelper.getString(PreferenceHelper.NODE_PORT, Api.getInstance().nodePort);
            socketJs = IO.socket(nodeServerHostUrl, opts);
            socketJs.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e("nodeJs def connect call", "Object : ");

                    if (socketJs.connected()) {
                        Logger.e("nodeJs connected", socketJs.toString() + "");
                        socketJs.off();

                        if (isGroupChat) {
                            emitGroupChatRoomConnect();
//                            listenIncomingGroupMessage();
                        } else {
                            emitIndividualChatRoomConnect();
//                            listenIncomingIndividualMessage();
                        }
//                        connection_test();
//                        listenConnection();
//                        listenDisconnectionFromIndividualChatRoom();
//                        listenDisconnectionFromPlayerChatRoom();
//                        listenDisconnectionFromTournamentViewer();
                    } else {
//                        connectToServerJs();
                    }
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e("nodeJs def. disconnect", "Object " + args[0]);
                    socketJs.off();
                    connectToServerJs();
                }
            });
            //listen to node before connect
            if (isGroupChat) {
//                emitGroupChatRoomConnect();
                listenIncomingGroupMessage();
            } else {
//                emitIndividualChatRoomConnect();
                listenIncomingIndividualMessage();
            }
            listenConnection();
            listenDisconnectionFromIndividualChatRoom();
            listenDisconnectionFromPlayerChatRoom();
            listenDisconnectionFromTournamentViewer();
            socketJs.connect();
            Logger.e("connectToServerJs", ">>>>>>>>>>CONNECTING>>>>>>>>>>\n" + nodeServerHostUrl);
        } catch (URISyntaxException e) {
            Logger.e("connectToServerJs ex", e.getMessage() + " ");
        }
    }

    public void listenIncomingGroupMessage() {
        Logger.e("listenIncomingGroupMessage EVENT:", EVENT_LISTEN_GROUP_MESSAGE_INCOMING);
        socketJs.off(EVENT_LISTEN_GROUP_MESSAGE_INCOMING);
        socketJs.on(EVENT_LISTEN_GROUP_MESSAGE_INCOMING, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("listenIncomingGroupMessage call", args[0].toString() + "");
                try {
                    final JSONObject eachChat = new JSONObject(args[0].toString());
                    ChatActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ChatModel chatModel = new ChatModel();
                                chatModel.setMessageId(eachChat.getString("id"));
                                chatModel.setSenderTeamId(eachChat.getString("from_team_id"));
                                chatModel.setMyTeamId(teamModel.getMyTeamId());
                                String userName = eachChat.getString("username");
                                if (userName.equalsIgnoreCase("null") || TextUtils.isEmpty(userName)) {
                                    chatModel.setUserName(eachChat.getString("full_name"));
                                } else {
                                    chatModel.setUserName(userName);
                                }
                                String messageType = eachChat.getString("message_type");
                                chatModel.setMessageType(messageType);
                                chatModel.setMessageTime(eachChat.getString("send_date"));
                                String message = eachChat.getString("sms_text");
                                chatModel.setMessagePreview(message);

                                if (messageType.equals(ChatModel.MESSAGE_TYPE_ATTACHMENT) && !TextUtils.isEmpty(message)) {
                                    chatModel.setMessagePreview(fileBaseUrl + message);
                                }
                                Logger.e("prevsize", chatList.size() + " ");

                                Logger.e("after list size", chatList.size() + " ");
                                chatList.add(chatModel);
                                Logger.e("chat runnable", " u r here " + chatList.size());
//                            chatAdapter.notifyDataSetChanged();
                                chatAdapter.notifyItemInserted(chatList.size() - 1);
                                recyclerChat.scrollToPosition(chatList.size() - 1);
                                if (recyclerChat.getVisibility() == View.GONE) {
                                    recyclerChat.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                Logger.e("listeenIncoming group onUI thread ex", e.getMessage() + " ");
                            }
                        }
                    });
                } catch (Exception e) {
                    Logger.e("listeenIncoming ex", e.getMessage() + " ");
                }
            }
        });
    }

    public void listenIncomingIndividualMessage() {
        Logger.e("listenIncomingIndividualMessage EVENT:", EVENT_LISTEN_INDIVIDUAL_MESSAGE);
        socketJs.off(EVENT_LISTEN_INDIVIDUAL_MESSAGE);
        socketJs.on(EVENT_LISTEN_INDIVIDUAL_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Logger.e("listenIncomingIndividualMessage call", args[0].toString() + " ");
                            JSONObject eachChat = new JSONObject(args[0].toString());
                            final ChatModel chatModel = new ChatModel();
                            chatModel.setMessageId(eachChat.getString("id"));
                            chatModel.setSenderTeamId(eachChat.getString("from_user_id"));
                            chatModel.setMyTeamId(prefHelper.getUserId());
                            String userName = eachChat.getString("username");
                            if (userName.equalsIgnoreCase("null") || TextUtils.isEmpty(userName)) {
                                chatModel.setUserName(eachChat.getString("full_name"));
                            } else {
                                chatModel.setUserName(userName);
                            }
                            String messageType = eachChat.getString("message_type");
                            chatModel.setMessageType(messageType);
                            chatModel.setMessageTime(eachChat.getString("send_date"));
                            String message = eachChat.getString("sms_text");
                            chatModel.setMessagePreview(message);

                            if (messageType.equals(ChatModel.MESSAGE_TYPE_ATTACHMENT) && !TextUtils.isEmpty(message)) {
                                chatModel.setMessagePreview(fileBaseUrl + message);
                            }

                            chatList.add(chatModel);
                            chatAdapter.notifyItemInserted(chatList.size() - 1);
                            recyclerChat.scrollToPosition(chatList.size() - 1);
                            if (recyclerChat.getVisibility() == View.GONE) {
                                recyclerChat.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            Logger.e("listenIncomingIndividualMessage ex", e.getMessage() + " ");
                        }
                    }
                });
            }
        });
    }

    public void listenDisconnectionFromPlayerChatRoom() {
        Logger.e("listenDisconnectFomPlayerChat EVENT:", EVENT_LISTEN_DISCONNECTION_FROM_PLAYER_CHAT);
        socketJs.on(EVENT_LISTEN_DISCONNECTION_FROM_PLAYER_CHAT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("listenDisconnectFomPlayerChat call", " d1111111111111111111");
            }
        });

    }

    public void listenDisconnectionFromIndividualChatRoom() {
        Logger.e("listenDisconnectFomIndividualChat EVENT:", EVENT_LISTEN_DISCONNECTION_FROM_INDIVIDUAL_CHAT);
        socketJs.on(EVENT_LISTEN_DISCONNECTION_FROM_INDIVIDUAL_CHAT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("listenDisconnectFomIndividualChat call", "d222222222222222222");

            }
        });

    }

    public void listenDisconnectionFromTournamentViewer() {
        Logger.e("listenDisconnectFomPlayerChat EVENT:", EVENT_LISTEN_DISCONNECTION_FROM_TOURNAMENT_VIEWER);
        socketJs.on(EVENT_LISTEN_DISCONNECTION_FROM_TOURNAMENT_VIEWER, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("listenDisconnectFomPlayerChat call", "d333333333333333333333");

            }
        });

    }


    public void listenConnection() {
        Logger.e("listenConnection EVENT:", EVENT_LISTEN_CONNECTION);
        socketJs.on(EVENT_LISTEN_CONNECTION, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("listenConnection call", "d44444444444444444444444444444444444");
//                if (isGroupChat) {
//                    emitGroupChatRoomConnect();
//                } else {
//                    emitIndividualChatRoomConnect();
//                }
            }
        });

    }

//    public void connection_test() {
//        socketJs.on("connection_test", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("connection_test call", args[0].toString() + "");
//            }
//        });
//    }

    public void emitGroupChatRoomConnect() {
        if (!socketJs.connected()) {
            connectToServerJs();
            return;
        }
        // Sending an object
        JSONObject obj = new JSONObject();
        try {
            obj.put("match_id", matchId);
            obj.put("user_id", prefHelper.getUserId());
            socketJs.emit(EVENT_JOIN_GROUP_CHAT_ROOM, obj, new Ack() {
                @Override
                public void call(Object... args) {
                    Logger.e("emitGroupChatRoomConnect ack", args[0] + "");
                }
            });
            Logger.e("emitGroupChatRoomConnect emit", "EVENT:{" + EVENT_JOIN_GROUP_CHAT_ROOM + "}\n" + obj.toString());
        } catch (JSONException e) {
            Logger.e("emitGroupChatRoomConnect ex", e.getMessage() + "");
        }
    }

    public void emitIndividualChatRoomConnect() {
        if (!socketJs.connected()) {
            connectToServerJs();
            return;
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", prefHelper.getUserId());
            obj.put("friend_user_id", friendMdl.getUserId());
            socketJs.emit(EVENT_JOIN_INDIVIDUAL_CHAT_ROOM, obj, new Ack() {
                @Override
                public void call(Object... args) {
                    Logger.e("emitIndividualChatRoomConnect ack", args[0] + "");
                }
            });
            Logger.e("emitIndividualChatRoomConnect emit", "EVENT:{" + EVENT_JOIN_INDIVIDUAL_CHAT_ROOM + "}\n" + obj.toString());
        } catch (JSONException ex) {
            Logger.e("emitIndividualChatRoomConnect json ex", ex.getMessage());
        }
    }

    public void emitOutgoingGroupMessage(String jsonResponse) {
        if (!socketJs.connected()) {
            connectToServerJs();
            return;
        }
        // Sending an object
        try {
            JSONObject obj = new JSONObject(jsonResponse);
            obj = obj.getJSONObject("message_data");
            socketJs.emit(EVENT_SEND_GROUP_MESSAGE, obj, new Ack() {
                @Override
                public void call(Object... args) {
                    Logger.e("emitOutgoingGroupMessage ack", args[0] + "");
                }
            });
            Logger.e("emitOutgoingGroupMessage emit", "EVENT:{" + EVENT_SEND_GROUP_MESSAGE + "}\n" + obj.toString());
        } catch (JSONException e) {
            Logger.e("emitOutgoingGroupMessage ex", e.getMessage() + "");
        }
    }

    public void emitOutGoingIndividualMessage(String jsonResponse) {
        if (!socketJs.connected()) {
            connectToServerJs();
            return;
        }

        try {
            JSONObject obj = new JSONObject(jsonResponse);
            obj = obj.getJSONObject("sent_message");
            socketJs.emit(EVENT_SEND_INDIVIDUAL_MESSAGE, obj, new Ack() {
                @Override
                public void call(Object... args) {
                    Logger.e("emitOutGoingIndividualMessage ack", args[0] + "");
                }
            });
            Logger.e("emitOutGoingIndividualMessage emit", "EVENT:{" + EVENT_SEND_INDIVIDUAL_MESSAGE + "}\n" + obj.toString());
        } catch (JSONException e) {
            Logger.e("emitOutGoingIndividualMessage ex", e.getMessage() + "");
        }
    }

    public void emitDisconnect() {
        // Sending an object
        JSONObject obj = new JSONObject();
        socketJs.emit(EVENT_DISCONNECT_ROOM, obj, new Ack() {
            @Override
            public void call(Object... args) {
                Logger.e("emitDisconnect ack", args[0] + "");
            }
        });
        Logger.e("emitDisconnect emit", "EVENT:{" + EVENT_DISCONNECT_ROOM + "}\n" + obj.toString());
    }

    @Override
    public void onBackPressed() {
        disconnectJS();
        if (chatList.size() > 0) {
            markChatReadTask(chatList.get(chatList.size() - 1).getMessageId());
        }
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        disconnectJS();
        super.onDestroy();
    }

//    @Override
//    protected void onStop() {
//        disconnectJS();
//        super.onStop();
//    }

    private void disconnectJS() {
        Logger.e("disconnectJs", "method call");
        try {
            AlertUtils.hideInputMethod(this, recyclerChat);
            if (socketJs != null) {
                Logger.e("disconnectJs", "socket not null");
                if (socketJs.connected()) {
                    Logger.e("Disconnecting SOCKET >>>>>>>>>>>>>>>>>", "DISCONNECT >>>>>>>>>>>>>>");
                    emitDisconnect();
                    socketJs.off();
                    socketJs.disconnect();
//                    socketJs.close();

                    if (socketJs.connected()) {
                        Logger.e("socket still connected", "connected !!!");
                        socketJs.disconnect();
                    }
                    socketJs = null;
                }
            } else {
                Logger.e("disconnectJs", "socket already null");
            }
        } catch (Exception e) {
            Logger.e("disconnectJS ex", e.getMessage() + "");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNodeConnection();
        if (isGroupChat) {
            prefHelper.edit().putString(chattingMatchId, matchId).commit();
        } else {
            Logger.e("friend user id", friendMdl.getUserId());
            prefHelper.edit().putString(chattingMatchId, friendMdl.getUserId()).commit();
        }
    }

    Handler nodeConnectionHandler = new Handler();
    int reconnectTimeMills = 4000;
    Runnable nodeConnectRunnable = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (socketJs != null) {
                        if (!socketJs.connected()) {
//                            connectToServerJs();
                            Logger.e(" disconnected", " u rhere");
                        } else {
                            Logger.e("connected", " u here");

                            if (isGroupChat) {
                                if (socketJs.hasListeners(EVENT_LISTEN_GROUP_MESSAGE_INCOMING)) {
                                    Logger.e("node listening to incoming", " yes it is listening to :" + EVENT_LISTEN_GROUP_MESSAGE_INCOMING);
                                } else {
                                    listenIncomingGroupMessage();
                                }
                                emitGroupChatRoomConnect();
                            } else {
                                if (socketJs.hasListeners(EVENT_LISTEN_INDIVIDUAL_MESSAGE)) {
                                    Logger.e("node listening to incoming", " yes it is listening to : " + EVENT_LISTEN_INDIVIDUAL_MESSAGE);
                                } else {
                                    listenIncomingIndividualMessage();
                                }
                            }
                        }
                    }
                    nodeConnectionHandler.postDelayed(nodeConnectRunnable, reconnectTimeMills);
                }
            });
        }
    };

    private void checkNodeConnection() {
        if (nodeConnectionHandler != null) {
            nodeConnectionHandler.postDelayed(nodeConnectRunnable, reconnectTimeMills);
        } else {
            nodeConnectionHandler = new Handler();
            nodeConnectionHandler.postDelayed(nodeConnectRunnable, reconnectTimeMills);
        }
    }

    private void stopNodeConnectionCheck() {
        if (nodeConnectionHandler != null) {
            nodeConnectionHandler.removeCallbacks(nodeConnectRunnable);
            nodeConnectionHandler = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopNodeConnectionCheck();
        prefHelper.edit().putString(chattingMatchId, "null").commit();
    }


    private boolean IsRecyclerViewAtTop() {
        if (recyclerChat.getChildCount() == 0) {
            return true;
        }
        return recyclerChat.getChildAt(0).getTop() == 0;
    }

    //Add this method to show Dialog when the required permission has been granted to the app.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case FilePickerDialog.EXTERNAL_READ_PERMISSION_GRANT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (dialog != null) {   //Show dialog if the read permission has been granted.
                        dialog.show();
                    }
                } else {
                    //Permission has not been granted. Notify the user.
                    AlertUtils.showToast(ChatActivity.this, "Permission is Required for getting list of files");
                }
            }
        }
    }

    private void markReadTask(String mesgId) {
        VolleyHelper vHelper = VolleyHelper.getInstance(ChatActivity.this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", mesgId);
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }

    private void markChatReadTask(String mesgId) {
        VolleyHelper vHelper = VolleyHelper.getInstance(ChatActivity.this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("msg_id", mesgId);
        vHelper.addVolleyRequestListeners(Api.getInstance().readMessageApi, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markChatReadTask");
    }
}
