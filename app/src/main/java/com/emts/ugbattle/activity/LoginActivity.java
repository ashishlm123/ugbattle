package com.emts.ugbattle.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.MainActivity;
import com.emts.ugbattle.R;
import com.emts.ugbattle.SiteSettingsTask;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Srijana on 5/30/2017.
 */

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    CallbackManager callbackManager;
    TextView tvCreateAc, tvForgotPass, errorUserName, errorPassword;
    EditText etUserName, etPassword;
    LinearLayout fbRegister;
    PreferenceHelper prefsHelper;
    Calendar calendar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.setApplicationId(getString(R.string.facebook_app_id));

        //version check
        if (NetworkUtils.isInNetwork(this)) {
            SiteSettingsTask.getSiteSettings(this, new SiteSettingsTask.VersionUpdateCallback() {
                @Override
                public void onVersionUpdated(boolean isVersionUpdated) {
                    SiteSettingsTask.showVersionUpdateDialog(LoginActivity.this);
                }
            });
        }

        setContentView(R.layout.activity_login);

        prefsHelper = PreferenceHelper.getInstance(this);

        btnLogin = findViewById(R.id.btn_login);
        tvCreateAc = findViewById(R.id.tv_create_ac);
        tvForgotPass = findViewById(R.id.tv_forgot_password);
        etUserName = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        fbRegister = findViewById(R.id.btn_registerfb);
        errorUserName = findViewById(R.id.valid_username);
        errorPassword = findViewById(R.id.valid_password);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;
                if (TextUtils.isEmpty(etUserName.getText().toString())) {
                    isValid = false;
                    errorUserName.setText(R.string.error_text);
                    errorUserName.setVisibility(View.VISIBLE);
                } else {
                    errorUserName.setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                    isValid = false;
                    errorPassword.setText(R.string.error_text);
                    errorPassword.setVisibility(View.VISIBLE);
                } else {
                    errorPassword.setVisibility(View.GONE);
                }
                if (isValid) {
                    if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                        loginTask();
                    } else {
                        AlertUtils.showSnack(getApplicationContext(), btnLogin, getResources().getString(R.string.error_no_internet));
                    }
                } else {
                    AlertUtils.showSnack(getApplicationContext(), etPassword, getString(R.string.form_validate_message));
                }
            }
        });

        tvCreateAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

        tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });

        fbRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                    fbSignIN();
                } else {
                    AlertUtils.showSnack(LoginActivity.this, view, getString(R.string.error_no_internet));
                }
            }
        });

    }

    private void loginTask() {
        AlertUtils.hideInputMethod(LoginActivity.this, etUserName);
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(this, "Please Wait...");
        progressDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(LoginActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("username", etUserName.getText().toString().trim());
        postMap.put("password", etPassword.getText().toString().trim());
        postMap.put("device_id", FirebaseInstanceId.getInstance().getToken());

        vHelper.addVolleyRequestListeners(Api.getInstance().loginUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressDialog.dismiss();
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                loginToApp(resObj);
                            } else {
                                final String errorMsg = resObj.getString("message");
                                AlertUtils.showAlertMessage(LoginActivity.this, "Login Error",
                                        errorMsg);
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            Logger.e("loginTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userSignUpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(LoginActivity.this, btnLogin,
                                errorMsg);
                    }
                }, "loginTask");
    }

    private void fbSignIN() {
        LoginManager.getInstance().logOut();

        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, (Arrays.asList("email",
                "public_profile")));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        Logger.e("facebook login response", loginResult.toString() + " &&\n "
                                + loginResult.getAccessToken() + "\n" + loginResult.getRecentlyGrantedPermissions()
                                + "\n" + loginResult.getRecentlyDeniedPermissions() + "\n");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code
                                        try {
                                            String id = object.optString("id");
                                            String name = object.optString("name");
                                            String first_name = object.optString("first_name");
                                            String last_name = object.optString("last_name");
                                            String age = object.optString("age_range");

                                            String fbAccessToken = loginResult.getAccessToken().getToken();
                                            Logger.e("fbAccessToken", fbAccessToken);

//                                            https://graph.facebook.com/117023115815155/picture?type=large&access_token={access_token}

                                            String picture = "https://graph.facebook.com/" + id + "/picture?type=large&access_token=" + fbAccessToken;
                                            String email = object.optString("email"); //Get null value here
                                            String gender = object.optString("gender");
                                            String username = object.optString("username");

                                            Logger.e("Facebook Data", id + "\n" + name + "\n" + first_name
                                                    + "\n" + last_name + "\n" + email + "\n" + age + "\n" + picture + "\n"
                                                    + gender + "\n" + username);
                                            LoginManager.getInstance().logOut();


                                            registerWithFbTask(id, first_name, last_name, email, gender,
                                                    picture, false);

                                        } catch (Exception e) {
                                            Logger.e("facebook exception", e.getMessage());
                                            LoginManager.getInstance().logOut();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name,last_name,email,gender," +
                                "age_range");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Logger.e("fbSignIn cancel", "called");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Logger.e("fbSignIn error", exception.getMessage() + "\n" + exception.getCause());
                    }
                });

    }

    private void loginToApp(JSONObject resObj) throws JSONException {
        final JSONObject dataObj = resObj.getJSONObject("user_info");
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_ID, dataObj.getString("id")).commit();
        if (TextUtils.isEmpty(dataObj.getString("username"))) {
            prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, dataObj.getString("full_name")).commit();
        } else {
            prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, dataObj.getString("username")).commit();
        }
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, dataObj.getString("email")).commit();
        prefsHelper.edit().putString(PreferenceHelper.TOKEN, dataObj.getString("user_token")).commit();
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_IMG, dataObj.getString("profile_img")).commit();
        prefsHelper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, dataObj.getString("ticket_balance")).commit();
        prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void registerWithFbTask(final String fbId, final String fbFirst_name, final String lastName,
                                    final String fbEmail, final String gender, String profileImg,
                                    final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("social_id", fbId);
        postParams.put("reg_type", "facebook");
        postParams.put("email", fbEmail);
        postParams.put("first_name", fbFirst_name);
        postParams.put("last_name", lastName);
        postParams.put("profile_img", profileImg);
        postParams.put("device_id", FirebaseInstanceId.getInstance().getToken());
        if (gender != null && gender.length() > 1) {
            postParams.put("gender", gender.substring(0, 1).toUpperCase());
        }

//        String pushToken = FirebaseInstanceId.getInstance().getToken();
//        if (!TextUtils.isEmpty(pushToken)) {
//            postParams.put("push_id", pushToken);
//        }

        AlertUtils.hideInputMethod(LoginActivity.this, etUserName);

        vHelper.addVolleyRequestListeners(Api.getInstance().socialLoginUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                loginToApp(res);
                            } else {
                                AlertUtils.showAlertMessage(LoginActivity.this,
                                        getString(R.string.title_error), res.getString("message"));
                            }

                        } catch (JSONException e) {
                            Logger.e("loginTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userSignUpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(LoginActivity.this, btnLogin,
                                errorMsg);
                    }
                }, "registerWithFbTask");
    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("loginTask");
        VolleyHelper.getInstance(this).cancelRequest("registerWithFbTask");
        super.onDestroy();
    }
}
