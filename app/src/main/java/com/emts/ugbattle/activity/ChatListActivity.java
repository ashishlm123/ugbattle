//package com.emts.ugbattle.activity;
//
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.VolleyError;
//import com.emts.ugbattle.R;
//import com.emts.ugbattle.adapter.ChatListAdapter;
//import com.emts.ugbattle.helper.AlertUtils;
//import com.emts.ugbattle.helper.Api;
//import com.emts.ugbattle.helper.Logger;
//import com.emts.ugbattle.helper.NetworkUtils;
//import com.emts.ugbattle.helper.PreferenceHelper;
//import com.emts.ugbattle.helper.VolleyHelper;
//import com.emts.ugbattle.model.UserModel;
//
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//
//public class ChatListActivity extends AppCompatActivity {
//    RecyclerView chatListing;
//    ChatListAdapter chatListAdapter;
//    ArrayList<ChatThread> chatList;
//    Toolbar toolbar;
//    ProgressBar progressBar, infiniteProgressBar;
//    PreferenceHelper helper;
//    TextView errorText;
//    int limit = 50;
//    int offset = 0;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.activity_chat_list);
//
//        toolbar = findViewById(R.id.toolbar);
//        toolbar.setTitle("Chats");
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//
//        chatList = new ArrayList<>();
//        helper = PreferenceHelper.getInstance(this);
//
//        progressBar = findViewById(R.id.progress_bar);
//        infiniteProgressBar = findViewById(R.id.infinite_progress_bar);
//        errorText = findViewById(R.id.error_text);
//
//        chatListing = findViewById(R.id.listings);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(ChatListActivity.this);
//        chatListing.setLayoutManager(layoutManager);
//        chatListing.setNestedScrollingEnabled(false);
//        chatListAdapter = new ChatListAdapter(chatList, ChatListActivity.this);
//        chatListing.setAdapter(chatListAdapter);
//
////        chatListing.addOnScrollListener(new InfiniteScrollList(layoutManager) {
////            @Override
////            public void onLoadMore(int page, int totalItemsCount) {
////                if (NetworkUtils.isInNetwork(ChatListActivity.this)) {
////                    chatThreadTask();
////                } else {
////                    AlertUtils.showSnack(ChatListActivity.this, chatListing, getString(R.string.no_internet));
////                }
////            }
////        });
//
//        if (NetworkUtils.isInNetwork(this)) {
//            errorText.setVisibility(View.GONE);
//            chatThreadTask();
//        } else {
//            errorText.setText(R.string.error_no_internet);
//            errorText.setVisibility(View.VISIBLE);
//            progressBar.setVisibility(View.GONE);
//            chatListing.setVisibility(View.GONE);
//        }
//    }
//
//    private void chatThreadTask() {
//        if (offset == 0) {
//            progressBar.setVisibility(View.VISIBLE);
//        } else {
//            infiniteProgressBar.setVisibility(View.VISIBLE);
//        }
//
//        VolleyHelper volleyHelper = VolleyHelper.getInstance(ChatListActivity.this);
//        HashMap<String, String> postParams = volleyHelper.getPostParams();
//        postParams.put("offset", String.valueOf(offset));
//        postParams.put("limit", String.valueOf(limit));
//
//        volleyHelper.addVolleyRequestListeners(Api.getInstance().getChatThread, Request.Method.POST,
//                postParams, new VolleyHelper.VolleyHelperInterface() {
//                    @Override
//                    public void onSuccess(String response) {
//                        if (offset == 0) {
//                            chatList.clear();
//                            progressBar.setVisibility(View.GONE);
//                        } else {
//                            infiniteProgressBar.setVisibility(View.GONE);
//                        }
//                        try {
//                            JSONObject resObj = new JSONObject(response);
//                            if (resObj.getBoolean("status")) {
//                                offset = limit + offset;
//                                JSONArray dialogArray = resObj.getJSONArray("chat_list");
//                                for (int i = 0; i < dialogArray.length(); i++) {
//                                    JSONObject eachThread = dialogArray.getJSONObject(i);
//
//                                    ChatThread chatThread = new ChatThread();
//                                    //NOTE : here sender is the opponent friend that user is in chat
//                                    // with but not the sender for last message
//                                    UserModel sentBy = new UserModel();
////                                    sentBy.setUserId(eachThread.getString("sender_id"));
////                                    sentBy.setUserName(eachThread.getString("sender_name"));
////                                    String senderImage = eachThread.getString("sender_image");
//                                    sentBy.setUserId(eachThread.getString("opponent_id"));
//                                    sentBy.setUserName(eachThread.getString("opponent_name"));
//                                    String senderImage = eachThread.getString("opponent_image");
////                                    if (!TextUtils.isEmpty(senderImage)) {
////                                        sentBy.setImgUrl(resObj.getString("default_profile_img_Url") + senderImage);
////                                    } else {
////                                        sentBy.setImgUrl("");
////                                    }
//                                    chatThread.setThreadId(eachThread.getString("thread_id"));
//                                    chatThread.setSentBy(sentBy);
//                                    chatThread.setLatestMsgPreview(eachThread.getString("message"));
//                                    chatThread.setChatTime(eachThread.getString("message_date"));
//                                    chatThread.setLastMsgId(eachThread.getString("message_id"));
//
//                                    chatList.add(chatThread);
//                                }
//
//                                if (chatList.size() > 0) {
//                                    errorText.setVisibility(View.GONE);
//                                    chatListing.setVisibility(View.VISIBLE);
//                                    chatListAdapter.notifyDataSetChanged();
//                                } else {
//                                    errorText.setText("No Chats Available");
//                                    errorText.setVisibility(View.VISIBLE);
//                                    chatListing.setVisibility(View.GONE);
//                                }
//                            } else {
//                                if (offset == 0) {
//                                    errorText.setText(resObj.getString("message"));
//                                    errorText.setVisibility(View.VISIBLE);
//                                    chatListing.setVisibility(View.GONE);
//                                } else {
//                                    AlertUtils.showSnack(ChatListActivity.this, chatListing, resObj.getString("message"));
//                                }
//                            }
//                        } catch (JSONException e) {
//                            Logger.e("chatThreadTask resObj ex", e.getMessage() + "");
//                        }
//                    }
//
//                    @Override
//                    public void onError(String errorResponse, VolleyError volleyError) {
//                        String errMsg = "Unexpected error occur please reload again";
//                        try {
//                            JSONObject errorObj = new JSONObject(errorResponse);
//                            errMsg = errorObj.getString("message");
//                        } catch (Exception e) {
//                            Logger.e("chatThreadTask error Ex", e.getMessage() + "");
//                        }
//                        if (offset == 0) {
//                            errorText.setText(errMsg);
//                            progressBar.setVisibility(View.GONE);
//                            chatListing.setVisibility(View.GONE);
//                            errorText.setVisibility(View.VISIBLE);
//                        } else {
//                            infiniteProgressBar.setVisibility(View.GONE);
//                            AlertUtils.showSnack(ChatListActivity.this, chatListing, errMsg);
//                        }
//                    }
//                }, "chatThreadTask");
//    }
//
//    @Override
//    public void onBackPressed() {
//        AlertUtils.hideInputMethod(ChatListActivity.this, errorText);
//        super.onBackPressed();
//    }
//}
