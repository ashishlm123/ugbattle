package com.emts.ugbattle.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AddFriendDetailActivity extends AppCompatActivity {
    MessageModel messageMdl;
    boolean isFromPush = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request_detail);

        Intent intent = getIntent();
        if (intent != null) {
            messageMdl = (MessageModel) intent.getSerializableExtra("mesg");
            isFromPush = intent.getBooleanExtra("from_push", false);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView customTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        customTitle.setText("Friend Request");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ImageView friendImage = findViewById(R.id.user_img);
        TextView tvUserName = findViewById(R.id.tv_user_name);
        TextView tvStatus = findViewById(R.id.tv_user_status);
        Button btnAccept = findViewById(R.id.btn_accept);
        Button btnReject = findViewById(R.id.btn_reject);
        Glide.with(AddFriendDetailActivity.this).load(messageMdl.getUserProfilePic()).into(friendImage);

        tvUserName.setText(messageMdl.getUserName());
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptFriendRequestTask();
            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectFriendRequestTask();
            }
        });

        if (!messageMdl.getRespType().equals(Config.STATUS_AWAITING)) {
            btnAccept.setVisibility(View.GONE);
            btnReject.setVisibility(View.GONE);
            tvStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("You have " + messageMdl.getRespType() + " friend request send by " + messageMdl.getUserName());
        } else {
            tvStatus.setVisibility(View.GONE);
            btnAccept.setVisibility(View.VISIBLE);
            btnReject.setVisibility(View.VISIBLE);
        }

        if (isFromPush) {
            if (NetworkUtils.isInNetwork(AddFriendDetailActivity.this)) {
                markReadTask();
            }
        }
    }

    private void rejectFriendRequestTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(AddFriendDetailActivity.this, " Please wait");
        pDialog.show();
        VolleyHelper volleyHelper = VolleyHelper.getInstance(AddFriendDetailActivity.this);
        HashMap<String, String> postParam = volleyHelper.getPostParams();
        postParam.put("friend_id", messageMdl.getFriend_id());
        postParam.put("note_id", messageMdl.getMessageId());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().rejectFriendRequestUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Intent intent = new Intent();
                                intent.putExtra("resp_type", Config.IS_REJECTED);
                                intent.putExtra("not_id", messageMdl.getMessageId());
                                setResult(RESULT_OK, intent);

                                AlertUtils.showAlertMessage(AddFriendDetailActivity.this, false, "Success Message", res.getString("message"), "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        AddFriendDetailActivity.this.finish();
                                    }
                                });
                            } else {
                                AlertUtils.showAlertMessage(AddFriendDetailActivity.this, "Error Message", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("rejectFriendRequestTask ex", e.getMessage() + " ");
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("requestWithdrawalTask error res", e.getMessage() + " ");
                        }

                        AlertUtils.showAlertMessage(AddFriendDetailActivity.this, "Error Message", errorMsg);

                    }
                }, "rejectFriendRequestTask");
    }

    private void acceptFriendRequestTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(AddFriendDetailActivity.this, " Please wait");
        pDialog.show();
        VolleyHelper volleyHelper = VolleyHelper.getInstance(AddFriendDetailActivity.this);
        HashMap<String, String> postParam = volleyHelper.getPostParams();
        postParam.put("friend_id", messageMdl.getFriend_id());
        postParam.put("note_id", messageMdl.getMessageId());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().acceptFriendRequestUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Intent intent = new Intent();
                                intent.putExtra("resp_type", Config.Accepted);
                                intent.putExtra("not_id", messageMdl.getMessageId());
                                setResult(RESULT_OK, intent);
                                AlertUtils.showAlertMessage(AddFriendDetailActivity.this, false, "Success Message", res.getString("message"), "OK", " Cancel", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        AddFriendDetailActivity.this.finish();
                                    }
                                });
                            } else {
                                AlertUtils.showAlertMessage(AddFriendDetailActivity.this, "Error Message", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("rejectFriendRequestTask ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("requestWithdrawalTask error res", e.getMessage() + " ");
                        }

                        AlertUtils.showAlertMessage(AddFriendDetailActivity.this, "Error Message", errorMsg);

                    }
                }, "acceptFriendRequestTask");
    }

    private void markReadTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(AddFriendDetailActivity.this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", messageMdl.getMessageId());
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }
}
