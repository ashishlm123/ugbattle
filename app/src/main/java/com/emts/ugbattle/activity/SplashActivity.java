package com.emts.ugbattle.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Window;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.MainActivity;
import com.emts.ugbattle.R;
import com.emts.ugbattle.UserTicketBalanceUpdate;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.TournamentModel;
import com.emts.ugbattle.model.UserModel;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Ashish on 29/05/2017.
 */

public class SplashActivity extends AppCompatActivity {
    private int splashTimer = 2000;
    PreferenceHelper helper;

    Handler mHandler;
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (helper.isLogin()) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
            SplashActivity.this.finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getApplicationContext());
        super.onCreate(savedInstanceState);

        if (checkIfFCM()) {
            finish();
            return;
        }

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);

        helper = PreferenceHelper.getInstance(this);
        mHandler = new Handler();

        mHandler.postDelayed(mRunnable, splashTimer);

        if (helper.isLogin()) {
            if (!NetworkUtils.isInNetwork(this)) {
                return;
            }
            UserTicketBalanceUpdate.getTicketBalanceTask(this);
            if (helper.getBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, false)) {
                updateDeviceTokenTask(SplashActivity.this);
            }
        }


//        versionCheck versionCheck = new versionCheck();
//        versionCheck.getCurrentVersion(this);


//        PackageInfo info;
//        try {
//            info = getPackageManager().getPackageInfo("com.emts.ugbattle", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md;
//                md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String something = new String(Base64.encode(md.digest(), 0));
//                //String something = new String(Base64.encodeBytes(md.digest()));
//                Log.e("keyhash", something);
//            }
//        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
//        } catch (Exception e) {
//            Log.e("exception", e.toString());
//        }

    }

    @Override
    public void onBackPressed() {
        mHandler.removeCallbacks(mRunnable);
        super.onBackPressed();
    }

    public void updateDeviceTokenTask(Context context) {
        helper = PreferenceHelper.getInstance(SplashActivity.this);
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("token", helper.getToken());
        postParams.put("user_id", String.valueOf(helper.getUserId()));
        if (!TextUtils.isEmpty(FirebaseInstanceId.getInstance().getToken()) || !FirebaseInstanceId.getInstance().getToken().equals(null)) {
            postParams.put("device_id", FirebaseInstanceId.getInstance().getToken());
            Logger.e("devic_update", postParams.get("device_id"));
        }


        vHelper.addVolleyRequestListeners(Api.getInstance().upgradeDevicetoken, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                helper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, false).commit();
                            } else {
                                helper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, true).commit();

                            }
                        } catch (JSONException e) {

                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        helper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, true).commit();
                    }
                }, " updateDeviceTokenTask");


    }


    private boolean checkIfFCM() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return false;
        }

        String notType = bundle.getString("note_type");
        //bundle must contain all info sent in "data" field of the notification
        if (notType == null) return false;
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        HashMap<String, String> dataMap = new HashMap<>();
        for (String key : bundle.keySet()) {
            dataMap.put(key, bundle.get(key).toString()); //To Implement
        }
        if (!helper.isLogin()) {
            return false;
        }
        MessageModel messageModel = null;

        try {

            String title = dataMap.get("subject");

            String body = StringEscapeUtils.unescapeJava(dataMap.get("message"));

            String notify_type = dataMap.get("type");
//            int userId = 0;

            String messageFrom = dataMap.get("to_user_type");
            String sendby = dataMap.get("user_id");
//            String fileDir = data.get("dir");

            String jsonData = dataMap.get("description");
            JSONObject eachObj = new JSONObject(jsonData);

            Logger.e("FRM : DATA", "FRM : " + jsonData + "\nDATA : " + jsonData);

            //check if login to app else open splash

            messageModel = new MessageModel();
            messageModel.setMessageId(dataMap.get("note_id"));
            messageModel.setMessageType(dataMap.get("type"));
            messageModel.setMessageTime(dataMap.get("create_date"));

            switch (notify_type) {
                case Config.FRIEND_REQUEST:
                    messageModel.setUserName(eachObj.getString("requested_user"));
                    messageModel.setFriend_id(eachObj.getString("requested_user_id"));
                    messageModel.setRespType(eachObj.getString("inv_status"));
                    if (eachObj.getString("requested_user_img").equals("null")) {
                        messageModel.setUserProfilePic(eachObj.getString("default_profile_img_url"));
                    } else {
                        messageModel.setUserProfilePic(eachObj.getString("profile_img_url") +
                                eachObj.getString("requested_user_img"));
                    }
                    intent = new Intent(getApplicationContext(), AddFriendDetailActivity.class);
                    break;

                case Config.FRIEND_REQUEST_RESP:
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;


                case Config.WITHDRAWAL_RES:
                    PreferenceHelper.getInstance(getApplicationContext()).edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, eachObj.getString("remaining_ticket_balance")).apply();
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.PLAYER_INVITATION_REQ:
                    GameMdl gameMdl = new GameMdl();
                    gameMdl.setGameId(eachObj.getString("game_id"));
                    gameMdl.setGameServersArray(eachObj.getString("server_name"));
                    gameMdl.setGameUserServer(eachObj.getString("receiving_user_server_name"));
                    gameMdl.setGameUserId(eachObj.getString("receiving_user_player_id"));
                    gameMdl.setGameName(eachObj.getString("game_name"));
                    messageModel.setExtras(gameMdl);

                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.PLAYER_INVITATION_RES:
                    TournamentModel tournMdl = new TournamentModel();
                    tournMdl.setGameId(eachObj.getString("game_id"));
                    tournMdl.setTeamId(eachObj.getString("team_id"));
                    tournMdl.setCurrentRoundNo(Integer.parseInt(eachObj.getString("current_round_no")));
                    tournMdl.setTrnmntId(eachObj.getString("tourn_id"));
                    tournMdl.setGameName(eachObj.getString("game_name"));
                    tournMdl.setGameServersArray(eachObj.getString("server_name"));
                    tournMdl.setTrnmntName(eachObj.getString("tourn_name"));
                    messageModel.setExtras(tournMdl);

                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.TICKET_PURCHASE_RES:
                    PreferenceHelper.getInstance(getApplicationContext()).edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, eachObj.getString("new_ticket_balance")).apply();
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.MATCH_CHALLENGE_REQUEST:
                    TournamentModel tournamentModel = new TournamentModel();
                    tournamentModel.setGameId(eachObj.getString("game_id"));
                    tournamentModel.setTeamId(eachObj.getString("team_id"));
                    tournamentModel.setCurrentRoundNo(Integer.parseInt(eachObj.getString("current_round_no")));
                    tournamentModel.setTrnmntId(eachObj.getString("tourn_id"));
                    tournamentModel.setGameName(eachObj.getString("game_name"));
                    messageModel.setExtras(tournamentModel);
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.MATCH_CHALLENGE_RESP:
                    TournamentModel tournamentModel1 = new TournamentModel();
                    tournamentModel1.setGameId(eachObj.getString("game_id"));
                    tournamentModel1.setTeamId(eachObj.getString("team_id"));
                    tournamentModel1.setCurrentRoundNo(Integer.parseInt(eachObj.getString("current_round_no")));
                    tournamentModel1.setTrnmntId(eachObj.getString("tourn_id"));
                    tournamentModel1.setGameName(eachObj.getString("game_name"));
                    messageModel.setExtras(tournamentModel1);
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.MESSAGE_NOTI:
                    JSONObject response = new JSONObject();
                    try {
                        JSONObject match_info = new JSONObject();
                        match_info.put("game_id", eachObj.getString("game_id"));
                        match_info.put("tourn_id", eachObj.getString("tourn_id"));
                        match_info.put("match_id", eachObj.getString("match_id"));

                        JSONObject my_team = new JSONObject();
                        my_team.put("team_name", eachObj.getString("team_name"));
                        my_team.put("current_round_no", eachObj.getString("current_round_no"));
                        my_team.put("team_id", eachObj.getString("team_id"));
                        match_info.put("my_team", my_team);

                        JSONObject opponent_team = new JSONObject();
                        opponent_team.put("team_id", eachObj.getString("opponent_team_id"));
                        opponent_team.put("team_name", eachObj.getString("opponent_team_name"));
                        match_info.put("opponent_team", opponent_team);

                        response.put("match_info", match_info);
                    } catch (JSONException e) {
                        Logger.e("Json exception", e.getMessage());
                    }

                    String chattingMatchId = helper.getString(ChatActivity.chattingMatchId, "null");

                    if (chattingMatchId.equalsIgnoreCase(eachObj.getString("match_id")) &&
                            !eachObj.getString("match_id").equals("0")) {
                        Logger.e("here", "condition entered.");
                        return false;
                    }
                    if (eachObj.getString("game_image").equals("null")) {
                        messageModel.setGameImg(eachObj.getString("default_profile_img_url"));
                    } else {
                        messageModel.setGameImg(eachObj.getString("game_image"));
                    }
                    intent = new Intent(getApplicationContext(), ChatActivity.class);
                    intent.putExtra("response", response.toString());
                    intent.putExtra("fromPush", true);
                    intent.putExtra("noti_id", dataMap.get("note_id"));
                    break;

                case Config.MESSAGE_PLAYER_NOTI:
                    String friendId = helper.getString(ChatActivity.chattingMatchId, "null");
                    if (friendId.equalsIgnoreCase(eachObj.getString("friend_user_id")) &&
                            !eachObj.getString("friend_user_id").equals("0")) {
                        Logger.e("here", "condition entered.");
                        return false;
                    }

                    if (eachObj.getString("sender_profile").equals("null")) {
                        messageModel.setGameImg(eachObj.getString("default_profile_img_url"));
                    } else {
                        messageModel.setGameImg(eachObj.getString("sender_profile"));
                    }
                    UserModel friendMdl = new UserModel();
                    friendMdl.setUserId(eachObj.getString("friend_user_id"));
                    intent = new Intent(getApplicationContext(), ChatActivity.class);
                    intent.putExtra("friend", friendMdl);
                    intent.putExtra("fromPush", true);
                    intent.putExtra("noti_id", dataMap.get("note_id"));
                    break;

                case Config.WIN_NOTIFICATION:
//                    UserTicketBalanceUpdate.getTicketBalanceTask(MyFirebaseMessagingService.this);
                    helper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, eachObj.getString("balance")).apply();
                    Logger.e("sending Broadcast ***************", "\n BoroadCast Sent !!!!!!!!!!!!!!");
                    sendBroadcast(new Intent(MainActivity.BALANCE_UPDATE_ACTION));
                    return false;

                default:
                    return false;
            }

            intent.putExtra("mesg", messageModel);
            intent.putExtra("from_push", true);
            intent.putExtra("type", notify_type);
            startActivity(intent);
        } catch (Exception e) {
            Logger.e("Splash From Noti" + "data2 ex", e.getMessage() + " ");
        }

        return true;
    }
}
