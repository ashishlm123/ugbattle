package com.emts.ugbattle.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by theone on 7/12/2016.
 */
public class PayPalPaymentActivity extends AppCompatActivity {
    WebView webView;
    ProgressDialog progressDialog;

    String successUri = Api.getInstance().successUrl;
    String cancelUri = Api.getInstance().cancelUrl;

    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_paypal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Checkout");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressDialog = new ProgressDialog(PayPalPaymentActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());

        webView = findViewById(R.id.payment_webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setJavaScriptEnabled(true);

        // Register a new JavaScript interface called HTMLOUT /
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");

        webView.setWebViewClient(new WebViewClientPaymentGateway());

        CookieSyncManager.createInstance(PayPalPaymentActivity.this);
        CookieSyncManager.getInstance().sync();

        CookieManager.getInstance().removeAllCookie();
        webView.clearCache(true);

        String postParams = "";
        Intent intent = getIntent();
        if (intent != null) {
            postParams = intent.getStringExtra("post_params");
        }
        Logger.e("Payment Post Params:", postParams);

        String url = "";
        url = Api.getInstance().ticketCheckOut;
        webView.postUrl(url, postParams.getBytes());
    }

    private void removeSessionCookies() {
        CookieManager.getInstance().removeSessionCookie();
        CookieSyncManager.getInstance().sync();
    }

    @Override
    protected void onPause() {
        CookieSyncManager.getInstance().stopSync();
        super.onPause();
    }

    protected void onResume() {
        CookieSyncManager.getInstance().startSync();
        super.onResume();
    }

    private void showAlert(String message, final boolean ok) {
        AlertUtils.showAlertMessage(PayPalPaymentActivity.this, "", message, "OK", "",
                new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            if (ok) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("paid", true);
                                setResult(RESULT_OK, returnIntent);
                            }
                            finish();
                        }
                    }
                });
    }

    private class WebViewClientPaymentGateway extends WebViewClient {
        WebViewClientPaymentGateway() {
        }

        public void onPageFinished(WebView paramWebView, String paramString) {
            progressDialog.dismiss();
//            // This call inject JavaScript into the page which just finished loading.
//            if (isBuyBidCredit && paramString.contains(Api.getInstance().getIpGeneral())) {
//                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//            }
        }

        public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap) {
            Logger.e("onPageStarted", paramString);
            if (paramString.contains("")) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {
                        }
                    }
                }, 4000);

            }
            if (paramString.contains(successUri)) {
                progressDialog.dismiss();
                webView.stopLoading();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Api.getInstance().getUserDetailTask(PayPalPaymentActivity.this, null);
//                    }
//                }, 0); // due to the delay  in update

                showAlert("Payment Success", true);
            } else if (paramString.contains(cancelUri)) {
                progressDialog.dismiss();
                webView.stopLoading();
                showAlert("Payment Canceled", false);
                webView.setVisibility(View.GONE);
            }
            super.onPageStarted(paramWebView, paramString, paramBitmap);
        }

        public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2) {
            Logger.e("Error", "Any Error?");
        }
    }

    class MyJavaScriptInterface {
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processHTML(String html) {
            // process the html as needed by the app
            Logger.e("full html content", html + "");

            String jsonOnly = "{" + html.substring(html.indexOf(">{") + 2, html.indexOf("}</pre>")) + "}";
            Logger.e("json only", jsonOnly);

            try {
                final JSONObject resObj = new JSONObject(jsonOnly);
                if (!resObj.getBoolean("status")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                webView.setVisibility(View.GONE);
                                AlertUtils.showAlertMessage(PayPalPaymentActivity.this, "",
                                        resObj.getString("message"), "OK", "",
                                        new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                    onBackPressed();
                                                }
                                            }
                                        });
                            } catch (Exception e) {
                                Logger.e("ex label 23432", e.getMessage() + "");
                            }
                        }
                    });
                }
            } catch (JSONException ex) {
                Logger.e("error parsing html", ex.getMessage() + "");
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
