package com.emts.ugbattle.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.GameAdapter;
import com.emts.ugbattle.customviews.EndlessRecyclerViewScrollListener;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2018-03-19.
 */

public class GameListingFragment extends Fragment {
    RecyclerView moreGameListings;
    TextView errorText;
    ProgressBar progressBar, infiniteProgress;
    ArrayList<GameMdl> gameLists;
    GameAdapter gameAdapter;
    ParentActivityInterface openSpecificFragment;

    private int limit = 20;
    private int offset = 0;

    EndlessRecyclerViewScrollListener infiniteScrollListener;
    SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gameLists = new ArrayList<>();

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);

        errorText = view.findViewById(R.id.errorText);
        progressBar = view.findViewById(R.id.progress);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);

        moreGameListings = view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        moreGameListings.setLayoutManager(linearLayoutManager);
        gameAdapter = new GameAdapter(getActivity(), gameLists);
        gameAdapter.setGrid(false);
        moreGameListings.setAdapter(gameAdapter);
        moreGameListings.setNestedScrollingEnabled(false);
        gameAdapter.setRecyclerItemClickListener(new GameAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position) {
                openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.GAME_DETAIL_PAGE_ID, gameLists.get(position));
            }
        });

        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (gameLists.size() >= limit) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        moreGameListingTask(offset);
                    } else {
                        AlertUtils.showSnack(getActivity(), moreGameListings, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        };
        moreGameListings.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            moreGameListingTask(offset);
        } else {
            errorText.setText(getResources().getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            moreGameListings.setVisibility(View.GONE);
        }
    }


    private void moreGameListingTask(int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
            moreGameListings.setVisibility(View.GONE);
            errorText.setVisibility(View.GONE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offsetValue));

        vHelper.addVolleyRequestListeners(Api.getInstance().gameListingsUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                String gameImgDir = res.getString("game_image_url");
                                JSONArray gamesArray = res.getJSONArray("games");

                                if (gamesArray.length() == 0) {
                                    if (offset == 0) {
                                        errorText.setText("No games");
                                        errorText.setVisibility(View.VISIBLE);
                                        moreGameListings.setVisibility(View.GONE);
                                    } else {
                                        AlertUtils.showSnack(getActivity(), errorText, "No more games");
                                    }
                                } else {
                                    if (offset == 0) {
                                        gameLists.clear();
                                    }

                                    for (int i = 0; i < gamesArray.length(); i++) {
                                        JSONObject eachGames = gamesArray.getJSONObject(i);

                                        GameMdl gameMdl = new GameMdl();
                                        gameMdl.setGameId(eachGames.getString("game_id"));
                                        gameMdl.setGameName(eachGames.getString("game_name"));
                                        String gameImg = eachGames.getString("game_image");
                                        if (!TextUtils.isEmpty(gameImg)) {
                                            gameMdl.setGameImg(gameImgDir + gameImg);
                                        } else {
                                            gameMdl.setGameImg("");
                                        }
                                        gameMdl.setGameDescription(eachGames.getString("description"));
                                        gameMdl.setGameServersArray(eachGames.getString("server_names"));
                                        gameMdl.setGameUserId(eachGames.getString("game_user_id"));
                                        gameMdl.setGameUserServer(eachGames.getString("game_user_server"));

                                        gameLists.add(gameMdl);
                                    }
                                    offset = gameLists.size();
                                    gameAdapter.notifyDataSetChanged();
                                    moreGameListings.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                }
                            } else {
                                if (offset == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    moreGameListings.setVisibility(View.GONE);
                                } else {
                                    AlertUtils.showSnack(getActivity(), errorText, " No more Games...");
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("moreGameListingTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("moreGameListingTask error res", e.getMessage() + " ");
                        }
                        if (offset == 0) {
                            errorText.setText(errorMsg);
                            errorText.setVisibility(View.VISIBLE);
                            moreGameListings.setVisibility(View.GONE);
                        } else {
                            AlertUtils.showSnack(getActivity(), moreGameListings, errorMsg);
                        }
                    }
                }, "moreGameListingTask");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificFragment = null;
        VolleyHelper.getInstance(getActivity()).cancelRequest("moreGameListingTask");
        super.onDetach();
    }
}
