package com.emts.ugbattle.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.ChatActivity;
import com.emts.ugbattle.adapter.FragmentPageAdapter;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TournamentModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class OnGoingChallengeFrag extends Fragment {
    TournamentModel gameMdl;
    TabLayout tabLayout;
    ProgressBar progressBar;
    CoordinatorLayout detailHolder;
    TextView errorText;
    ViewPager viewPager;

    String jsonResponse = "";

    FragmentPageAdapter adapter;

    boolean isGameFinished = false;
    Button btnChat;
    TextView tvStatus;
    TextView tvDate;
    boolean isHistory = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            gameMdl = (TournamentModel) bundle.getSerializable("game");
            isHistory = bundle.getBoolean("isHistory");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ongoing_challenge, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress);
        errorText = view.findViewById(R.id.errorText);

        detailHolder = view.findViewById(R.id.ongoing_content);

        TextView tvGameName = view.findViewById(R.id.tv_game_name);
        tvGameName.setText("On-going game: " + gameMdl.getGameName());
        tvDate = view.findViewById(R.id.tv_date);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        tvDate.setText("Date: " + sdf.format(new Date()));
        tvStatus = view.findViewById(R.id.tv_status);
        tvStatus.setText("Status: Pending");

        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout.setupWithViewPager(viewPager);

        if (NetworkUtils.isInNetwork(getActivity())) {
            getMatchInfoTask(isHistory);
        } else {
            progressBar.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            detailHolder.setVisibility(View.GONE);
        }

        btnChat = view.findViewById(R.id.btnChat);
        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                intent.putExtra("tournament", gameMdl);
                intent.putExtra("response", jsonResponse);
                startActivity(intent);
            }
        });

    }

    private void addTabs(String response) {
        adapter = new FragmentPageAdapter(getChildFragmentManager());

        Fragment myTeamFrag = new TwoTeamFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("game", gameMdl);
        bundle.putBoolean("isMyTeam", true);
        bundle.putString("response", response);
        myTeamFrag.setArguments(bundle);
        adapter.addFrag(myTeamFrag, "Your Team");

        Fragment opponentTeamFrag = new TwoTeamFragment();
        Bundle bundle1 = new Bundle();
//        bundle1.putSerializable("game", gameMdl);
        bundle1.putBoolean("isMyTeam", false);
        bundle1.putString("response", response);
        opponentTeamFrag.setArguments(bundle1);
        adapter.addFrag(opponentTeamFrag, "Opponent Team");

        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);

        detailHolder.setVisibility(View.VISIBLE);
    }

    private void getMatchInfoTask(boolean isHistory) {
        errorText.setVisibility(View.GONE);
        detailHolder.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_id", gameMdl.getGameId());
        postMap.put("team_id", gameMdl.getTeamId());
        postMap.put("tourn_id", gameMdl.getTrnmntId());
        postMap.put("current_round", String.valueOf(gameMdl.getCurrentRoundNo()));

        String url = "";
        if (isHistory) {
            url = Api.getInstance().getHistoryMatchInfoUrl;
        } else {
            url = Api.getInstance().getMatchInfoUrl;
        }

        vHelper.addVolleyRequestListeners(url, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        jsonResponse = response;
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject matchInfo = res.getJSONObject("match_info");
                                isGameFinished = !matchInfo.getString("status").equalsIgnoreCase("ongoing");
                                if (isGameFinished) {
                                    tvDate.setText("Date: " + matchInfo.getString("completed_date"));
                                    tvStatus.setText("Status: Completed");
                                    btnChat.setVisibility(View.GONE);
                                } else {
                                    tvDate.setText("Date: " + matchInfo.getString("created_date"));
                                    tvStatus.setText("Status: Pending");
                                    btnChat.setVisibility(View.VISIBLE);
                                }
                                addTabs(response);
                            } else {
                                detailHolder.setVisibility(View.GONE);
                                errorText.setText(Html.fromHtml(res.getString("message")));
                                errorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("getMatchInfoTask json ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getMatchInfoTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(Html.fromHtml(errorMsg));
                        errorText.setVisibility(View.VISIBLE);
                        detailHolder.setVisibility(View.GONE);
                    }
                }, "getMatchInfoTask");
    }

    public void permissionGrant() {
        Fragment fragment = adapter.getItem(0);
        if (fragment instanceof TwoTeamFragment) {
            ((TwoTeamFragment) fragment).pickFromGallery();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        VolleyHelper.getInstance(getActivity()).cancelRequest("getMatchInfoTask");
    }
}
