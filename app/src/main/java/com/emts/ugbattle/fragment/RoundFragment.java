package com.emts.ugbattle.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.TeamsAdapter;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.TournamentModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoundFragment extends Fragment {
    TournamentModel gameMdl;
    TeamsAdapter teamsAdapter;
    ArrayList<TeamModel> teamLists = new ArrayList<>();
    ArrayList<TeamModel> filterLists = new ArrayList<>();
    RecyclerView teamListings;
    ProgressBar progressBar;
    TextView errorText;
    TextView myTeamName;

    ParentActivityInterface openSpecificFragment;

    SwipeRefreshLayout swipeRefreshLayout;
    PreferenceHelper preferenceHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            gameMdl = (TournamentModel) bundle.getSerializable("game");
        }
        preferenceHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rounds, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setEnabled(false);

        errorText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);

        teamListings = view.findViewById(R.id.team_listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        teamListings.setLayoutManager(linearLayoutManager);
        teamsAdapter = new TeamsAdapter(getActivity(), filterLists);
        teamListings.setAdapter(teamsAdapter);
        teamListings.setNestedScrollingEnabled(false);
        teamsAdapter.setClickListener(new TeamsAdapter.RecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClick(View view, int position) {
                if (view.getId() == R.id.btn_view_game) {
                    openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.ONGOING_CHALLENGE_PAGE, gameMdl);
                } else {
                    openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.TEAM_LISTS_FRAGMENTS_PAGE, position);
                }
            }
        });

        LinearLayout layNameRoundHolder = view.findViewById(R.id.lay_name_round_holder);
        layNameRoundHolder.setVisibility(View.GONE);
        myTeamName = view.findViewById(R.id.my_team_name);

        Button btnViewTeamInfo = view.findViewById(R.id.btn_view_team);
        btnViewTeamInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.e("iscliked", String.valueOf(true));
                openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.TEAM_LISTS_FRAGMENTS_PAGE, gameMdl.getTeamId());
            }
        });

        EditText edtFilterTeams = view.findViewById(R.id.edt_filter_teams);
        edtFilterTeams.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence)) {
                    filterLists.clear();
                    filterLists.addAll(teamLists);
                    teamsAdapter.notifyDataSetChanged();
                } else {
                    filterLists.clear();
                    for (int j = 0; j < teamLists.size(); j++) {
                        if (teamLists.get(j).getOpponentTeamName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            filterLists.add(teamLists.get(j));
                        }
                    }
                    teamsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            roundTeamListingTask();
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            teamListings.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    roundTeamListingTask();
                } else {
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                    teamListings.setVisibility(View.GONE);
                }
            }
        });
    }

    private void roundTeamListingTask() {
        errorText.setVisibility(View.GONE);
        teamListings.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_id", gameMdl.getGameId());
        postMap.put("team_id", gameMdl.getTeamId());
        postMap.put("tourn_id", gameMdl.getTrnmntId());
        postMap.put("round_no", String.valueOf(gameMdl.getCurrentRoundNo()));

        vHelper.addVolleyRequestListeners(Api.getInstance().getRoundTeamListUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        teamLists.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                boolean isMeLeader = false;
                                String imgDirectory = res.getString("game_image_url");
                                JSONObject myTeamInfo = res.getJSONObject("my_team_info");
                                myTeamName.setText(myTeamInfo.optString("team_name"));

                                JSONArray teamsArray = res.getJSONArray("teams");
                                for (int j = 0; j < teamsArray.length(); j++) {
                                    TeamModel teamModel = new TeamModel();
                                    JSONObject eachTeamObj = teamsArray.getJSONObject(j);

                                    if (myTeamInfo.has("team_id")) {
                                        Logger.e("here", "i am here");
                                        teamModel.setMyTeamId(myTeamInfo.getString("team_id"));
                                        teamModel.setMyTeamLeaderId(myTeamInfo.getString("creater_id"));
                                        teamModel.setMyTeamName(myTeamInfo.getString("team_name"));
                                        teamModel.setCurrentRound(myTeamInfo.getString("current_round_no"));
                                        gameMdl.setCurrentRoundNo(Integer.parseInt(teamModel.getCurrentRound()));
                                        if (teamModel.getMyTeamLeaderId().equalsIgnoreCase(preferenceHelper.getUserId())){
                                            isMeLeader = true;
                                        }
                                    }

                                    //opponent
                                    teamModel.setOpponentTeamId(eachTeamObj.getString("team_id"));
                                    teamModel.setGameId(eachTeamObj.getString("game_id"));
                                    teamModel.setGameUserId(eachTeamObj.getString("creater_game_user_id"));
                                    teamModel.setOpponentTeamName(eachTeamObj.getString("team_name"));
                                    teamModel.setOpponentTeamLeaderUserId(eachTeamObj.getString("creater_id"));
                                    teamModel.setChallengingTeamId(eachTeamObj.getString("challenging_team"));
                                    teamModel.setTrnmntId(eachTeamObj.getString("tournment_id"));
                                    teamModel.setTeamStatus(eachTeamObj.getString("is_challenged"));

                                    if (teamModel.getChallengingTeamId().equals(teamModel.getMyTeamId())
                                            && teamModel.getTeamStatus().equalsIgnoreCase(Config.IS_ACCEPTED)) {
                                        teamLists.clear();
                                        teamLists.add(teamModel);
                                        break;
                                    } else {
                                        if (teamModel.getChallengingTeamId().equals("0") ||
                                                teamModel.getChallengingTeamId().equals(teamModel.getMyTeamId())) {
                                            teamLists.add(teamModel);
                                        }
                                    }
                                }

                                if (teamLists.size() > 0) {
//                                    if (isMeLeader){
                                        swipeRefreshLayout.setEnabled(true);
//                                    }
                                    filterLists.clear();
                                    filterLists.addAll(teamLists);
                                    teamsAdapter.notifyDataSetChanged();
                                    teamListings.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    teamListings.setVisibility(View.GONE);
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("roundTeamListingTask json ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("roundTeamListingTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        teamListings.setVisibility(View.GONE);
                    }
                }, "roundTeamListingTask");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificFragment = null;
        super.onDetach();
    }
}
