package com.emts.ugbattle.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.UserTicketBalanceUpdate;
import com.emts.ugbattle.adapter.PlayerListsAdapter;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.TournamentModel;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.emitter.Emitter;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateJoinTeamFragment extends Fragment {
    ParentActivityInterface openSpecificFragment;

    TournamentModel gameMdl;

    ArrayList<UserModel> playerList;
    RecyclerView playerListView;
    PlayerListsAdapter adapter;
    LinearLayout inviteFriendHolder;
    LinearLayout infoHolder, createTeamHolder, holderInvite;

    ProgressBar mainProgress, listingProgres;

    TextView errorText, errorText1;
    TextView l1TvServerName, l1TvTourName, l1TvTeamName, l2TvServerName, l2TvTourName;

    PreferenceHelper prefsHelper;
    TeamModel team;
    Button btnStart, btnInvite;
    io.socket.client.Socket socketJs;

    SwipeController swipeController;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            gameMdl = (TournamentModel) bundle.getSerializable("game");
        }
        prefsHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_join_team, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    showTeamNameTask();
                } else {
                    AlertUtils.showSnack(getActivity(), playerListView, getResources().getString(R.string.error_no_internet));
                }
            }
        });
        infoHolder = view.findViewById(R.id.lay_start_game_asteam_member);

        createTeamHolder = view.findViewById(R.id.lay_create_team_before_invite);

        inviteFriendHolder = view.findViewById(R.id.lay_invite_friend);

        playerListView = view.findViewById(R.id.player_listings);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        playerListView.setLayoutManager(layoutManager);
        playerList = new ArrayList<>();
        adapter = new PlayerListsAdapter(getActivity(), playerList);
        playerListView.setAdapter(adapter);
        playerListView.setNestedScrollingEnabled(false);

        //swipe helper
        swipeController = new SwipeController(new SwipeController.SwipeControllerActions() {
            @Override
            public void onRightClicked(final int position) {
                if (prefsHelper.getUserId().equals(team.getMyTeamLeaderId())) {
                    AlertUtils.showAlertMessage(getActivity(), "Remove Player ??",
                            "Are you sure you want to remove the player from your team?",
                            "YES", "NO", new AlertUtils.OnAlertButtonClickListener() {
                                @Override
                                public void onAlertButtonClick(boolean isPositiveButton) {
                                    if (isPositiveButton) {
                                        leaveTeamTask(position);
                                    }
                                }
                            });
                } else {
                    AlertUtils.showAlertMessage(getActivity(), "Leave Team ??", "Are you sure you want to leave the team?",
                            "YES", "NO", new AlertUtils.OnAlertButtonClickListener() {
                                @Override
                                public void onAlertButtonClick(boolean isPositiveButton) {
                                    if (isPositiveButton) {
                                        leaveTeamTask(position);
                                    }
                                }
                            });
                }
            }
        });
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
        itemTouchHelper.attachToRecyclerView(playerListView);
        playerListView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });

        final Button btnCreate = view.findViewById(R.id.btn_create);

        btnInvite = view.findViewById(R.id.btn_invite_player);
        btnStart = view.findViewById(R.id.btn_start);
        btnStart.setEnabled(false);

        l1TvServerName = view.findViewById(R.id.l1_tv_server_name);
        l1TvTourName = view.findViewById(R.id.l1_tv_tour_name);
        l1TvTeamName = view.findViewById(R.id.l1_tv_team_name);
        l1TvServerName.setText("Server : " + gameMdl.getGameServersArray());
        l1TvTourName.setText("Tournament : " + gameMdl.getTrnmntName());

        l2TvServerName = view.findViewById(R.id.l2_tv_server_name);
        l2TvTourName = view.findViewById(R.id.l2_tv_tournament_name);
        l2TvServerName.setText("Server : " + gameMdl.getGameServersArray());
        l2TvTourName.setText("Tournament : " + gameMdl.getTrnmntName());

        final EditText edtTeamName = view.findViewById(R.id.l2_edt_team_name);
        errorText = view.findViewById(R.id.error_text);

        mainProgress = view.findViewById(R.id.main_progress_bar);
        errorText1 = view.findViewById(R.id.error_text1);
        listingProgres = view.findViewById(R.id.progress_bar);

        infoHolder.setVisibility(View.GONE);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(edtTeamName.getText().toString().trim())) {
                    errorText.setVisibility(View.GONE);
                    AlertUtils.hideInputMethod(getActivity(), errorText);
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        createTeamTask(edtTeamName.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(getActivity(), edtTeamName, getResources().getString(R.string.error_no_internet));
                    }
                } else {
                    errorText.setVisibility(View.VISIBLE);
                }
            }
        });

        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (team == null) {
                    openSpecificFragment.goBack();
                } else {
                    openSpecificFragment.openSpecificPageWithData(view.getId(), team);
                }
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    startGameTask();
                } else {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.error_no_internet));
                }
            }
        });

        adapter.setRecyclerItemClickListener(new PlayerListsAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    cancelAlreadySendInvitationTask(position);
                } else {
                    AlertUtils.showSnack(getActivity(), edtTeamName, getResources().getString(R.string.error_no_internet));
                }
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Logger.e("onActivityCreated", "is Added to activity?" + isAdded()
                + "\nbtnInvite isNull:?" + (btnInvite == null));
        if (NetworkUtils.isInNetwork(getActivity())) {
            showTeamNameTask();
        } else {
            AlertUtils.showSnack(getActivity(), playerListView, getResources().getString(R.string.error_no_internet));
        }
    }

    private void cancelAlreadySendInvitationTask(final int pos) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("tourn_id", team.getTrnmntId());
        postMap.put("team_id", team.getTeamId());
        postMap.put("game_id", team.getGameId());
        postMap.put("friend_user_id", playerList.get(pos).getUserId());
        postMap.put("creater_id", prefsHelper.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().cancelTeamInvitation, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                playerList.remove(pos);
                                adapter.notifyItemRemoved(pos);
                                team.setInvitationCount(team.getInvitationCount() - 1);

                                if (playerList.size() >= 11) {
                                    btnInvite.setEnabled(false);
                                    btnInvite.setBackgroundResource(R.drawable.rounded_grey);
                                } else {
                                    btnInvite.setEnabled(true);
                                    btnInvite.setBackgroundResource(R.drawable.rounded_blue);
                                }

                            } else {
                                AlertUtils.showSnack(getActivity(), playerListView, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("cancelAlreadySendInvitationTask json ex: ", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("cancelAlreadySendInvitationTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), errorText, errorMsg);
                    }
                }, "cancelAlreadySendInvitationTask");
    }

    private void startGameTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("tourn_id", gameMdl.getTrnmntId());
        postMap.put("team_id", gameMdl.getTeamId());
        postMap.put("game_id", gameMdl.getGameId());

        vHelper.addVolleyRequestListeners(Api.getInstance().statGameUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        mainProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.ROUND_START_FRAG_ID, null);
                            } else {
                                AlertUtils.showAlertMessage(getActivity(), "Error!", res.getString("message"),
                                        "OK", "Cancel", null);
                            }
                        } catch (
                                JSONException e)

                        {
                            Logger.e("startGameTask json ex: ", e.getMessage());
                            String errorMessage = "Unexpected error !!! Please try again with internet access.";
                            try {
                                JSONObject res = new JSONObject(response);
                                errorMessage = res.getString("message");
                            } catch (JSONException e1) {
                                Logger.e("inside catch startGameTask json ex: ", e.getMessage());
                            }
                            AlertUtils.showAlertMessage(getActivity(), "Error !!!", errorMessage);
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("showTeamNameTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), errorText, errorMsg);
                    }
                }, "showTeamNameTask");
    }

    public void createTeamTask(final String teamName) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(getActivity(), "Please Wait...");
        dialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_user_id", gameMdl.getGameUserId());
        postMap.put("tourn_id", gameMdl.getTrnmntId());
        postMap.put("team_name", teamName);
        postMap.put("game_id", gameMdl.getGameId());
        postMap.put("server_name", gameMdl.getGameServersArray());

        vHelper.addVolleyRequestListeners(Api.getInstance().createJoinTeam, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                //For Team Data
                                JSONObject teamData = res.getJSONObject("my_team");
                                team = new TeamModel();
                                team.setTeamId(teamData.getString("team_id"));
                                gameMdl.setTeamId(teamData.getString("team_id"));
                                team.setMyTeamName(teamData.getString("team_name"));
                                team.setGameId(teamData.getString("game_id"));
                                team.setTrnmntId(teamData.getString("tournment_id"));
                                team.setCurrentRound(teamData.getString("current_round_no"));
                                team.setMyTeamLeaderId(teamData.getString("creater_id"));
                                team.setMyGameUserId(teamData.getString("creater_game_user_id"));
                                team.setNumOfTeamsEntered(res.getString("no_of_team_entered"));

                                AlertUtils.showAlertMessage(getActivity(), false, "Success",
                                        res.getString("message"), "Ok", "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                            }
                                        });
                                UserModel userModel = new UserModel();
                                userModel.setGameUserId(gameMdl.getGameUserId());
                                userModel.setUserName(prefsHelper.getString(PreferenceHelper.APP_USER_NAME, ""));
                                userModel.setUserId(prefsHelper.getString(PreferenceHelper.APP_USER_ID, ""));
                                userModel.setUserProfilePic(prefsHelper.getString(PreferenceHelper.APP_USER_IMG, ""));
                                userModel.setUserMemberStatus(Config.LEADER);
                                playerList.add(userModel);
                                adapter.notifyDataSetChanged();
                                playerListView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);

                                l1TvServerName.setText("Server : " + gameMdl.getGameServersArray());
                                l1TvTourName.setText("Tournament : " + gameMdl.getTrnmntName());
                                l1TvTeamName.setText("Team Name : " + teamName);
                                infoHolder.setVisibility(View.VISIBLE);
                                createTeamHolder.setVisibility(View.GONE);
                                inviteFriendHolder.setVisibility(View.VISIBLE);

                                try {
                                    connectToServerJs();
                                } catch (URISyntaxException e) {
                                    Logger.e("socket ex", " u here");
                                }

                                UserTicketBalanceUpdate.getTicketBalanceTask(getActivity());

                            } else {
                                dialog.dismiss();
                                AlertUtils.showAlertMessage(getActivity(), "Error", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            Logger.e("createTeamTask json ex", e.getMessage());
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("createTeamTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), errorText, errorMsg);
                    }
                }, "createTeamTask");
    }

    private void leaveTeamTask(final int position) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        postParams.put("game_id", gameMdl.getGameId());
        postParams.put("tourn_id", gameMdl.getTrnmntId());
        postParams.put("team_id", gameMdl.getTeamId());
        postParams.put("member_id", playerList.get(position).getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().leaveTeam, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                if (playerList.get(position).getUserId().equals(prefsHelper.getUserId())) {
                                    UserTicketBalanceUpdate.getTicketBalanceTask(getActivity());
                                    openSpecificFragment.goBack();
                                } else {
                                    playerList.remove(position);
                                    adapter.notifyDataSetChanged();

                                    //check if start game
                                    if (playerList.size() >= 10) {
                                        btnInvite.setEnabled(false);
                                        btnInvite.setBackgroundResource(R.drawable.rounded_grey);
                                    } else {
                                        btnInvite.setEnabled(true);
                                        btnInvite.setBackgroundResource(R.drawable.rounded_blue);
                                    }
//                                    if (acceptedPlayerCount == 5) {
//                                        btnStart.setEnabled(true);
//                                        btnStart.setBackgroundResource(R.drawable.rounded_blue);
//                                        btnInvite.setClickable(false);
//                                        btnInvite.setBackgroundResource(R.drawable.rounded_grey);
//                                    } else {
                                    btnStart.setEnabled(false);
                                    btnStart.setBackgroundResource(R.drawable.rounded_grey);
//                                    }
                                }

                            } else {
                                AlertUtils.showSnack(getActivity(), playerListView, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("leaveTeamTask json ex: ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("leaveTeamTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), playerListView, errorMsg);
                    }
                }, "leaveTeamTask");
    }

    private void showTeamNameTask() {
        mainProgress.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
//        postMap.put("game_user_id", gameMdl.getGameUserId());
        postMap.put("tourn_id", gameMdl.getTrnmntId());
        postMap.put("team_id", gameMdl.getTeamId());
        postMap.put("game_id", gameMdl.getGameId());

        vHelper.addVolleyRequestListeners(Api.getInstance().showTeamName, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        mainProgress.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                playerList.clear();
                                //For Team Data
                                JSONObject teamData = res.getJSONObject("my_team");
                                if (teamData.length() != 0) {
                                    team = new TeamModel();
                                    team.setTeamId(teamData.getString("team_id"));
                                    team.setMyTeamName(teamData.getString("team_name"));
                                    team.setGameId(teamData.getString("game_id"));
                                    team.setTrnmntId(teamData.getString("tournment_id"));
                                    team.setCurrentRound(teamData.getString("current_round_no"));
                                    team.setMyTeamLeaderId(teamData.getString("creater_id"));
                                    team.setMyGameUserId(teamData.getString("creater_game_user_id"));

                                    createTeamHolder.setVisibility(View.GONE);
                                    l1TvTeamName.setText("Team Name : " + team.getMyTeamName());
                                    l1TvServerName.setText("Server : " + gameMdl.getGameServersArray());
                                    l1TvTourName.setText("Tournament : " + gameMdl.getTrnmntName());
                                    infoHolder.setVisibility(View.VISIBLE);

                                    //For Player Listings Under Invite and Start Button
                                    String defaultImg = res.getString("default_img_path");
                                    String imgDir = res.getString("user_img_base_url");
                                    JSONArray playerArray = res.getJSONArray("player_list");
                                    int acceptedPlayerCount = 1;

                                    ArrayList<UserModel> memberLists = new ArrayList<>();
                                    ArrayList<UserModel> awaitingLists = new ArrayList<>();
                                    ArrayList<UserModel> rejectedLists = new ArrayList<>();
                                    for (int i = 0; i < playerArray.length(); i++) {
                                        JSONObject eachPlayer = playerArray.getJSONObject(i);
                                        UserModel playerModel = new UserModel();
                                        playerModel.setUserId(eachPlayer.getString("user_id"));
                                        playerModel.setGameId(eachPlayer.getString("game_id"));
                                        playerModel.setGameUserId(eachPlayer.getString("game_user_id"));
                                        playerModel.setGameServersArray(eachPlayer.getString("server_name"));
                                        playerModel.setUserName(eachPlayer.getString("username"));
                                        playerModel.setUserEmail(eachPlayer.getString("email"));
                                        playerModel.setUserFullName(eachPlayer.getString("full_name"));

                                        String playerImg = eachPlayer.getString("profile_img");
                                        if (playerImg.equalsIgnoreCase("null") || TextUtils.isEmpty(playerImg)) {
                                            playerModel.setUserProfilePic(defaultImg);
                                        } else {
                                            playerModel.setUserProfilePic(imgDir + playerImg);
                                        }
                                        playerModel.setUserMemberStatus(eachPlayer.getString("inv_resp_status"));
                                        if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.LEADER)) {
                                            playerList.add(0, playerModel);
                                        } else {
                                            if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.IS_AWAITING)) {
                                                awaitingLists.add(playerModel);
                                            } else if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.Accepted)) {
                                                memberLists.add(playerModel);
                                            } else if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.IS_REJECTED)) {
                                                rejectedLists.add(playerModel);
                                            }
                                            if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.Accepted)) {
                                                acceptedPlayerCount++;
                                            }
                                        }
                                    }
                                    team.setInvitationCount(playerList.size());
                                    adapter.setTeam(team);
                                    //group here
                                    Comparator<UserModel> myComparator = new Comparator<UserModel>() {
                                        @Override
                                        public int compare(UserModel lhs, UserModel rhs) {
                                            return lhs.getUserName().compareTo(rhs.getUserName());
                                        }
                                    };

                                    Collections.sort(memberLists, myComparator);
                                    Collections.sort(awaitingLists, myComparator);
                                    Collections.sort(rejectedLists, myComparator);
                                    playerList.addAll(memberLists);
                                    playerList.addAll(awaitingLists);
                                    playerList.addAll(rejectedLists);

                                    adapter.notifyDataSetChanged();
                                    playerListView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);

                                    ArrayList<Integer> swipeDisablePositions = new ArrayList<>();
                                    int myIdPosition = -1;
                                    for (int i = 0; i < playerList.size(); i++) {
                                        if (prefsHelper.getUserId().equals(team.getMyTeamLeaderId())) {
                                            if (!playerList.get(i).getUserMemberStatus().equals(Config.Accepted)) {
                                                swipeDisablePositions.add(i);
                                            }
                                        } else {
                                            swipeDisablePositions.add(i);
                                            if (playerList.get(i).getUserId().equalsIgnoreCase(prefsHelper.getUserId())) {
                                                myIdPosition = i;
                                            }
                                        }
                                    }
                                    if (prefsHelper.getUserId().equals(team.getMyTeamLeaderId())) {
                                        openSpecificFragment.setTitle("Create/Join Team");
                                        inviteFriendHolder.setVisibility(View.VISIBLE);
                                        //check if start game
                                        if (playerList.size() >= 10) {
                                            btnInvite.setEnabled(false);
                                            btnInvite.setBackgroundResource(R.drawable.rounded_grey);
                                        } else {
                                            btnInvite.setBackgroundResource(R.drawable.rounded_blue);
                                            btnInvite.setEnabled(true);
                                        }
                                        if (acceptedPlayerCount == 5) {
                                            btnStart.setEnabled(true);
                                            btnStart.setBackgroundResource(R.drawable.rounded_blue);
                                            btnInvite.setEnabled(false);
                                            btnInvite.setBackgroundResource(R.drawable.rounded_grey);
                                        } else {
                                            btnStart.setEnabled(false);
                                            btnStart.setBackgroundResource(R.drawable.rounded_grey);
                                        }
                                    } else {
                                        inviteFriendHolder.setVisibility(View.GONE);
                                        if (myIdPosition >= 0) {
                                            swipeDisablePositions.remove(myIdPosition);
                                        }
                                    }
                                    swipeController.setDisableSwipePosition(swipeDisablePositions,
                                            prefsHelper.getUserId().equals(team.getMyTeamLeaderId()));
                                }
                            }
                            swipeRefreshLayout.setEnabled(true);
                        } catch (JSONException e) {
                            Logger.e("showTeamNameTask json ex: ", e.getMessage());
                            try {
                                JSONObject res = new JSONObject(response);
                                if (res.getBoolean("status")) {
                                    JSONArray teamName = res.getJSONArray("my_team");
                                    if (teamName.length() == 0) {
                                        infoHolder.setVisibility(View.GONE);
                                        openSpecificFragment.setTitle("Create/Join Team");
                                        createTeamHolder.setVisibility(View.VISIBLE);
                                        inviteFriendHolder.setVisibility(View.GONE);
                                        swipeRefreshLayout.setEnabled(false);
                                    }
                                }
                            } catch (JSONException e1) {
                                Logger.e("inside catch showTeamNameTask json ex: ", e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        swipeRefreshLayout.setRefreshing(false);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("showTeamNameTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), errorText, errorMsg);
                    }
                }, "showTeamNameTask");
    }


    private void connectToServerJs() throws URISyntaxException {
        if (socketJs != null) {
            if (socketJs.connected()) {
                socketJs.off();
                socketJs.disconnect();
            }
        }
        String nodeServerHostUrl = prefsHelper.getString(PreferenceHelper.NODE_SEVER, Api.getInstance().nodeUrl)
                + ":" + prefsHelper.getString(PreferenceHelper.NODE_PORT, Api.getInstance().nodePort);
        socketJs = IO.socket(nodeServerHostUrl);
        Logger.e("nodeJs url", nodeServerHostUrl);
        socketJs.on(io.socket.client.Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("nodeJs def connect call", "Object : ");

                if (socketJs.connected()) {
                    Logger.e("nodeJs connected", socketJs.toString() + "");
                    emitConnectAndJoinTeamRoom(prefsHelper.getUserId(), team.getGameId(), gameMdl.getTeamId(), team.getNumOfTeamsEntered());
                } else {
                    try {
                        connectToServerJs();
                    } catch (URISyntaxException e) {
                        Logger.e("socket_ex", e.getMessage() + " ");
                    }
                }

            }

        }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("nodeJs def. disconnect", "Object " + args.length);
//                connectionConfirm = false;
            }
        });
        socketJs.connect();
    }

    //    START NODE JS LISTENERS
    public void emitConnectAndJoinTeamRoom(String hostId, String game_id, String team_id, String num_of_team_entered) {
        if (socketJs.connected()) {
            Logger.e("nodeJs emit for connected and join team room", "emitter for socket connected and join to team room");
            //emitter to post data
            try {
                JSONObject roomObj = new JSONObject();
                roomObj.put("user_id", hostId);
                roomObj.put("game_id", game_id);
                roomObj.put("team_id", team_id);
                roomObj.put("tourn_id", gameMdl.getTrnmntId());
                roomObj.put("num_of_team_entered", num_of_team_entered);


                Logger.e("nodeJs auction_room emitter data", roomObj.toString() + " **");

                socketJs.emit("send_team_join_information", roomObj, new Ack() {
                    @Override
                    public void call(Object... args) {
                        Logger.e("nodeJs auction_room emit ack", "Ack received for auction_room :" +
                                (args.length > 0 ? args[0].toString() : "0"));
                    }
                });
            } catch (JSONException e) {
                Logger.e("nodeJs ex15", e.getMessage() + "");
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
        Logger.e("CreateJoinTeamFragment onAttach()", "fragment attached");
    }

    @Override
    public void onDetach() {
        cancelVolleyTask();
        openSpecificFragment = null;
        super.onDetach();
    }

    @Override
    public void onStop() {
        cancelVolleyTask();
        super.onStop();
    }

    public void cancelVolleyTask() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("showTeamNameTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("leaveTeamTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("createTeamTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("leaveTeamTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("startGameTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("cancelAlreadySendInvitationTask");
    }
}
