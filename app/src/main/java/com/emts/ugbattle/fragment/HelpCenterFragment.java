package com.emts.ugbattle.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.ForgotPasswordActivity;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class HelpCenterFragment extends Fragment {
    Button btnSend;
    EditText messageBox;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_help_center, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        messageBox = view.findViewById(R.id.edt_message_box);
        btnSend = view.findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtils.hideInputMethod(getActivity(), messageBox);
                if (TextUtils.isEmpty(messageBox.getText().toString().trim())) {
                    AlertUtils.showSnack(getActivity(), view, "Please enter some message to send.");
                    return;
                }
                if (NetworkUtils.isInNetwork(getActivity())) {
                    helpTask();
                } else {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.error_no_internet));
                }
            }
        });
    }

    private void helpTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Sending message...");

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("message", messageBox.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().helpUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                messageBox.setText("");
                                AlertUtils.showAlertMessage(getActivity(), "Success",
                                        res.getString("message"), "OK",
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {

                                                }
                                            }
                                        });
                            } else {
                                AlertUtils.showAlertMessage(getActivity(), "Error",
                                        res.getString("message"), "OK",
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {

                                                }
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            Logger.e("helpTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("helpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), btnSend, errorMsg);
                    }
                }, "helpTask");
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("helpTask");
        super.onPause();
    }
}
