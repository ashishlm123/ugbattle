package com.emts.ugbattle.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.UserTicketBalanceUpdate;
import com.emts.ugbattle.adapter.InvitationAdapter;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.InvitationModel;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.TournamentModel;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class InvitationFragment extends Fragment {
    RecyclerView invitationListings;
    ProgressBar progressBar, infiniteProgress;
    TextView errorText;

    InvitationAdapter invitationAdapter;
    ArrayList<InvitationModel> invitationLists = new ArrayList<>();

    InvitationCount invitationCount;
    GameMdl gameMdl;

    ParentActivityInterface openSpecificFragment;
    PreferenceHelper preferenceHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            gameMdl = (GameMdl) bundle.getSerializable("game");
        }
        preferenceHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_invitation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);
        errorText = view.findViewById(R.id.error_text);

        invitationListings = view.findViewById(R.id.listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        invitationListings.setLayoutManager(linearLayoutManager);
        invitationAdapter = new InvitationAdapter(getActivity(), invitationLists);
        invitationListings.setAdapter(invitationAdapter);
        invitationListings.setNestedScrollingEnabled(false);

        invitationAdapter.setRecyclerItemClickListener(new InvitationAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position, View view) {
                if (!NetworkUtils.isInNetwork(getActivity())) {
                    AlertUtils.showToast(getActivity(), getResources().getString(R.string.error_no_internet));
                    return;
                }
                if (view.getId() == R.id.btn_accept_invtn) {
                    acceptInvitationTask(position);
                } else if (view.getId() == R.id.btn_reject_invtn) {
                    Button btnLeave = (Button) view;
                    if (btnLeave.getText().toString().equalsIgnoreCase("leave")) {
                        leaveTeamTask(position);
                    } else {
                        rejectInvitationTask(position);
                    }
                } else if (view.getId() == R.id.tv_accept) {
                    TournamentModel tMdl = invitationLists.get(position).getInviteToTeam();
                    openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, tMdl);
                }
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            if (!TextUtils.isEmpty(gameMdl.getGameUserId()) || gameMdl.getGameUserId().equals("null")) {
                VolleyHelper.getInstance(getActivity()).cancelRequest("invitationListingTask");
                invitationListingTask();
            } else {
                errorText.setText("You have not joined the game yet.");
                errorText.setVisibility(View.VISIBLE);
                invitationListings.setVisibility(View.GONE);
            }
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            invitationListings.setVisibility(View.GONE);
        }
    }

    private void rejectInvitationTask(final int position) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Processing....");

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_id", invitationLists.get(position).getInviteToTeam().getGameId());
        postMap.put("inv_id", invitationLists.get(position).getInvitationId());
//        postMap.put("team_id", invitationLists.get(position).getInviteToTeam().getTeamId());

        vHelper.addVolleyRequestListeners(Api.getInstance().rejectInvitation, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {

                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                invitationLists.remove(position);
                                invitationAdapter.notifyDataSetChanged();

                                if (invitationCount != null) {
                                    invitationCount.setInvitation(invitationLists.size());
                                }
                            } else {
                                AlertUtils.showSnack(getActivity(), invitationListings, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("rejectInvitationTask error res", e.getMessage());
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("rejectInvitationTask error res", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                        AlertUtils.showSnack(getActivity(), invitationListings, errorMsg);
                    }
                }, "rejectInvitationTask");
    }

    private void acceptInvitationTask(final int position) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Processing....");

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_id", invitationLists.get(position).getInviteToTeam().getGameId());
        postMap.put("inv_id", invitationLists.get(position).getInvitationId());
//        postMap.put("team_id", invitationLists.get(position).getInviteToTeam().getTeamId());

        vHelper.addVolleyRequestListeners(Api.getInstance().acceptInvitation, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {

                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                invitationLists.get(position).getInviteToTeam().setTeamStatus(InvitationModel.INVITE_ACCEPTED);

                                invitationAdapter.notifyDataSetChanged();

                                UserTicketBalanceUpdate.getTicketBalanceTask(getActivity());

                                if (acceptRequestCallBack != null){
                                    acceptRequestCallBack.setAcceptCallback();
                                }
                            } else {
                                invitationAdapter.notifyItemChanged(position);
                                AlertUtils.showAlertMessage(getActivity(), "Error", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("acceptInvitationTask error res", e.getMessage());
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("acceptInvitationTask error res", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                        AlertUtils.showSnack(getActivity(), invitationListings, errorMsg);
                    }
                }, "acceptInvitationTask");
    }

    private void leaveTeamTask(final int position) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        postParams.put("game_id", invitationLists.get(position).getInviteToTeam().getGameId());
        postParams.put("tourn_id", invitationLists.get(position).getInviteToTeam().getTrnmntId());
        postParams.put("team_id", invitationLists.get(position).getInviteToTeam().getTeamId());
        postParams.put("member_id", preferenceHelper.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().leaveTeam, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                invitationLists.remove(position);
                                invitationAdapter.notifyDataSetChanged();
                                AlertUtils.showAlertMessage(getActivity(), "Success", res.getString("message"));
                                if (invitationCount != null) {
                                    invitationCount.setInvitation(invitationLists.size());
                                }

                                UserTicketBalanceUpdate.getTicketBalanceTask(getActivity());
                            } else {
                                AlertUtils.showSnack(getActivity(), invitationListings, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("leaveTeamTask json ex: ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("leaveTeamTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), invitationListings, errorMsg);
                    }
                }, "leaveTeamTask");
    }

    private void invitationListingTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_id", gameMdl.getGameId());

        vHelper.addVolleyRequestListeners(Api.getInstance().invitationListings, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {

                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            invitationLists.clear();
                            if (res.getBoolean("status")) {
                                JSONArray invitationArray = res.getJSONArray("friend_invitation_list");
                                for (int i = 0; i < invitationArray.length(); i++) {
                                    JSONObject eachInv = invitationArray.getJSONObject(i);

                                    InvitationModel invitationModel = new InvitationModel();
                                    invitationModel.setInvitationId(eachInv.getString("id"));
                                    invitationModel.setInvitationStatus(eachInv.getString("inv_status"));

                                    UserModel inviteBy = new UserModel();
                                    inviteBy.setUserName(eachInv.getString("username"));
                                    inviteBy.setUserFullName(eachInv.getString("full_name"));
                                    inviteBy.setUserId(eachInv.getString("creater_id"));
                                    inviteBy.setGameUserId(eachInv.getString("creater_game_user_id"));
                                    invitationModel.setInviteBy(inviteBy);

                                    TeamModel teamModel = new TeamModel();
                                    teamModel.setGameId(eachInv.getString("game_id"));
                                    teamModel.setTeamId(eachInv.getString("team_id"));
                                    teamModel.setMyTeamName(eachInv.getString("team_name"));
                                    //no of team entered = no of player added
                                    teamModel.setNumOfTeamsEntered(eachInv.getString("num_player_added"));
                                    teamModel.setRequiredTeams(eachInv.getString("players_participated"));
                                    teamModel.setTcktsPerPerson(eachInv.optString("ticket_per_person"));

                                    //tournament info
                                    teamModel.setTrnmntId(eachInv.getString("tournment_id"));
                                    teamModel.setCurrentRound(eachInv.getString("current_round_no"));
                                    teamModel.setTrnmntName(eachInv.optString("tourn_name"));
                                    teamModel.setGameServersArray(eachInv.optString("server_name"));

//                                    teamModel.setTeamStatus(InvitationModel.INVITE_RECEIVED);
                                    teamModel.setTeamStatus(eachInv.getString("inv_status"));

                                    invitationModel.setInviteToTeam(teamModel);

                                    invitationLists.add(invitationModel);
                                }

                                if (invitationCount != null) {
                                    invitationCount.setInvitation(invitationLists.size());
                                }

                                if (invitationLists.size() > 0) {
                                    invitationAdapter.notifyDataSetChanged();
                                    invitationListings.setVisibility(View.VISIBLE);
                                } else {
                                    errorText.setText("You have no invitations");
                                    errorText.setVisibility(View.VISIBLE);
                                    invitationListings.setVisibility(View.GONE);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("invitationListingTask error res", e.getMessage());
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("invitationListingTask error res", e.getMessage() + " ");
                        }
                        progressBar.setVisibility(View.GONE);
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        invitationListings.setVisibility(View.GONE);
                    }
                }, "invitationListingTask");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificFragment = null;
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VolleyHelper.getInstance(getContext()).cancelRequest("invitationListingTask");
    }

    public interface InvitationCount {
        void setInvitation(int count);
    }

    public interface AcceptRequestCallBack {
        void setAcceptCallback();
    }

    public void setInvitationCountCallback(InvitationCount invitationCount) {
        this.invitationCount = invitationCount;

    }

    AcceptRequestCallBack acceptRequestCallBack;
    public void setAcceptRequestCallback(AcceptRequestCallBack acceptCallback) {
        this.acceptRequestCallBack = acceptCallback;

    }

}
