package com.emts.ugbattle.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.PlayerListsAdapter;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.ImageFilePath;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TournamentModel;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class TwoTeamFragment extends Fragment {
    ImageView addScreenshot;
    public static final int REQUEST_PICK_PHOTO = 135;
    boolean isTeamLeader = false;
    boolean isMyTeam = false;
    PreferenceHelper prefHelper;
    boolean isGameFinished = false;
    String fileUploadPath = "";
    Button btnSubmit;
    TournamentModel gameMdl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ongoing_teams, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView teamName = view.findViewById(R.id.tv_team_name);
        RecyclerView memberList = view.findViewById(R.id.memberList);
        memberList.setLayoutManager(new LinearLayoutManager(getActivity()));
        memberList.setNestedScrollingEnabled(false);
        addScreenshot = view.findViewById(R.id.imgUpload);
        btnSubmit = view.findViewById(R.id.btnSubmit);

        try {
            Bundle bundle = getArguments();
            if (bundle == null) {
                return;
            }
            isMyTeam = bundle.getBoolean("isMyTeam");
            if (isMyTeam) {
                gameMdl = (TournamentModel) bundle.getSerializable("game");
            }
            String response = bundle.getString("response");

            JSONObject res = new JSONObject(response);
            JSONObject matchInfo = res.getJSONObject("match_info");
            isGameFinished = !matchInfo.getString("status").equalsIgnoreCase("ongoing");
            JSONObject teamInfo = null;
            if (isMyTeam) {
                teamInfo = matchInfo.getJSONObject("my_team");
            } else {
                teamInfo = matchInfo.getJSONObject("opponent_team");
            }
            String teamLeaderId = teamInfo.getString("creater_id");
            if (isMyTeam && teamLeaderId.equalsIgnoreCase(prefHelper.getUserId())) {
                isTeamLeader = true;
            }

            teamName.setText(teamInfo.getString("team_name"));

            String defaultImg = res.getString("default_img_path");
            String userImageBaseUrl = res.getString("user_img_base_url");
            JSONArray playerArray = teamInfo.getJSONArray("members");
            ArrayList<UserModel> playerList = new ArrayList<>(playerArray.length());
            for (int i = 0; i < playerArray.length(); i++) {
                JSONObject eachPlayer = playerArray.getJSONObject(i);
                UserModel playerModel = new UserModel();
                playerModel.setUserId(eachPlayer.getString("id"));
                playerModel.setGameUserId(eachPlayer.getString("game_user_id"));
                playerModel.setUserName(eachPlayer.getString("username"));
                playerModel.setUserFullName(eachPlayer.getString("full_name"));
                if (playerModel.getUserId().equalsIgnoreCase(teamLeaderId)) {
                    playerModel.setUserMemberStatus("Leader");
                } else {
                    playerModel.setUserMemberStatus("Member");
                }

                String playerImg = eachPlayer.getString("profile_img");
                if (playerImg.equalsIgnoreCase("null") || TextUtils.isEmpty(playerImg)) {
                    playerModel.setUserProfilePic(defaultImg);
                } else {
                    playerModel.setUserProfilePic(userImageBaseUrl + playerImg);
                }
                if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.LEADER)) {
                    playerList.add(0, playerModel);
                } else {
                    playerList.add(playerModel);
                }
            }

            PlayerListsAdapter adapter = new PlayerListsAdapter(getActivity(), playerList);
//            adapter.setTeam(team);
            adapter.setGameStarted(true);
            memberList.setAdapter(adapter);

            if (isGameFinished) {
//                if (matchInfo.getString("winner_team").equalsIgnoreCase(teamInfo.getString("team_id"))) {
//                    addScreenshot.setImageResource(R.drawable.victory);
//                } else {
//                    addScreenshot.setImageResource(R.drawable.defeat);
//                }
                JSONObject uploadedImages = matchInfo.getJSONObject("team_upload_images");
                String uploadedImg = "";
                if (isMyTeam) {
                    uploadedImg = uploadedImages.getString("my_team_img");
                } else {
                    uploadedImg = uploadedImages.getString("opponent_team_img");
                }
                if (!TextUtils.isEmpty(uploadedImg) && !uploadedImg.equalsIgnoreCase("null")) {
                    Glide.with(getActivity()).load(uploadedImg).into(addScreenshot);
                }
            } else {
                JSONObject uploadedImages = matchInfo.getJSONObject("team_upload_images");
                String uploadedImg = "";
                if (isMyTeam) {
                    uploadedImg = uploadedImages.getString("my_team_img");
                } else {
                    uploadedImg = uploadedImages.getString("opponent_team_img");
                }
                if (!TextUtils.isEmpty(uploadedImg) && !uploadedImg.equalsIgnoreCase("null")) {
                    Glide.with(getActivity()).load(uploadedImg).into(addScreenshot);
                }

                if (isTeamLeader) {
                    addScreenshot.setOnClickListener(new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onClick(View view) {
                            try {
                                if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PICK_PHOTO);
                                } else {
                                    pickFromGallery();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            pickFromGallery();
                        }
                    });
                    btnSubmit.setVisibility(View.VISIBLE);
                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!TextUtils.isEmpty(fileUploadPath)) {
                                AlertUtils.showAlertMessage(getActivity(), false,
                                        "Upload Screenshot !!!", "Are you sure you want to upload the screenshot?",
                                        "OK", "Cancel", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                    uploadScreenshotTask();
                                                }
                                            }
                                        });
                            } else {
                                AlertUtils.showSnack(getActivity(), view, "Pick image to upload!!!");
                            }
                        }
                    });
                } else {
                    addScreenshot.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertUtils.showSnack(getActivity(), view, "Only team leader can upload screenshots");
                        }
                    });
                }
            }

        } catch (JSONException e) {
            Logger.e("TwoTeamFragment Response ex", e.getMessage() + " ");
        }
    }

    private void uploadScreenshotTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Uploading screenshot...");

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_id", gameMdl.getGameId());
        postMap.put("team_id", gameMdl.getTeamId());
        postMap.put("tourn_id", gameMdl.getTrnmntId());
        postMap.put("current_round", String.valueOf(gameMdl.getCurrentRoundNo()));

        HashMap<String, String> fileMap = new HashMap<>();
        fileMap.put("image_file", fileUploadPath);

        vHelper.addMultipartRequest(Api.getInstance().uploadScreenshotApi, Request.Method.POST,
                postMap, fileMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showAlertMessage(getActivity(), "Upload Success!!!", res.getString("message"));
                            } else {
                                AlertUtils.showAlertMessage(getActivity(), "Upload Error!!!", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("uploadScreenshotTask json ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("uploadScreenshotTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAlertMessage(getActivity(), "Upload Error!!!", errorMsg);
                    }
                }, "uploadScreenshotTask");
    }

    public void pickFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_PICK_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.e("onActivityResult", "REQ:" + requestCode + " RES:" + resultCode + " DATA::" + data);
        if (requestCode == REQUEST_PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                fileUploadPath = ImageFilePath.getPath(getActivity(), data.getData());
                Logger.e("fileUploadPath", fileUploadPath);
                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                addScreenshot.setImageBitmap(BitmapFactory.decodeStream(inputStream));
                btnSubmit.setBackgroundResource(R.drawable.rounded_blue);
            } catch (FileNotFoundException e) {
                Logger.e("imagePick ex", e.getMessage() + " ");
            }
        }
    }

}
