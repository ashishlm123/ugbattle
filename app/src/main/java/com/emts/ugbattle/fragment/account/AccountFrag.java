package com.emts.ugbattle.fragment.account;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;

/**
 * Created by User on 2018-03-19.
 */

public class AccountFrag extends Fragment {
    RelativeLayout btnOrder, btnMyWithdraw, btnMyMessages, btnMyProfile, btnMyFriendsList, btnWithdrawalHistory,
            btnLogOut;
    public ParentActivityInterface openSpecificFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnOrder = view.findViewById(R.id.btn_order);
        btnMyWithdraw = view.findViewById(R.id.btn_my_withdrawl);
        btnMyMessages = view.findViewById(R.id.btn_message);
        btnMyProfile = view.findViewById(R.id.btn_profile);
        btnMyFriendsList = view.findViewById(R.id.btn_friendLists);
        btnWithdrawalHistory = view.findViewById(R.id.btn_withdrawl_his);
        btnLogOut = view.findViewById(R.id.btn_logout);

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnMyWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnMyMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
//                AlertUtils.showToast(getActivity(), "TO:DO");
            }
        });

        btnMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnMyFriendsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnWithdrawalHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

