package com.emts.ugbattle.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;

/**
 * Created by User on 2018-03-19.
 */

public class QuestionsFrag extends Fragment {
    RelativeLayout btnHelpCenter, btnFAQs, btnPrivacyPolicy, btnTermsOfUse;
    Toolbar toolbar;
    public ParentActivityInterface openSpecificFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_questions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar = view.findViewById(R.id.toolbar);

        btnHelpCenter = view.findViewById(R.id.btn_help_center);
        btnFAQs = view.findViewById(R.id.btn_faqs);
        btnPrivacyPolicy = view.findViewById(R.id.btn_privacy);
        btnTermsOfUse = view.findViewById(R.id.btn_terms_of_use);

        btnHelpCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnFAQs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        btnTermsOfUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }
}
