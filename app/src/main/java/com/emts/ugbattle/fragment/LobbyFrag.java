package com.emts.ugbattle.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.GameChallengeAdapter;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.TeamModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2018-03-19.
 */

public class LobbyFrag extends Fragment {
    ProgressBar progressBar, infiniteProgress;
    TextView errorText;
    ArrayList<GameMdl> gameLists = new ArrayList<>();
    GameChallengeAdapter adapter;
    RecyclerView gameListsView;

    int limit = 10000;
    int offset = 0;
    ParentActivityInterface mainInterface;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);
        errorText = view.findViewById(R.id.error_text);

        gameListsView = view.findViewById(R.id.listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        gameListsView.setLayoutManager(linearLayoutManager);
        adapter = new GameChallengeAdapter(getActivity(), gameLists);
        adapter.setOpenSpecificFragment(mainInterface);
        gameListsView.setAdapter(adapter);
        gameListsView.setNestedScrollingEnabled(false);

        if (NetworkUtils.isInNetwork(getActivity())) {
            gameLobbyTask();
        } else {
            gameListsView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void gameLobbyTask() {
        errorText.setVisibility(View.GONE);
        gameListsView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().lobbyUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        gameLists.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                ArrayList<TeamModel> myTeamIds = new ArrayList<>();
                                JSONArray gameArray = res.getJSONArray("on_going_games");

                                String imgDirectory = res.getString("game_image_url");
                                for (int i = 0; i < gameArray.length(); i++) {
                                    JSONObject eachGame = gameArray.getJSONObject(i);
                                    GameMdl gameMdl = new GameMdl();
                                    gameMdl.setGameId(eachGame.getString("game_id"));
                                    gameMdl.setGameUserId(eachGame.getString("game_user_id"));
                                    gameMdl.setGameName(eachGame.getString("game_name"));
                                    gameMdl.setGameServersArray(eachGame.getString("selected_server"));
                                    gameMdl.setGameDescription(eachGame.getString("description"));

                                    String eachGameImg = eachGame.getString("game_image");
                                    if (!TextUtils.isEmpty(eachGameImg) || !eachGameImg.equals("null")) {
                                        gameMdl.setGameImg(imgDirectory + eachGameImg);
                                    }

                                    JSONObject myTeamInfo = eachGame.getJSONObject("my_team_info");
                                    if (myTeamInfo.has("team_id")) {
                                        TeamModel teamModel = new TeamModel();
                                        teamModel.setTeamId(myTeamInfo.getString("team_id"));
                                        teamModel.setMyTeamLeaderId(myTeamInfo.getString("creater_id"));
                                        teamModel.setMyTeamName(myTeamInfo.getString("team_name"));
                                        myTeamIds.add(teamModel);
                                    }
                                    JSONArray teamsArray = eachGame.getJSONArray("teams");
                                    ArrayList<TeamModel> teamLists = new ArrayList<>();
                                    if (teamsArray.length() > 0) {
                                        for (int j = 0; j < teamsArray.length(); j++) {
                                            TeamModel teamModel = new TeamModel();
                                            if (myTeamInfo.has("team_id")) {
                                                teamModel.setMyTeamId(myTeamInfo.getString("team_id"));
                                                teamModel.setMyTeamLeaderId(myTeamInfo.getString("creater_id"));
                                                teamModel.setMyTeamName(myTeamInfo.getString("team_name"));

                                                JSONObject eachTeams = teamsArray.getJSONObject(j);
                                                teamModel.setOpponentTeamId(eachTeams.getString("team_id"));
                                                teamModel.setGameId(eachGame.getString("game_id"));
                                                teamModel.setGameUserId(eachGame.getString("game_user_id"));
                                                teamModel.setOpponentTeamName(eachTeams.getString("team_name"));
                                                teamModel.setChallengingTeamId(eachTeams.getString("challenging_team"));
                                                teamModel.setOpponentTeamLeaderUserId(eachTeams.getString("creater_id"));
                                                teamModel.setTrnmntId(eachTeams.getString("tournment_id"));
                                                teamModel.setTotalRounds(Integer.parseInt(eachTeams.getString("total_rounds")));
                                                teamModel.setTeamStatus(eachTeams.getString("is_challenged"));
//                                                teamModel.setTeamStatus("null");
                                                Logger.e("team status", eachTeams.getString("is_challenged"));
                                                try {
                                                    teamModel.setCurrentRoundNo(Integer.parseInt(eachTeams.getString("current_round_no")));
                                                } catch (Exception e) {
                                                    teamModel.setCurrentRoundNo(0);
                                                }
                                                if (teamModel.getChallengingTeamId().equals(teamModel.getMyTeamId())
                                                        && teamModel.getTeamStatus().equalsIgnoreCase(Config.IS_ACCEPTED)) {
                                                    teamLists.clear();
                                                    teamLists.add(teamModel);
                                                    Logger.e("where", "I am inside accepted");
                                                    break;
                                                } else {
                                                    if (teamModel.getChallengingTeamId().equals("0") ||
                                                            teamModel.getChallengingTeamId().equals(teamModel.getMyTeamId())) {
                                                        Logger.e("where", "I am inside else");
                                                        teamLists.add(teamModel);
                                                    }
                                                }

                                            } else {
                                                gameListsView.setVisibility(View.GONE);
                                                errorText.setText("You have not created a team yet. Please redirect to homepage and create a team");
                                                errorText.setVisibility(View.VISIBLE);
                                                return;
                                            }
                                        }
                                    }
                                    gameMdl.setTeamLists(teamLists);
                                    gameLists.add(gameMdl);
                                }
                                adapter.setMyTeamLists(myTeamIds);
                                adapter.notifyDataSetChanged();
                                gameListsView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);

                                if (gameLists.size() > 0) {
//                                    ticketAdapter.notifyDataSetChanged();
                                    gameListsView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    gameListsView.setVisibility(View.GONE);
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                gameListsView.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("gameLobbyTask json ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("gameLobbyTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        gameListsView.setVisibility(View.GONE);
                    }
                }, "gameLobbyTask");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainInterface = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        mainInterface = null;
        super.onDetach();
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("gameLobbyTask");
        super.onPause();
    }

}
