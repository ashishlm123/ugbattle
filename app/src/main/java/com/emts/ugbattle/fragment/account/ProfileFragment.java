package com.emts.ugbattle.fragment.account;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.GameUserIDAdapter;
import com.emts.ugbattle.fragment.TwoTeamFragment;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.ImageHelper;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.RoundedCornersTransformation;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {
    ImageView userProfilePic, editProfilePic;
    EditText edtEmail, edtUserName, edtFullName, edtIcPassportNo, edtContatNumber,
            edtOldPassWord, edtNewPsw, edtConPswd;
    CheckBox cbPaypal, cbDbtCrdtCard;
    Button btnAddUserId, btnSave;

    PreferenceHelper prefsHelper;

    RecyclerView gameItemListings;
    GameUserIDAdapter gameUserIDAdapter;
    ArrayList<GameMdl> gameItemLists = new ArrayList<>();
    ArrayList<GameMdl> allGame = new ArrayList<>();
    ArrayList<String> serverList = new ArrayList<>();

    LinearLayout holderGameInfoForm, profileHolder;
    Spinner spGameName, spGameServer;
    EditText edtGameUserId;
    ProgressBar progressBar;
    TextView errorText;
    public static Uri fileUri = null;
    String selectedImagePath;
    public static final int RESULT_LOAD_IMAGE = 123;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 9873;

    boolean isSocialUser = false;
    int editGameItemPosition = -1;
    NestedScrollView scrollProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VolleyHelper.getInstance(getContext()).cancelRequest("");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

        gameItemListings = view.findViewById(R.id.game_user_id_listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        gameItemListings.setLayoutManager(linearLayoutManager);
        gameUserIDAdapter = new GameUserIDAdapter(getActivity(), gameItemLists);
        gameItemListings.setAdapter(gameUserIDAdapter);
        gameItemListings.setNestedScrollingEnabled(false);

        gameUserIDAdapter.setRecyclerItemClickListener(new GameUserIDAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position) {
                editGameItemPosition = position;
                holderGameInfoForm.setVisibility(View.VISIBLE);
                edtGameUserId.setText(gameItemLists.get(position).getGameUserId());
                for (int i = 0; i < allGame.size(); i++) {
                    if (allGame.get(i).getGameName().equals(gameItemLists.get(position).getGameName())) {
                        spGameName.setSelection(i);
                        spGameName.setClickable(false);
                        spGameName.setEnabled(false);
                    }

                }

                for (int i = 0; i < serverList.size(); i++) {
                    if (serverList.get(i).equals(gameItemLists.get(position).getGameServersArray())) {
                        spGameServer.setSelection(i);
                    }
                }

                scrollProfile.scrollTo((int) btnAddUserId.getX(), (int) btnAddUserId.getY());

            }
        });

        btnAddUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSave.setEnabled(false);
                if (holderGameInfoForm.getVisibility() == View.VISIBLE) {
                    holderGameInfoForm.setVisibility(View.GONE);
                } else {
                    holderGameInfoForm.setVisibility(View.VISIBLE);
                }
            }
        });

        edtGameUserId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(edtGameUserId.getText().toString().trim())) {
                    btnSave.setEnabled(false);
                } else {
                    btnSave.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    AlertUtils.hideInputMethod(getActivity(), errorText);
                    saveGameUserIdTask(edtGameUserId.getText().toString().trim(),
                            editGameItemPosition, spGameName.getSelectedItem().toString(),
                            spGameServer.getSelectedItem().toString(),
                            allGame.get(spGameName.getSelectedItemPosition()).getGameId(),
                            allGame.get(spGameName.getSelectedItemPosition()).getGameImg());
                } else {
                    AlertUtils.showSnack(getActivity(), editProfilePic, getResources().getString(R.string.error_no_internet));
                }

            }
        });
        editProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto();
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            getUserDetailTask();
        } else {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(R.string.error_no_internet);
        }
    }

    private void init(View view) {
        scrollProfile = view.findViewById(R.id.scroll_profile);
        userProfilePic = view.findViewById(R.id.profile_icon);
        editProfilePic = view.findViewById(R.id.edit_profile_pic);

        edtEmail = view.findViewById(R.id.edt_email_address);
        edtUserName = view.findViewById(R.id.edt_user_name);
        edtFullName = view.findViewById(R.id.edt_full_name);
        edtIcPassportNo = view.findViewById(R.id.edt_ic_passport_no);
        edtContatNumber = view.findViewById(R.id.edt_contact_number);
        edtOldPassWord = view.findViewById(R.id.edt_old_password);
        edtNewPsw = view.findViewById(R.id.edt_new_password);
        edtConPswd = view.findViewById(R.id.edt_confirm_password);

        cbPaypal = view.findViewById(R.id.checkbox_paypal);
        cbDbtCrdtCard = view.findViewById(R.id.checkbox_debit_credit_card);

        cbPaypal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cbDbtCrdtCard.setChecked(false);
                }
            }
        });
        cbDbtCrdtCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cbPaypal.setChecked(false);
                }
            }
        });

        final TextView errorEmail = view.findViewById(R.id.errorEmail);
        final TextView errorUsername = view.findViewById(R.id.errorUsername);
        final TextView errorFullName = view.findViewById(R.id.errorFullName);
        final TextView errorIcPassword = view.findViewById(R.id.errorIcPassport);
        final TextView errorContact = view.findViewById(R.id.errorContact);
        final TextView errorPayment = view.findViewById(R.id.errorPayment);

        btnAddUserId = view.findViewById(R.id.btn_add_user_id);
        btnSave = view.findViewById(R.id.btn_save);

        holderGameInfoForm = view.findViewById(R.id.layout_game_user_id_form);
        spGameName = view.findViewById(R.id.sp_game);
        spGameServer = view.findViewById(R.id.sp_server);
        edtGameUserId = view.findViewById(R.id.edt_game_user_id);
        profileHolder = view.findViewById(R.id.lay_profile);
        progressBar = view.findViewById(R.id.progress_bar);
        errorText = view.findViewById(R.id.error_text);

        Button btnSaveProfile = view.findViewById(R.id.btnSaveProfile);
        btnSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetworkUtils.isInNetwork(getActivity())) {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.error_no_internet));
                    return;
                }
                boolean isPasswordChanging = false;
                if (!TextUtils.isEmpty(edtNewPsw.getText().toString().trim()) ||
                        !TextUtils.isEmpty(edtConPswd.getText().toString().trim())) {
                    isPasswordChanging = true;
                    if (TextUtils.isEmpty(edtOldPassWord.getText().toString().trim()) ||
                            TextUtils.isEmpty(edtNewPsw.getText().toString().trim()) ||
                            TextUtils.isEmpty(edtConPswd.getText().toString().trim())) {
                        AlertUtils.showAlertMessage(getActivity(), "",
                                "Please enter old password, new password and confirm password" +
                                        " if you wish to change password or else leave those field empty");
                        return;
                    } else {
                        if (!containAlphanumeric(edtNewPsw.getText().toString().trim())) {
                            AlertUtils.showAlertMessage(getActivity(), "", "New password must contains alphanumeric character");
                            return;
                        }
                        if (!edtNewPsw.getText().toString().trim().equals(edtConPswd.getText().toString())) {
                            AlertUtils.showAlertMessage(getActivity(), "", "New password and confirm password does not match");
                            return;
                        }
                    }
                }

                boolean isValid = true;
                if (!isSocialUser && TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
                    errorEmail.setText(R.string.error_field_required);
                    errorEmail.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorEmail.setVisibility(View.GONE);
                }
                if (!isSocialUser && TextUtils.isEmpty(edtUserName.getText().toString().trim())) {
                    errorUsername.setText(R.string.error_field_required);
                    errorUsername.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorUsername.setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(edtFullName.getText().toString().trim())) {
                    errorFullName.setText(R.string.error_field_required);
                    errorFullName.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorFullName.setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(edtIcPassportNo.getText().toString().trim())) {
                    errorIcPassword.setText(R.string.error_field_required);
                    errorIcPassword.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorIcPassword.setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(edtContatNumber.getText().toString().trim())) {
                    errorContact.setText(R.string.error_field_required);
                    errorContact.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorContact.setVisibility(View.GONE);
                }

                if (!cbDbtCrdtCard.isChecked() && !cbPaypal.isChecked()) {
                    errorPayment.setText("Please check one payment method");
                    errorPayment.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorPayment.setVisibility(View.GONE);
                }

                if (isValid) {
                    updateProfileTask();
                } else {
                    AlertUtils.showSnack(getActivity(), view, "Please complete the form with proper inputs");
                }
            }
        });

    }

    private void getUserDetailTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = volleyHelper.getPostParams();

        volleyHelper.addVolleyRequestListeners(Api.getInstance().getUserDetailUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        profileHolder.setVisibility(View.VISIBLE);

                        JSONObject userInformation = res.getJSONObject("user_info");
                        edtFullName.setText(userInformation.getString("full_name"));
                        edtEmail.setText(userInformation.getString("email"));
                        edtIcPassportNo.setText(userInformation.getString("ic_passport"));
                        edtContatNumber.setText(userInformation.getString("contact_no"));
                        edtUserName.setText(userInformation.getString("username"));
                        Picasso.with(getActivity()).load(userInformation.getString("profile_img"))
                                .transform(new RoundedCornersTransformation()).into(userProfilePic);

                        // payment
                        String paymentType = userInformation.getString("payment_method");
                        if (paymentType.equals("1")) {
                            cbPaypal.setChecked(true);
                            cbDbtCrdtCard.setChecked(false);
                        } else if (paymentType.equals("2")) {
                            cbDbtCrdtCard.setChecked(true);
                            cbPaypal.setChecked(false);
                        }

                        //check if fb user
                        setFbUser(!userInformation.getString("reg_type").equals("app"));

                        JSONArray gameProfile = res.getJSONArray("game_profiles");
                        for (int k = 0; k < gameProfile.length(); k++) {
                            JSONObject eachObj = gameProfile.getJSONObject(k);
                            GameMdl gameMdl = new GameMdl();
                            gameMdl.setGameName(eachObj.getString("game_name"));
                            gameMdl.setGameId(eachObj.getString("game_id"));
                            gameMdl.setGameServersArray(eachObj.getString("server_name"));
                            gameMdl.setGameUserId(eachObj.getString("game_user_id"));
                            gameMdl.setGameImg(res.getString("game_img_base_url") + eachObj.getString("game_image"));
                            gameItemLists.add(gameMdl);
                        }
                        gameUserIDAdapter.notifyDataSetChanged();
                        gameItemListings.setVisibility(View.VISIBLE);

                        // all game list
                        JSONArray allGameList = res.getJSONArray("all_games");
                        for (int i = 0; i < allGameList.length(); i++) {
                            JSONObject gameObj = allGameList.getJSONObject(i);
                            GameMdl gameMdl = new GameMdl();
                            gameMdl.setGameName(gameObj.getString("game_name"));
                            gameMdl.setGameId(gameObj.getString("game_id"));
                            gameMdl.setGameServersArray(gameObj.getString("server_names"));
                            gameMdl.setGameImg(res.getString("game_img_base_url") + gameObj.getString("game_image"));
                            allGame.add(gameMdl);
                        }

                        ArrayAdapter<GameMdl> dataAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner_view, allGame);

                        // Drop down layout style - list view with radio button
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        // attaching data adapter to spinner
                        spGameName.setAdapter(dataAdapter);


                        String serverObject = allGame.get(spGameName.getSelectedItemPosition()).getGameServersArray().toString();
                        JSONArray serverArray = new JSONArray(serverObject);
                        for (int j = 0; j < serverArray.length(); j++) {
                            serverList.add(serverArray.getString(j));
                        }

                        ArrayAdapter<String> serverAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner_view, serverList);

                        // Drop down layout style - list view with radio button
                        serverAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        // attaching data adapter to spinner
                        spGameServer.setAdapter(serverAdapter);
                    } else {
                        profileHolder.setVisibility(View.GONE);
                        errorText.setText(Html.fromHtml(res.getString("message")));
                        errorText.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Logger.e("getUserDetailTask ex", e.getMessage() + " ");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = String.valueOf(Html.fromHtml(errorObj.getString("message")));
                } catch (Exception e) {
                    Logger.e("getUserDetailTask error res", e.getMessage() + " ");
                }

                errorText.setText(errorMsg);
                errorText.setVisibility(View.VISIBLE);
            }
        }, "getUserDetailTask");
    }

    private void setFbUser(boolean isFbUser) {
        if (isFbUser) {
            isSocialUser = true;
//            edtUserName.setVisibility(View.GONE);
            edtOldPassWord.setVisibility(View.GONE);
            edtNewPsw.setVisibility(View.GONE);
            edtConPswd.setVisibility(View.GONE);
        }
    }

    private void updateProfileTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Updating profile...");

        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = volleyHelper.getPostParams();

        postParam.put("full_name", edtFullName.getText().toString().trim());
//        if (!isSocialUser) {
            postParam.put("username", edtUserName.getText().toString().trim());
//        }
        postParam.put("email", edtEmail.getText().toString().trim());
        postParam.put("contact_no", edtContatNumber.getText().toString().trim());
        postParam.put("ic_passport", edtIcPassportNo.getText().toString().trim());
        postParam.put("payment_method", cbPaypal.isChecked() ? "1" : "2");
        if (!TextUtils.isEmpty(edtNewPsw.getText().toString().trim())) {
            postParam.put("old_password", edtOldPassWord.getText().toString().trim());
            postParam.put("new_password", edtNewPsw.getText().toString().trim());
        }

        volleyHelper.addVolleyRequestListeners(Api.getInstance().updateProfileUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showAlertMessage(getActivity(), "Update Success", String.valueOf(Html.fromHtml(res.getString("message"))), "OK", "", null);
                            } else {
                                AlertUtils.showAlertMessage(getActivity(), "Update Error",
                                        String.valueOf(Html.fromHtml(res.getString("message"))), "OK", "", null);
                            }
                        } catch (Exception e) {
                            Logger.e("updateProfileTask ex", e.getMessage() + " ");
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = String.valueOf(Html.fromHtml(errorObj.getString("message")));
                        } catch (Exception e) {
                            Logger.e("updateProfileTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAlertMessage(getActivity(), "Error !!!", errorMsg);
                    }
                }, "updateProfileTask");
    }

    public void uploadImageTask() {
        final ProgressDialog progress = AlertUtils.showProgressDialog(getActivity(), "Please wait");
        progress.show();

        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = volleyHelper.getPostParams();
        HashMap<String, String> fileMap = new HashMap<>();
        Bitmap imgBm = ImageHelper.resizeHeightWidth(selectedImagePath);
        ImageHelper.saveImageToPath(selectedImagePath, imgBm);
        fileMap.put("profile_img", selectedImagePath);
        Logger.e("profile_img", selectedImagePath);

        volleyHelper.addMultipartRequest(Api.getInstance().updateProfileImage, Request.Method.POST,
                postMap, fileMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                String path = res.getString("user_img_base_url");
                                String profileIco = res.getString("new_profile_img");
                                if (!TextUtils.isEmpty(profileIco)) {
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_IMG, path + profileIco).commit();
                                    Glide.with(getActivity()).load(path + profileIco).into(userProfilePic);

                                }
                                AlertUtils.showSnack(getActivity(), userProfilePic, (res.getString("message")));
                            } else {
                                AlertUtils.showAlertMessage(getActivity(), "Error", res.getString("message"),
                                        "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            Logger.e("changePasswordTask json ex: ", e.getMessage());
                        }
                        progress.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progress.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("uploadImageTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), errorText, errorMsg);
                    }
                }, "uploadImageTask");
    }

    private void saveGameUserIdTask(final String gameUserId, final int position, final String gameName,
                                    final String serverName, final String gameId, final String gameImg) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Please Wait...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("game_id", allGame.get(spGameName.getSelectedItemPosition()).getGameId());
        postMap.put("game_user_id", gameUserId);
        postMap.put("server_name", spGameServer.getSelectedItem().toString());


        vHelper.addVolleyRequestListeners(Api.getInstance().addGameUserIdUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            pDialog.dismiss();
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showAlertMessage(getActivity(), false, "Success", "Game User Id has been successfully added.",
                                        "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                holderGameInfoForm.setVisibility(View.GONE);
                                                if (position == -1) {
                                                    GameMdl gameMdl = new GameMdl();
                                                    gameMdl.setGameName(gameName);
                                                    gameMdl.setGameServersArray(serverName);
                                                    gameMdl.setGameUserId(gameUserId);
                                                    gameMdl.setGameId(gameId);
                                                    gameMdl.setGameImg(gameImg);
                                                    gameItemLists.add(gameMdl);
                                                    gameUserIDAdapter.notifyDataSetChanged();
                                                } else {
                                                    GameMdl gameMdl = gameItemLists.get(position);
                                                    gameMdl.setGameName(gameName);
                                                    gameMdl.setGameServersArray(serverName);
                                                    gameMdl.setGameUserId(gameUserId);
                                                    gameMdl.setGameImg(gameImg);
                                                    gameUserIDAdapter.notifyItemChanged(position);
                                                }
                                            }
                                        });

                                gameItemListings.setVisibility(View.VISIBLE);
                                spGameName.setClickable(true);
                                spGameName.setEnabled(true);
                                spGameServer.setSelection(0);
                                spGameName.setSelection(0);
                                edtGameUserId.setText("");

                            } else {
                                pDialog.dismiss();
                                AlertUtils.showSnack(getActivity(), editProfilePic, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            Logger.e("userSignUpTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userSignUpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), editProfilePic,
                                errorMsg);
                    }
                }, "btnSaveGameUserIdTask");
    }

    private void alertForPhoto() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Upload Profile Picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }

                } else if (options[item].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, TwoTeamFragment.REQUEST_PICK_PHOTO);
                    } else {
                        chooseFrmGallery(false);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(boolean video) {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, TwoTeamFragment.REQUEST_PICK_PHOTO);
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                BuildConfig.APPLICATION_ID + ".provider",
//                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {
        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "Alacer");
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_ugbattle");
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_rubids" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == TwoTeamFragment.REQUEST_PICK_PHOTO) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                CropImage.activity(selectedImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(1, 1)
                        .start(getActivity());
            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (fileUri == null) {
                    return;
                }
                CropImage.activity(fileUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(1, 1)
                        .start(getActivity());

            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                selectedImagePath = resultUri.getPath();
                if (resultUri.getPath() != null) {
                    uploadImageTask();
                } else {

                }
            }
        }
    }

    //checking if the password is alphanumeric or not
    public boolean containAlphanumeric(final String str) {
        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=()!?<>';:|`~*/]).{5,13}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z]).{5,13}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }
}
