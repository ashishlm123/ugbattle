package com.emts.ugbattle.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.AddFriendDetailActivity;
import com.emts.ugbattle.activity.ChatActivity;
import com.emts.ugbattle.adapter.MessageAdapter;
import com.emts.ugbattle.customviews.EndlessRecyclerViewScrollListener;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.TournamentModel;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

/**
 * Created by User on 2018-03-19.
 */

public class InboxFrag extends Fragment {
    ProgressBar progressBar, infiniteProgress;
    TextView errorText;
    MessageAdapter messageAdapter;
    RecyclerView messageListView;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    ArrayList<MessageModel> messageList = new ArrayList<>();
    int limit = 10;
    int offSet = 0;
    ParentActivityInterface openSpecificFragment;
    PreferenceHelper preferenceHelper;
    public static final int RESULT_FRIEND_RESPONSE = 8923;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_messages, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        preferenceHelper = PreferenceHelper.getInstance(getActivity());
        errorText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);

        messageListView = view.findViewById(R.id.conversation_listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        messageListView.setLayoutManager(linearLayoutManager);
        messageAdapter = new MessageAdapter(getActivity(), messageList);
        messageListView.setAdapter(messageAdapter);
        messageListView.setNestedScrollingEnabled(false);

        messageAdapter.setRecyclerItemClickListener(new MessageAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position) {
                MessageModel msg = messageList.get(position);
                if (!msg.isRead()) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        markReadTask(position);
                    }
                }
                msg.setRead(true);
                messageAdapter.notifyItemChanged(position);

                switch (messageList.get(position).getMessageType()) {
                    case Config.FRIEND_REQUEST:
                        Intent intent = new Intent(getActivity(), AddFriendDetailActivity.class);
                        intent.putExtra("mesg", msg);
                        getActivity().startActivityForResult(intent, RESULT_FRIEND_RESPONSE);
                        break;

                    case Config.FRIEND_REQUEST_RESP:
                        openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.FRIEND_LIST_FRAG_ID, msg);
                        break;

                    case Config.PLAYER_INVITATION_REQ:
                        openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.GAME_DETAIL_INVITATION_PAGE_ID, msg.getExtras());
                        break;

                    case Config.PLAYER_INVITATION_RES:
                        openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, msg.getExtras());
                        break;

                    case Config.WITHDRAWAL_RES:
                        openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.WITHDRAWAL_HISTORY_ID, msg);
                        break;

                    case Config.TICKET_PURCHASE_RES:
                        openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.ORDER_PAGE_ID, msg);
                        break;

                    case Config.MATCH_CHALLENGE_REQUEST:
                        openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, msg.getExtras());
                        break;

                    case Config.MATCH_CHALLENGE_RESP:
                        openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, msg.getExtras());
                        break;
                    case Config.MESSAGE_NOTI:
                        String chattingMatchId = preferenceHelper.getString(ChatActivity.chattingMatchId, "null");
//                        // in message gameId has value of match id
//                        if (chattingMatchId.equalsIgnoreCase(msg.getGameId()) &&
//                                !msg.getGameId().equals("0")) {
//                            return;
//                        }
                        intent = new Intent(getActivity(), ChatActivity.class);
                        intent.putExtra("response", (String) msg.getExtras());
                        startActivity(intent);
                        break;
                    case Config.MESSAGE_PLAYER_NOTI:
                        String friendId = preferenceHelper.getString(ChatActivity.chattingMatchId, "null");

//                        if (friendId.equalsIgnoreCase(messageList.get(position).getFriend_id()) &&
//                                !messageList.get(position).getFriend_id().equals("0")) {
//                            return;
//                        }

                        UserModel friendMdl = new UserModel();
                        friendMdl.setUserId(msg.getFriend_id());
                        Intent intent1 = new Intent(getActivity(), ChatActivity.class);
                        intent1.putExtra("friend", friendMdl);
                        startActivity(intent1);
                        break;


                }
            }
        });
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (messageList.size() >= limit) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        getNotificationListTask(offSet);
                    } else {
                        errorText.setText(getResources().getString(R.string.error_no_internet));
                        errorText.setVisibility(View.VISIBLE);
                        AlertUtils.showSnack(getActivity(), errorText, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        };
        messageListView.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            offSet = 0;
            getNotificationListTask(offSet);
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.VISIBLE);
        }

    }

    private void markReadTask(final int position) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", messageList.get(position).getMessageId());
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                try {
                                    openSpecificFragment.setBadge(true, resObj.getInt("no_of_message"));
                                } catch (Exception e) {
                                }
                                try {
                                    openSpecificFragment.setBadge(false, resObj.getInt("no_of_note"));
                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("markReadTask res ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }

    private void getNotificationListTask(final int offsetValue) {
        if (offSet == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("limit", String.valueOf(limit));
        postParam.put("offset", String.valueOf(offsetValue));

        vHelper.addVolleyRequestListeners(Api.getInstance().getNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray notificationArray = res.getJSONArray("notification");

                                if (notificationArray.length() == 0) {
                                    if (offsetValue == 0) {
                                        errorText.setText("You have no notifications messages");
                                        errorText.setVisibility(View.VISIBLE);
                                    } else {
                                        AlertUtils.showSnack(getActivity(), errorText, "No more notification");
                                    }
                                } else {
                                    if (offSet == 0) {
                                        messageList.clear();
                                    }
                                    int unReadCount = 0;
                                    for (int i = 0; i < notificationArray.length(); i++) {
                                        JSONObject eachObj = notificationArray.getJSONObject(i);
                                        MessageModel messageModel = new MessageModel();

                                        messageModel.setMessageId(eachObj.getString("note_id"));
                                        messageModel.setMessagePreview(eachObj.getString("message"));
                                        messageModel.setMesgTitle(eachObj.getString("subject"));
                                        messageModel.setMessageTime(eachObj.getString("create_date"));
                                        messageModel.setMessageType(eachObj.getString("type"));

                                        JSONObject desObj = eachObj.getJSONObject("description");
                                        if (eachObj.getString("status").equals("1")) {
                                            messageModel.setRead(true);
                                        } else {
                                            messageModel.setRead(false);
                                            unReadCount++;
                                        }

                                        switch (eachObj.getString("type")) {
                                            case Config.WITHDRAWAL_RES:
                                                preferenceHelper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, desObj.getString("remaining_ticket_balance")).commit();
                                                break;
                                            case Config.TICKET_PURCHASE_RES:
                                                preferenceHelper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, desObj.getString("new_ticket_balance")).commit();
                                                break;
                                            case Config.FRIEND_REQUEST:
                                                messageModel.setUserName(desObj.getString("requested_user"));
                                                messageModel.setFriend_id(desObj.getString("requested_user_id"));
                                                messageModel.setRespType(desObj.getString("inv_status"));
                                                if (desObj.getString("requested_user_img").equals("null")) {
                                                    messageModel.setUserProfilePic(res.getString("user_default_img"));
                                                } else {
                                                    messageModel.setUserProfilePic(res.getString("user_img_base_url") +
                                                            desObj.getString("requested_user_img"));
                                                }
                                                break;
                                            case Config.FRIEND_REQUEST_RESP:
                                                messageModel.setUserName(desObj.getString("response_user"));
                                                messageModel.setFriend_id(desObj.getString("response_user_id"));
                                                messageModel.setRespType(desObj.getString("resp_type"));
                                                if (desObj.getString("response_user_img").equals("null")) {
                                                    messageModel.setUserProfilePic(res.getString("user_default_img"));
                                                } else {
                                                    messageModel.setUserProfilePic(res.getString("user_img_base_url") +
                                                            desObj.getString("response_user_img"));
                                                }
                                                break;

                                            case Config.PLAYER_INVITATION_REQ:
                                                GameMdl gameMdl = new GameMdl();
                                                gameMdl.setGameId(desObj.getString("game_id"));
                                                gameMdl.setGameServersArray(desObj.getString("server_name"));
                                                gameMdl.setGameUserServer(desObj.getString("receiving_user_server_name"));
                                                gameMdl.setGameUserId(desObj.getString("receiving_user_player_id"));
                                                gameMdl.setGameName(desObj.getString("game_name"));
                                                messageModel.setExtras(gameMdl);
                                                break;

                                            case Config.PLAYER_INVITATION_RES:
                                                TournamentModel tournMdl = new TournamentModel();
                                                tournMdl.setGameId(desObj.getString("game_id"));
                                                tournMdl.setTeamId(desObj.getString("team_id"));
                                                try {
                                                    tournMdl.setCurrentRoundNo(Integer.parseInt(desObj.getString("current_round_no")));
                                                } catch (Exception e) {
                                                    tournMdl.setCurrentRoundNo(0);
                                                }
                                                tournMdl.setTrnmntId(desObj.getString("tourn_id"));
                                                tournMdl.setGameName(desObj.getString("game_name"));
                                                tournMdl.setGameServersArray(desObj.getString("server_name"));
                                                tournMdl.setTrnmntName(desObj.getString("tourn_name"));
                                                messageModel.setExtras(tournMdl);
                                                break;
                                            case Config.MATCH_CHALLENGE_REQUEST:
                                                TournamentModel tournamentModel = new TournamentModel();
                                                tournamentModel.setGameId(desObj.getString("game_id"));
                                                tournamentModel.setTeamId(desObj.getString("team_id"));
                                                tournamentModel.setCurrentRoundNo(Integer.parseInt(desObj.getString("current_round_no")));
                                                tournamentModel.setTrnmntId(desObj.getString("tourn_id"));
                                                tournamentModel.setGameName(desObj.getString("game_name"));
                                                messageModel.setExtras(tournamentModel);
                                                break;
                                            case Config.MATCH_CHALLENGE_RESP:
                                                TournamentModel tournamentModel1 = new TournamentModel();
                                                tournamentModel1.setGameId(desObj.getString("game_id"));
                                                tournamentModel1.setTeamId(desObj.getString("team_id"));
                                                tournamentModel1.setCurrentRoundNo(Integer.parseInt(desObj.getString("current_round_no")));
                                                tournamentModel1.setTrnmntId(desObj.getString("tourn_id"));
                                                tournamentModel1.setGameName(desObj.getString("game_name"));
                                                messageModel.setExtras(tournamentModel1);
                                                break;
                                            case Config.MESSAGE_NOTI:
                                                JSONObject response1 = new JSONObject();
                                                try {
                                                    JSONObject match_info = new JSONObject();
                                                    match_info.put("game_id", desObj.getString("game_id"));
                                                    match_info.put("tourn_id", desObj.getString("tourn_id"));
                                                    match_info.put("match_id", desObj.getString("match_id"));

                                                    JSONObject my_team = new JSONObject();
                                                    my_team.put("team_name", desObj.getString("team_name"));
                                                    my_team.put("current_round_no", desObj.getString("current_round_no"));
                                                    my_team.put("team_id", desObj.getString("team_id"));
                                                    match_info.put("my_team", my_team);

                                                    JSONObject opponent_team = new JSONObject();
                                                    opponent_team.put("team_id", desObj.getString("opponent_team_id"));
                                                    opponent_team.put("team_name", desObj.getString("opponent_team_name"));
                                                    match_info.put("opponent_team", opponent_team);

                                                    response1.put("match_info", match_info);
                                                } catch (JSONException e) {
                                                    Logger.e("Json exception", e.getMessage());
                                                }
                                                messageModel.setExtras(response1.toString());
                                                messageModel.setGameImg(desObj.getString("game_image"));
                                                messageModel.setGameId(desObj.getString("match_id"));// i have set match id in gameid inorder to check in click
                                                break;
                                            case Config.MESSAGE_PLAYER_NOTI:
                                                messageModel.setFriend_id(desObj.getString("friend_user_id"));
                                                messageModel.setGameImg(desObj.getString("sender_profile"));
                                                break;

                                        }
                                        messageList.add(messageModel);

                                    }
                                    openSpecificFragment.setBadge(false, unReadCount);
                                    progressBar.setVisibility(View.GONE);
                                    offSet = messageList.size();
                                    messageAdapter.notifyDataSetChanged();
                                    messageListView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                }
                            } else {
                                if (offSet == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    messageListView.setVisibility(View.GONE);
                                } else {
                                    messageListView.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(getActivity(), errorText, res.getString("message"));
                                }
                            }
                        } catch (
                                JSONException e)

                        {
                            Logger.e("getNotificationListTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getNotificationListTask error res", e.getMessage() + " ");
                        }

                        if (offSet == 0) {
                            errorText.setText(errorMsg);
                            errorText.setVisibility(View.VISIBLE);
                            messageListView.setVisibility(View.GONE);
                        } else {
                            messageListView.setVisibility(View.VISIBLE);
                            AlertUtils.showSnack(getActivity(), errorText, errorMsg);
                        }
                    }
                }, "getNotificationListTask");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificFragment = null;
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_FRIEND_RESPONSE) {
                String respType = data.getStringExtra("resp_type");
                String not_id = data.getStringExtra("not_id");
                for (int i = 0; i < messageList.size(); i++) {
                    if (messageList.get(i).getMessageId().equals(not_id)) {
                        messageList.get(i).setRespType(respType);
                        messageAdapter.notifyItemChanged(i);
                        break;

                    }
                }
            }
        }
    }
}
