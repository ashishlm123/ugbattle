package com.emts.ugbattle.fragment.account;

import android.app.DownloadManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.AddFriendDetailActivity;
import com.emts.ugbattle.adapter.WithdrawalHistoryAdapter;
import com.emts.ugbattle.customviews.EndlessRecyclerViewScrollListener;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.DateUtils;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class WithdrawalHistoryFrag extends Fragment {
    ProgressBar progressBar, infiniteProgress;
    TextView errorText;
    RecyclerView withDrawalListView;
    ArrayList<OrderModel> withDrawalList;
    WithdrawalHistoryAdapter adapter;
    int limit = 100;
    int offset = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    boolean isFromPush = false;
    MessageModel messageModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            isFromPush = bundle.getBoolean("from_notification", false);
            messageModel = (MessageModel) bundle.getSerializable("mesg");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        withDrawalList = new ArrayList<>();
        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);
        errorText = view.findViewById(R.id.error_text);
        withDrawalListView = view.findViewById(R.id.listings);
        adapter = new WithdrawalHistoryAdapter(withDrawalList, getActivity());
        withDrawalListView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        withDrawalListView.setLayoutManager(layoutManager);

        infiniteScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (withDrawalList.size() >= limit) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        getWithdrawalHistoryTask(offset);
                    } else {
                        errorText.setText(getResources().getString(R.string.error_no_internet));
                        errorText.setVisibility(View.VISIBLE);
                        AlertUtils.showSnack(getActivity(), errorText, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        };
        withDrawalListView.addOnScrollListener(infiniteScrollListener);
        if (NetworkUtils.isInNetwork(getActivity())) {
            getWithdrawalHistoryTask(offset);
        } else {
            errorText.setVisibility(View.VISIBLE);
            errorText.setText(R.string.error_no_internet);
        }

        if (isFromPush) {
            if (NetworkUtils.isInNetwork(getActivity()) && messageModel.getMessageId() != null) {
                markReadTask();
            }
        }
    }

    private void markReadTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", messageModel.getMessageId());
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }

    private void getWithdrawalHistoryTask(final int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = volleyHelper.getPostParams();
        postParam.put("limit", String.valueOf(limit));
        postParam.put("offset", String.valueOf(offsetValue));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().getWithDrawalHistoryUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                infiniteProgress.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

                        JSONArray orderArray = res.getJSONArray("history_datas");

                        if (orderArray.length() == 0) {
                            if (offsetValue == 0) {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                            } else {
                                AlertUtils.showSnack(getActivity(), errorText, "No more withdrawal in list");
                            }
                        } else {
                            if (offset == 0) {
                                withDrawalList.clear();
                            }

                            for (int i = 0; i < orderArray.length(); i++) {
                                JSONObject eachObj = orderArray.getJSONObject(i);
                                OrderModel orderModel = new OrderModel();
                                orderModel.setOrderDate(DateUtils.convertDate1(eachObj.getString("request_date")));
                                orderModel.setOrderId(eachObj.getString("id"));
                                orderModel.setOrderAmount(eachObj.getString("amount"));
                                orderModel.setPaymentMode(eachObj.getString("payment_method"));
                                orderModel.setStatus(eachObj.getString("status"));
                                orderModel.setOneTicketCost(res.getString("one_ticket_amount"));
                                withDrawalList.add(orderModel);
                            }

                            offset = withDrawalList.size();
                            adapter.notifyDataSetChanged();
                            withDrawalListView.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                        }
                    } else {
                        if (offset == 0) {
                            errorText.setText(Html.fromHtml(res.getString("message")));
                            errorText.setVisibility(View.VISIBLE);
                            withDrawalListView.setVisibility(View.GONE);
                        } else {
                            withDrawalListView.setVisibility(View.VISIBLE);
                            AlertUtils.showSnack(getActivity(), errorText, " No more Games...");
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("moreGameListingTask json ex", e.getMessage());
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                infiniteProgress.setVisibility(View.GONE);
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("getWithdrawalHistoryTask error res", e.getMessage() + " ");
                }
                errorText.setText(Html.fromHtml(errorMsg));
                errorText.setVisibility(View.VISIBLE);


            }
        }, "getWithdrawalHistoryTask");
    }


}
