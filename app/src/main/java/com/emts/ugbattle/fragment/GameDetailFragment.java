package com.emts.ugbattle.fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.FragmentPageAdapter;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.view.Gravity.CENTER;

public class GameDetailFragment extends Fragment {
    GameMdl gameMdl;
    LinearLayout layoutGameUserIdForm;
    RelativeLayout layoutGameUserIdInfo;
    TabLayout tabLayout;
    ProgressBar progressBar;
    CoordinatorLayout detailHolder;
    EditText edtGmUsrId;
    Spinner spGmSrvr;
    TextView tvGmUsrId, tvGmSrvr, tvErrGmUsrId, errorText;
    ViewPager viewPager;

    FragmentPageAdapter adapter;

    ArrayList<String> serverLists = new ArrayList<>();
    PreferenceHelper prefHelper;
    boolean fromPush = false;
    boolean fromInbox = false;
    String notificationId = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            gameMdl = (GameMdl) bundle.getSerializable("game");
            fromPush = bundle.getBoolean("from_push");
            notificationId = bundle.getString("noti_id");
            fromInbox = bundle.getBoolean("from_inbox");
        }
        prefHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_game_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Layout Add Game User ID form initialization
        layoutGameUserIdForm = view.findViewById(R.id.layout_game_user_id_form);
        edtGmUsrId = view.findViewById(R.id.edt_game_user_id);
        spGmSrvr = view.findViewById(R.id.sp_server);
        final TextView tvTicketBalance = view.findViewById(R.id.tv_ticket_balance);
        tvTicketBalance.setText("Total Balance: " + prefHelper.getString(PreferenceHelper.APP_USER_TICKET_BALANCE, "0"));
        prefHelper.getPrefs().registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                tvTicketBalance.setText("Total Balance: " + prefHelper.getString(PreferenceHelper.APP_USER_TICKET_BALANCE, "0"));
            }
        });

        TextView tvFormTicketBalance = view.findViewById(R.id.ticket_balance);
        tvFormTicketBalance.setText(prefHelper.getString(PreferenceHelper.APP_USER_TICKET_BALANCE, "0"));


        //Layout Added Game User ID info initialization
        layoutGameUserIdInfo = view.findViewById(R.id.layout_game_user_id_info);
        tvGmUsrId = view.findViewById(R.id.tv_game_user_id);
        tvErrGmUsrId = view.findViewById(R.id.tv_error_game_user_id);
        tvGmSrvr = view.findViewById(R.id.tv_game_server);

        progressBar = view.findViewById(R.id.progress);
        errorText = view.findViewById(R.id.errorText);

        detailHolder = view.findViewById(R.id.lay_game_detail);

        final Button btnSaveGameUserId = view.findViewById(R.id.btn_save_game_user_id);
        btnSaveGameUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtils.hideInputMethod(getActivity(), errorText);
                if (TextUtils.isEmpty(edtGmUsrId.getText().toString())) {
                    tvErrGmUsrId.setVisibility(View.VISIBLE);
                } else {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        saveGameUserIDTask(edtGmUsrId.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(getActivity(), tvErrGmUsrId, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        });

        final ImageView edtGameUserInfo = view.findViewById(R.id.edt_game_user_id_info);
        edtGameUserInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutGameUserIdInfo.setVisibility(View.GONE);
                edtGmUsrId.setText(gameMdl.getGameUserId());

                int pos = serverLists.indexOf(gameMdl.getGameUserServer());
                if (pos >= 0) {
                    spGmSrvr.setSelection(pos);
                }
                layoutGameUserIdForm.setVisibility(View.VISIBLE);
            }
        });

        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout.setupWithViewPager(viewPager);

        //show game data
        if (gameMdl == null) return;

        //game server spinner
        try {
            serverLists.clear();
            JSONArray serverArray = new JSONArray(gameMdl.getGameServersArray());
            for (int i = 0; i < serverArray.length(); i++) {
                serverLists.add(serverArray.getString(i));
            }
            setSpinnerAdapter(serverLists);
        } catch (JSONException e) {
            Logger.e("set server spinner ex", e.getMessage() + " ");
        }

        if (!TextUtils.isEmpty(gameMdl.getGameUserId()) && !gameMdl.getGameUserId().equals("null")) {
            tvGmUsrId.setText("Game user Id : " + gameMdl.getGameUserId());
            tvGmSrvr.setText("Server : " + gameMdl.getGameUserServer());

            layoutGameUserIdInfo.setVisibility(View.VISIBLE);
        } else {
            layoutGameUserIdInfo.setVisibility(View.GONE);
            layoutGameUserIdForm.setVisibility(View.VISIBLE);
        }

        addTabs(viewPager);
    }

    private void saveGameUserIDTask(final String gameUserId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Please Wait...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("game_id", gameMdl.getGameId());
        postMap.put("game_user_id", gameUserId);
        postMap.put("server_name", spGmSrvr.getSelectedItem().toString());

        vHelper.addVolleyRequestListeners(Api.getInstance().addGameUserIdUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            pDialog.dismiss();
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                layoutGameUserIdForm.setVisibility(View.GONE);

                                gameMdl.setGameUserId(gameUserId);
                                gameMdl.setGameUserServer(spGmSrvr.getSelectedItem().toString());

                                tvGmUsrId.setText("Game user ID : " + gameMdl.getGameUserId());
                                tvGmSrvr.setText("Server : " + gameMdl.getGameUserServer());

                                TournamentFragment tFrag = (TournamentFragment) adapter.getItem(0);
                                tFrag.setServer(gameMdl.getGameUserServer());
                                tFrag.setGameMdl(gameMdl);
                                layoutGameUserIdInfo.setVisibility(View.VISIBLE);
                                AlertUtils.showAlertMessage(getActivity(), "Success", "Game User Id has been successfully added.",
                                        "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                addTabs(viewPager);
                                            }
                                        });

                            } else {
                                pDialog.dismiss();
                                AlertUtils.showSnack(getActivity(), detailHolder, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            Logger.e("userSignUpTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userSignUpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), detailHolder, errorMsg);
                    }
                }, "btnSaveGameUserIdTask");
    }

    private void setSpinnerAdapter(final ArrayList<String> gameServers) {
        // Creating adapter for spinner
        if (getActivity() == null) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() == null) return;
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_textview, gameServers);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spGmSrvr.setAdapter(dataAdapter);

                }
            }, 1000);
            return;
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_textview, gameServers);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spGmSrvr.setAdapter(dataAdapter);
    }

    private void addTabs(ViewPager viewPager) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("game", gameMdl);

        adapter = new FragmentPageAdapter(getChildFragmentManager());

        Fragment tournamentFragment = new TournamentFragment();
        tournamentFragment.setArguments(bundle);
        adapter.addFrag(tournamentFragment, "Tournaments");

        Fragment historyFragment = new HistoryFragment();
        historyFragment.setArguments(bundle);
        adapter.addFrag(historyFragment, "History");

        InvitationFragment invitationFragment = new InvitationFragment();
        invitationFragment.setArguments(bundle);
        invitationFragment.setInvitationCountCallback(new InvitationFragment.InvitationCount() {
            @Override
            public void setInvitation(int count) {
                try {
                    if (count > 0) {
                        tabLayout.getTabAt(2).setText("Invitations : " + count);
                    } else {
                        tabLayout.getTabAt(2).setText("Invitations");
                    }
                } catch (Exception e) {
                }
            }
        });
        invitationFragment.setAcceptRequestCallback(new InvitationFragment.AcceptRequestCallBack() {
            @Override
            public void setAcceptCallback() {
                try {
                    TournamentFragment toFrag = (TournamentFragment) adapter.getItem(0);
                    if (toFrag != null) {
                        toFrag.tournamentListingsTask(0);
                    }
                } catch (Exception e) {
                }
            }
        });

        adapter.addFrag(invitationFragment, "Invitations");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
        if (gameMdl.getGameUserId().equals("null") || TextUtils.isEmpty(gameMdl.getGameUserId())) {
            disableTab(1);
            disableTab(2);
        }

        if (fromPush || fromInbox) {
            if (NetworkUtils.isInNetwork(getActivity()) && notificationId != null) {
                markReadTask(notificationId);
            }
            viewPager.setCurrentItem(2);
        } else {
            viewPager.setCurrentItem(0);
        }
    }

    private void disableTab(int index) {
        TabLayout.Tab tab = tabLayout.getTabAt(index);
        if (tab == null) {
            Logger.e("DISSABLE TAB", "Tab here is null");
            return;
        }
        //custom tab layout
        TextView tabOne = new TextView(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        lp.gravity = CENTER;
        tabOne.setLayoutParams(lp);
        tabOne.setGravity(CENTER);
        tabOne.setText(tab.getText().toString() + " ");
        tabOne.setTextColor(getResources().getColor(R.color.grey));
        tabOne.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        tab.setCustomView(tabOne);
    }

    private void enableTab(int index) {
        TabLayout.Tab tab = tabLayout.getTabAt(index);
        Logger.e("on touch ", index + " ");

        if (tab == null) {
            Logger.e("enable TAB", "Tab here is null");
            return;
        }

        tab.getCustomView().setClickable(true);
        tab.getCustomView().setEnabled(true);
        tab.getCustomView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Logger.e("on touch ", "Tab here is null");
                return false;
            }
        });


    }

    private void markReadTask(String notificationId) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", notificationId);
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }

}
