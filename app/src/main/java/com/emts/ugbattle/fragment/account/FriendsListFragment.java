package com.emts.ugbattle.fragment.account;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.FriendsListAdapter;
import com.emts.ugbattle.customviews.EndlessRecyclerViewScrollListener;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FriendsListFragment extends Fragment {
    TextView tabFriendsLists, tabPlayersLists;

    TextView errorText;
    ProgressBar progressBar, infiniteProgress;
    RecyclerView friendsListing;
    FriendsListAdapter friendsListAdapter;
    ArrayList<UserModel> userLists = new ArrayList<>();
    ArrayList<UserModel> playersLists = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    int limit = 50;
    int offSet = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    EditText edtSearch;
    boolean isFromPush = false;
    String notificationId = null;

    boolean isInviteToTeam = false;
    TeamModel teamModel;
    int tabSelect = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            isInviteToTeam = bundle.getBoolean("isInviteToTeam");
            teamModel = (TeamModel) bundle.getSerializable("team");
            isFromPush = bundle.getBoolean("from_push");
            notificationId = bundle.getString("noti_id");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_friends_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabFriendsLists = view.findViewById(R.id.tab_friends_lists);
        tabPlayersLists = view.findViewById(R.id.tab_player_lists);

        errorText = view.findViewById(R.id.error_text);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);
        progressBar = view.findViewById(R.id.progress_bar);

        friendsListing = view.findViewById(R.id.friends_listings);
        edtSearch = view.findViewById(R.id.edt_search);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);

        final LinearLayoutManager friendsLayoutManager = new LinearLayoutManager(getActivity());
        friendsListing.setLayoutManager(friendsLayoutManager);
        friendsListAdapter = new FriendsListAdapter(getActivity(), playersLists, isInviteToTeam);
        friendsListAdapter.setUpdatePlayerCallBack(new FriendsListAdapter.UpdatePlayer() {
            @Override
            public void updatePlayer(int position, int type) {
                if (type == FriendsListAdapter.UpdatePlayer.INVITE) {
                    userLists.remove(playersLists.get(position));
                    friendsListAdapter.notifyItemChanged(position);
                    playersLists.remove(position);
                    friendsListAdapter.notifyDataSetChanged();
                } else if (type == FriendsListAdapter.UpdatePlayer.ADD_FRIEND) {
                    UserModel userModel = playersLists.get(position);
                    int mainIndex = userLists.indexOf(userModel);
                    userModel.setUserMemberStatus(Config.IS_AWAITING);
                    friendsListAdapter.notifyItemChanged(position);
                    userLists.set(mainIndex, userModel);
                }
            }
        });
        friendsListing.setAdapter(friendsListAdapter);
        friendsListing.setNestedScrollingEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                offSet = 0;
                edtSearch.setText("");
                if (isInviteToTeam) {
                    getUserListToInviteTask(offSet, "");
                } else {
                    getUserListTask(offSet, "");
                }

            }
        });


        if (teamModel != null && teamModel.getInvitationCount() != 0) {
            friendsListAdapter.setInvitationCount(teamModel.getInvitationCount());
        }

        if (isInviteToTeam) {
            friendsListAdapter.setTeam(teamModel);
        }

        infiniteScrollListener = new EndlessRecyclerViewScrollListener(friendsLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (userLists.size() >= limit) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        getUserListTask(offSet, "");
                    } else {
                        errorText.setText(getResources().getString(R.string.error_no_internet));
                        errorText.setVisibility(View.VISIBLE);
                        AlertUtils.showSnack(getActivity(), errorText, getResources().getString(R.string.error_no_internet));
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        };
        friendsListing.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            if (isInviteToTeam) {
                getUserListToInviteTask(offSet, "");
            } else {
                getUserListTask(offSet, "");
            }
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.VISIBLE);
        }

        tabFriendsLists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTab(view);
                filterFriendList();
            }
        });

        tabPlayersLists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTab(view);
                filterPlayer();
            }
        });


        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (keyEvent != null) {
                    if (!keyEvent.isShiftPressed()) {
                        if (!TextUtils.isEmpty(edtSearch.getText().toString())) {
                            offSet = 0;
                            if (isInviteToTeam) {
                                getUserListToInviteTask(offSet, edtSearch.getText().toString());
                            } else {
                                getUserListTask(offSet, edtSearch.getText().toString());
                            }
                        } else {
                            if (isInviteToTeam) {
                                getUserListToInviteTask(offSet, "");
                            } else {
                                getUserListTask(offSet, "");
                            }
                        }
                    }
                }
                return false;
            }
        });
        ImageView imgSearch = view.findViewById(R.id.searchImg);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(edtSearch.getText().toString())) {
                    offSet = 0;
                    if (isInviteToTeam) {
                        getUserListToInviteTask(offSet, edtSearch.getText().toString());
                    } else {
                        getUserListTask(offSet, edtSearch.getText().toString());
                    }
                } else {
                    if (isInviteToTeam) {
                        getUserListToInviteTask(offSet, "");
                    } else {
                        getUserListTask(offSet, "");
                    }
                }
            }
        });

        if (isFromPush) {
            if (NetworkUtils.isInNetwork(getActivity()) && notificationId != null) {
                markReadTask(notificationId);
            }
        }

    }

    private void getUserListToInviteTask(final int offSetValue, String searchValue) {
        AlertUtils.hideInputMethod(getActivity(), errorText);
        friendsListing.setVisibility(View.GONE);
        if (offSet == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("limit", String.valueOf(limit));
        postParam.put("offset", String.valueOf(offSetValue));
        postParam.put("game_id", teamModel.getGameId());
        postParam.put("team_id", teamModel.getTeamId());
        postParam.put("game_user_id", teamModel.getMyGameUserId());
        if (tabSelect == 1) {
            postParam.put("friend_global_field", searchValue);
        } else {
            postParam.put("player_global_field", searchValue);
        }


        vHelper.addVolleyRequestListeners(Api.getInstance().listUserToInviteToTeamUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray orderArray = res.getJSONArray("friend_list");
                                if (orderArray.length() == 0) {
                                    if (offSet == 0) {
                                        if (tabSelect == 1) {
                                            errorText.setText("You don't have any friends in your friend list");
                                        } else {
                                            errorText.setText("There is no any player in this tournament");
                                        }
                                        errorText.setVisibility(View.VISIBLE);
                                    } else {
                                        offSet = userLists.size();
                                        if (tabSelect == 1) {
                                            filterFriendList();
                                        } else {
                                            filterPlayer();
                                        }
                                    }
                                } else {
                                    if (offSet == 0) {
                                        userLists.clear();
                                    }
                                    for (int i = 0; i < orderArray.length(); i++) {
                                        JSONObject eachObj = orderArray.getJSONObject(i);
                                        UserModel friendsModel = new UserModel();
                                        friendsModel.setUserFullName(eachObj.getString("full_name"));
                                        friendsModel.setUserEmail(eachObj.getString("email"));
                                        friendsModel.setUserId(eachObj.getString("id"));
                                        friendsModel.setUserName(eachObj.getString("username"));
                                        friendsModel.setUserContact(eachObj.getString("contact_no"));

                                        if (eachObj.getString("profile_img") == null ||
                                                eachObj.getString("profile_img").equals("null")) {
                                            friendsModel.setUserProfilePic(res.getString("default_img_path"));
                                        } else {
                                            friendsModel.setUserProfilePic(res.getString("user_img_base_url") + eachObj.getString("profile_img"));
                                        }
                                        if (eachObj.getString("is_friend").equals("0")) {
                                            friendsModel.setFriend(false);
                                            friendsModel.setUserMemberStatus(eachObj.optString("inv_resp_status"));
                                        } else if (eachObj.getString("is_friend").equals("1")) {
                                            friendsModel.setFriend(true);
                                            friendsModel.setUserMemberStatus(eachObj.optString(" ")); // for testing only since change in api is remaining
                                        }
                                        userLists.add(friendsModel);
                                    }
                                    offSet = userLists.size();
                                    if (tabSelect == 1) {
                                        filterFriendList();
                                    } else {
                                        filterPlayer();
                                    }
                                }
                            } else {
                                if (offSet == 0) {
                                    errorText.setText(Html.fromHtml(res.getString("message")));
                                    errorText.setVisibility(View.VISIBLE);
                                    friendsListing.setVisibility(View.GONE);
                                } else {
                                    friendsListing.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(getActivity(), errorText, res.getString("message"));
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getUserListToInviteTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        infiniteProgress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = String.valueOf(Html.fromHtml(errorObj.getString("message")));
                        } catch (Exception e) {
                            Logger.e("getUserListToInviteTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getUserListToInviteTask");
    }

    private void filterFriendList() {
        playersLists.clear();
        for (int i = 0; i < userLists.size(); i++) {
            if (userLists.get(i).isFriend()) {
                playersLists.add(userLists.get(i));
            }
        }
        if (playersLists.size() == 0) {
            errorText.setText("You don't have any friends in your friend list");
            errorText.setVisibility(View.VISIBLE);
            friendsListing.setVisibility(View.GONE);
        } else {
            errorText.setVisibility(View.GONE);
            friendsListing.setVisibility(View.VISIBLE);
        }
        friendsListAdapter.notifyDataSetChanged();
    }

    private void getUserListTask(final int offsetValue, final String searchValue) {
        AlertUtils.hideInputMethod(getActivity(), errorText);
        if (offSet == 0) {
            friendsListing.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("limit", String.valueOf(limit));
        postParam.put("offset", String.valueOf(offsetValue));
        postParam.put("global_field", searchValue);
        vHelper.addVolleyRequestListeners(Api.getInstance().getAllUserList, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray frnArray = res.getJSONArray("friend_list");
                                if (frnArray.length() == 0) {
                                    if (offSet == 0) {
                                        if (tabSelect == 1) {
                                            errorText.setText("You don't have any friends in your friend list");
                                        } else {
                                            errorText.setText("There is no any player in this tournament");
                                        }
                                        errorText.setVisibility(View.VISIBLE);
                                    } else {
                                        offSet = userLists.size();
                                        if (tabSelect == 1) {
                                            filterFriendList();
                                        } else {
                                            filterPlayer();
                                        }
                                    }
                                } else {
                                    if (offSet == 0) {
                                        userLists.clear();
                                    }
                                    for (int i = 0; i < frnArray.length(); i++) {
                                        JSONObject eachObj = frnArray.getJSONObject(i);
                                        UserModel friendsModel = new UserModel();
                                        friendsModel.setUserFullName(eachObj.getString("full_name"));
                                        friendsModel.setUserEmail(eachObj.getString("email"));
                                        friendsModel.setUserId(eachObj.getString("id"));
                                        friendsModel.setUserName(eachObj.getString("username"));
                                        friendsModel.setUserContact(eachObj.getString("contact_no"));

                                        if (eachObj.getString("profile_img") == null ||
                                                eachObj.getString("profile_img").equals("null")) {
                                            friendsModel.setUserProfilePic(res.getString("default_img_path"));
                                        } else {
                                            friendsModel.setUserProfilePic(res.getString("user_img_base_url") + eachObj.getString("profile_img"));
                                        }
                                        if (eachObj.getString("is_friend").equals("0")) {
                                            friendsModel.setFriend(false);
                                            friendsModel.setUserMemberStatus(eachObj.optString("inv_resp_status"));
                                        } else if (eachObj.getString("is_friend").equals("1")) {
                                            friendsModel.setFriend(true);
                                            friendsModel.setUserMemberStatus(""); // for testing only since change in api is remaining
                                        }
                                        userLists.add(friendsModel);
                                    }
                                    offSet = userLists.size();
                                    if (tabSelect == 1) {
                                        filterFriendList();
                                    } else {
                                        filterPlayer();
                                    }
                                }
                            } else {
                                if (offSet == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    friendsListing.setVisibility(View.GONE);
                                } else {
                                    friendsListing.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(getActivity(), errorText, res.getString("message"));
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getUserListTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getUserListTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getUserListTask");
    }

    private void filterPlayer() {
        playersLists.clear();
        errorText.setVisibility(View.GONE);
        for (int i = 0; i < userLists.size(); i++) {
            if (!userLists.get(i).isFriend()) {
                playersLists.add(userLists.get(i));
            }
        }
        if (playersLists.size() == 0) {
            friendsListing.setVisibility(View.GONE);
            errorText.setText("You don't have any user in your Player list");
            errorText.setVisibility(View.VISIBLE);
        }
        friendsListing.setVisibility(View.VISIBLE);
        friendsListAdapter.notifyDataSetChanged();
    }

    private void selectTab(View view) {
        switch (view.getId()) {
            case R.id.tab_friends_lists:
                tabSelect = 1;
                tabFriendsLists.setBackgroundColor(getActivity().getResources().getColor(R.color.mainButtonColor));
                tabPlayersLists.setBackgroundColor(getActivity().getResources().getColor(R.color.darker_grey));
                break;

            case R.id.tab_player_lists:
                tabSelect = 2;
                tabPlayersLists.setBackgroundColor(getActivity().getResources().getColor(R.color.mainButtonColor));
                tabFriendsLists.setBackgroundColor(getActivity().getResources().getColor(R.color.darker_grey));
                break;
        }
    }

    private void markReadTask(String notificationId) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", notificationId);
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }
}
