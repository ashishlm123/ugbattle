package com.emts.ugbattle.fragment.newhome;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.ChatActivity;
import com.emts.ugbattle.adapter.MessageAdapter;
import com.emts.ugbattle.customviews.EndlessRecyclerViewScrollListener;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.ChatModel;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyMessagesFragment extends Fragment {
    TextView errorText;
    ProgressBar progressBar, infiniteProgress;

    MessageAdapter messageAdapter;
    RecyclerView conversationListings;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    ArrayList<MessageModel> conversationLists = new ArrayList<>();
    int limit = 10;
    int offSet = 0;

    PreferenceHelper preferenceHelper;

    ParentActivityInterface mainInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_messages, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        errorText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);

        conversationListings = view.findViewById(R.id.conversation_listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        conversationListings.setLayoutManager(linearLayoutManager);
        messageAdapter = new MessageAdapter(getActivity(), conversationLists);
        messageAdapter.setRecyclerItemClickListener(new MessageAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position) {
                //mark chat message read
                MessageModel msgMdl = conversationLists.get(position);
                if (!msgMdl.isRead()) {
                    markChatReadTask(conversationLists.get(position).getMessageId());
                }
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                if (msgMdl.getMessageType().equals(ChatModel.GROUP_MSG)) {
                    intent.putExtra("response", (String) msgMdl.getExtras());
                    startActivity(intent);
                } else if (msgMdl.getMessageType().equals(ChatModel.FRIEND_MSG)) {
                    UserModel friend = new UserModel();
                    friend.setUserId(msgMdl.getUserId());
                    intent.putExtra("friend", friend);
                    startActivity(intent);
                }
            }
        });
        messageAdapter.setMessageFragment(true);
        conversationListings.setAdapter(messageAdapter);
        conversationListings.setNestedScrollingEnabled(false);
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (conversationLists.size() >= limit) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        getMessageListTask(offSet);
                    } else {
                        errorText.setText(getResources().getString(R.string.error_no_internet));
                        errorText.setVisibility(View.VISIBLE);
                        AlertUtils.showSnack(getActivity(), errorText, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        };

        conversationListings.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            offSet = 0;
            getMessageListTask(offSet);
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.VISIBLE);
        }

        FloatingActionButton fabFriendList = view.findViewById(R.id.fabFriendList);
        fabFriendList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainInterface.openSpecificPage(view.getId());
            }
        });
        fabFriendList.hide();
    }

    private void getMessageListTask(final int offsetValue) {
        if (offSet == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("limit", String.valueOf(limit));
        postParam.put("offset", String.valueOf(offsetValue));

        vHelper.addVolleyRequestListeners(Api.getInstance().getMessageLists, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        conversationLists.clear();
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray conversationArray = res.getJSONArray("message_datas");
                                if (conversationArray.length() == 0) {
                                    if (offsetValue == 0) {
                                        errorText.setText("No conversations yet...");
                                        errorText.setVisibility(View.VISIBLE);
                                    } else {
                                        AlertUtils.showSnack(getActivity(), errorText, "");
                                    }
                                } else {
                                    if (offSet == 0) {
                                        conversationLists.clear();
                                    }

                                    int unReadCount = 0;
                                    for (int i = 0; i < conversationArray.length(); i++) {
                                        JSONObject eachConversation = conversationArray.getJSONObject(i);
                                        MessageModel messageModel = new MessageModel();

                                        if (eachConversation.getString("type").equals(ChatModel.FRIEND_MSG)) {
                                            messageModel.setMessageId(eachConversation.getString("id"));
                                            String messageCategory = eachConversation.getString("type");
                                            messageModel.setMessageType(messageCategory);

                                            String messageType = eachConversation.getString("message_type");
                                            messageModel.setMessageTime(eachConversation.getString("send_date"));

                                            JSONObject msgDescription = eachConversation.getJSONObject("description");
                                            if (messageType.equals(ChatModel.MESSAGE_TYPE_TEXT)) {
                                                messageModel.setMessagePreview(msgDescription.getString("message"));
                                            } else if (messageType.equals(ChatModel.MESSAGE_TYPE_ATTACHMENT)) {
                                                messageModel.setMessagePreview("{Attachment}");
                                            }

                                            String toUserId = eachConversation.getString("to_user_id");
                                            String fromUserId = eachConversation.getString("from_user_id");
                                            if (fromUserId.equals(preferenceHelper.getUserId())) {
                                                messageModel.setUserId(toUserId);
                                            } else {
                                                messageModel.setUserId(fromUserId);
                                            }

                                            messageModel.setUserName(msgDescription.getString("sender_name"));
                                            String sendProfilePic = msgDescription.getString("sender_profile");
                                            if (!TextUtils.isEmpty(sendProfilePic) && !sendProfilePic.equals("null")) {
                                                messageModel.setUserProfilePic(sendProfilePic);
                                            }
                                        } else if (eachConversation.getString("type").equals(ChatModel.GROUP_MSG)) {
                                            messageModel.setMessageId(eachConversation.getString("id"));
                                            String messageCategory = eachConversation.getString("type");
                                            messageModel.setMessageType(messageCategory);

                                            String messageType = eachConversation.getString("message_type");
                                            messageModel.setMessageTime(eachConversation.getString("send_date"));

                                            JSONObject msgDescription = eachConversation.getJSONObject("description");
                                            if (messageType.equals(ChatModel.MESSAGE_TYPE_TEXT)) {
                                                messageModel.setMessagePreview(msgDescription.getString("message"));
                                            } else if (messageType.equals(ChatModel.MESSAGE_TYPE_ATTACHMENT)) {

                                                messageModel.setMessagePreview("{Attachment}");
                                            }

                                            messageModel.setUserName(msgDescription.getString("team_name"));
                                            String gamePic = msgDescription.getString("game_image");
                                            if (!TextUtils.isEmpty(gamePic) && !gamePic.equals("null")) {
                                                messageModel.setUserProfilePic(gamePic);
                                            }

                                            JSONObject groupMessageResponse = new JSONObject();
                                            JSONObject match_info = new JSONObject();
                                            match_info.put("game_id", msgDescription.getString("game_id"));
                                            match_info.put("tourn_id", msgDescription.getString("tourn_id"));
                                            match_info.put("match_id", msgDescription.getString("match_id"));

                                            JSONObject my_team = new JSONObject();
                                            my_team.put("team_name", msgDescription.getString("team_name"));
                                            my_team.put("current_round_no", msgDescription.getString("current_round_no"));
                                            my_team.put("team_id", msgDescription.getString("team_id"));
                                            match_info.put("my_team", my_team);

                                            JSONObject opponent_team = new JSONObject();
                                            opponent_team.put("team_id", msgDescription.getString("opponent_team_id"));
                                            opponent_team.put("team_name", msgDescription.getString("opponent_team_name"));
                                            match_info.put("opponent_team", opponent_team);

                                            groupMessageResponse.put("match_info", match_info);
                                            messageModel.setExtras(groupMessageResponse.toString());
                                        }
                                        if (eachConversation.getString("status").equals("3")) {
                                            // deleted messages
                                            continue;
                                        } else if (eachConversation.getString("status").equals("2")) {
                                            messageModel.setRead(true);
                                        } else {
                                            messageModel.setRead(false);
                                            unReadCount++;
                                        }
                                        conversationLists.add(messageModel);
                                    }
                                    mainInterface.setBadge(true, unReadCount);
                                    offSet = conversationLists.size();
                                    messageAdapter.notifyDataSetChanged();
                                    conversationListings.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                }
                            } else {
                                if (offSet == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    conversationListings.setVisibility(View.GONE);
                                } else {
                                    conversationListings.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(getActivity(), errorText, res.getString("message"));
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getMessageListTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getFriendListTask error res", e.getMessage() + " ");
                        }

                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getMessageListTask");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainInterface = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        mainInterface = null;
        super.onDetach();
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("getMessageListTask");
        super.onPause();
    }

    private void markChatReadTask(String mesgId) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("msg_id", mesgId);
        vHelper.addVolleyRequestListeners(Api.getInstance().readMessageApi, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                try {
                                    mainInterface.setBadge(true, resObj.getInt("no_of_message"));
                                } catch (Exception e) {
                                }
                                try {
                                    mainInterface.setBadge(false, resObj.getInt("no_of_note"));
                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("markReadTask res ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markChatReadTask");
    }
}
