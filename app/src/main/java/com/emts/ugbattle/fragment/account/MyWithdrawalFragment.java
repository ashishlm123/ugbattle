package com.emts.ugbattle.fragment.account;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MyWithdrawalFragment extends Fragment {
    LinearLayout holderWithdrawal;
    EditText etAmount, etEmail, etContact;
    TextView tvBalance, tvOneTicketCost, errorText, errorAmount,
            errorEmail, errorContact;
    ProgressBar progressBar;
    ArrayList<String> bankList = new ArrayList<>();
    boolean isValid = true;
    Button btnRequestWithdraw;
    int ticketBalance = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_withdrawl, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        holderWithdrawal = view.findViewById(R.id.holderWithdrawal);
        errorText = view.findViewById(R.id.errorText);
        progressBar = view.findViewById(R.id.progress);

        tvBalance = view.findViewById(R.id.ticket_balance);
        tvOneTicketCost = view.findViewById(R.id.ticket_value);

        etAmount = view.findViewById(R.id.edt_withdraw_amt);
        etEmail = view.findViewById(R.id.edt_email);
        etContact = view.findViewById(R.id.et_contact);
        errorAmount = view.findViewById(R.id.error_amount);
        errorContact = view.findViewById(R.id.error_contact_no);
        errorEmail = view.findViewById(R.id.error_email);


        btnRequestWithdraw = view.findViewById(R.id.btnRequestWithdraw);
        btnRequestWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtils.hideInputMethod(getActivity(), etAmount);
                String amount = etAmount.getText().toString().trim();
                try {
                    if (Double.parseDouble(amount) < 10) {
                        errorAmount.setText("Min withdraw ticket is 10. Please enter 10 tickets or more");
                        errorAmount.setVisibility(View.VISIBLE);
                    } else if (Integer.parseInt(amount) > ticketBalance) {
                        errorAmount.setText("Invalid amount entered. The amount you entered exceeded your balance");
                        errorAmount.setVisibility(View.VISIBLE);
                    } else {
                        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
                            errorEmail.setText("Please enter valid email address");
                            errorEmail.setVisibility(View.VISIBLE);
                        } else {
                            errorAmount.setVisibility(View.GONE);
                            AlertUtils.showAlertMessage(getActivity(), false, "", "Are you sure want to withdraw?", "No", "Yes", new AlertUtils.OnAlertButtonClickListener() {
                                @Override
                                public void onAlertButtonClick(boolean isPositiveButton) {
                                    if (isPositiveButton) {
                                    } else {
                                        requestWithdrawalTask();
                                    }
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    Logger.e("btnreq ex", e.getMessage() + " ");

                }
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            getWithdrawalTask();
        } else {
            holderWithdrawal.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }

        etAmount.addTextChangedListener(textWatcher);
        etEmail.addTextChangedListener(textWatcher);
        etContact.addTextChangedListener(textWatcher);
    }

    private void requestWithdrawalTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(getActivity(), "Please wait..");
        progressDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("amount", etAmount.getText().toString().trim());
        postParam.put("payment_method", Config.PAYPAL);
        postParam.put("email", etEmail.getText().toString());
        postParam.put("contact_no", etContact.getText().toString());
        vHelper.addVolleyRequestListeners(Api.getInstance().withDrawalUsingCard, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        etAmount.setText("");
                        etContact.setText("");
                        etEmail.setText("");
                        AlertUtils.showAlertMessage(getActivity(), "Success Message", (res.getString("message")), "Ok", "", null);
                    } else {
                        AlertUtils.showAlertMessage(getActivity(), "Error Message", (res.getString("message")), "Ok", "", null);
                    }
                } catch (Exception e) {
                    Logger.e("requestWithdrawalTask ex", e.getMessage() + " ");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = String.valueOf(Html.fromHtml(errorObj.getString("message")));
                } catch (Exception e) {
                    Logger.e("requestWithdrawalTask error res", e.getMessage() + " ");
                }

                AlertUtils.showAlertMessage(getActivity(), "Error Message", errorMsg);
            }
        }, "requestWithdrawalTask");
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (TextUtils.isEmpty(etAmount.getText().toString().trim()) ||
                    TextUtils.isEmpty(etEmail.getText().toString().trim()) ||
                    TextUtils.isEmpty(etContact.getText().toString().trim())) {
                errorEmail.setVisibility(View.GONE);
                btnRequestWithdraw.setBackground(getResources().getDrawable(R.drawable.rounded_grey));
                btnRequestWithdraw.setClickable(false);
                btnRequestWithdraw.setEnabled(false);

            } else {
                btnRequestWithdraw.setBackground(getResources().getDrawable(R.drawable.rounded_blue));
                btnRequestWithdraw.setClickable(true);
                btnRequestWithdraw.setEnabled(true);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void getWithdrawalTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getWithDrawal, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONObject userInfo = res.getJSONObject("user_info");

                        ticketBalance = Integer.parseInt(userInfo.getString("ticket_balance"));
                        tvBalance.setText(userInfo.getString("ticket_balance"));
                        tvOneTicketCost.setText("1 Ticket equal to " + res.getString("default_currency_sign") +
                                " " + res.getString("get_one_ticket_price") + ":");

                        JSONArray bankArray = res.getJSONArray("banks");

                        for (int i = 0; i < bankArray.length(); i++) {
                            bankList.add(bankArray.getString(i));
                        }
                        holderWithdrawal.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {
                    Logger.e("getWithdrawalTask ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = String.valueOf(Html.fromHtml(errorObj.getString("message")));
                } catch (Exception e) {
                    Logger.e("getWithdrawalTask error res", e.getMessage() + " ");
                }

                errorText.setText(errorMsg);
                errorText.setVisibility(View.VISIBLE);
            }
        }, "getWithdrawalTask");


    }

}
