package com.emts.ugbattle.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.GameAdapter;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2018-03-19.
 */

public class HomeFrag extends Fragment {
    RecyclerView homeGameListings;
    ProgressBar progress;
    TextView errorText, tvTicketBlc;
    ArrayList<GameMdl> gameList;
    GameAdapter gameAdapter;
    ParentActivityInterface openSpecificFragment;
    ArrayList<String> images = new ArrayList<>();
    PreferenceHelper helper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = PreferenceHelper.getInstance(getActivity());
        gameList = new ArrayList<>();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificFragment = null;
        VolleyHelper.getInstance(getActivity()).cancelRequest("gameListingsTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("getBannerImageTask");
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("gameListingsTask");
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView userImg = view.findViewById(R.id.user_img);
        String imgName = helper.getString(PreferenceHelper.APP_USER_IMG, "");
        if (!TextUtils.isEmpty(imgName)) {
            Picasso.with(getActivity()).load(imgName).into(userImg);
        }
        TextView tvUsername = view.findViewById(R.id.tvUsername);
        tvUsername.setText(helper.getString(PreferenceHelper.APP_USER_NAME, ""));
        tvTicketBlc = view.findViewById(R.id.ticket_balance);
        updateTicketBalance();

        TextView tvMoreGame = view.findViewById(R.id.tvMoreGame);
        tvMoreGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        ImageView buyTicket = view.findViewById(R.id.buy_ticket);
        buyTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSpecificFragment.openSpecificPage(view.getId());
            }
        });

        progress = view.findViewById(R.id.progress);
        errorText = view.findViewById(R.id.errorText);

        homeGameListings = view.findViewById(R.id.gameListHome);
        homeGameListings.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        gameAdapter = new GameAdapter(getActivity(), gameList);
        gameAdapter.setGrid(true);
        homeGameListings.setAdapter(gameAdapter);

        gameAdapter.setRecyclerItemClickListener(new GameAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position) {
                openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.GAME_DETAIL_PAGE_ID, gameList.get(position));
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            gameListingsTask();
        } else {
            homeGameListings.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }

        if (NetworkUtils.isInNetwork(getActivity())) {
            getBannerImageTask(view);
        } else {
            AlertUtils.showSnack(getActivity(), buyTicket, getActivity().getResources().getString(R.string.error_no_internet));
        }
    }

    public void updateTicketBalance() {
        try {
            tvTicketBlc.setText(helper.getString(PreferenceHelper.APP_USER_TICKET_BALANCE, ""));
        } catch (Exception e) {
        }
    }

    private void getBannerImageTask(final View view) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getBannerImage, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        images.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray bannerImgArray = res.getJSONArray("banner_imgs");
                                String imgDir = res.getString("banner_base_url");
                                for (int i = 0; i < bannerImgArray.length(); i++) {
                                    JSONObject eachImg = bannerImgArray.getJSONObject(i);
                                    images.add(imgDir + eachImg.getString("file_name"));
                                }
                                setImageTask(view, images);
                            }
                        } catch (JSONException e) {
                            Logger.e("getBannerImageTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getBannerImageTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showSnack(getActivity(), errorText, errorMsg);
                    }
                }, "getBannerImageTask");
    }

    private void setImageTask(View view, final ArrayList<String> images) {
        final SliderLayout imageSlider = view.findViewById(R.id.slider);
        final PagerIndicator pagerIndicator = view.findViewById(R.id.custom_indicator);

        imageSlider.stopAutoCycle();
        imageSlider.setVisibility(View.GONE);
        for (int i = 0; i < images.size(); i++) {
            DefaultSliderView defaultSlider = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSlider
                    .image(images.get(i))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);

            imageSlider.addSlider(defaultSlider);
        }

        if (images.size() > 1) {
            // play from first image, when all images loaded
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                        imageSlider.setCustomIndicator(pagerIndicator);
//                        imageSlider.setCurrentPosition(0, false);
//                        imageSlider.setCurrentPosition(0, false); //this hangs and gives ANR
                        //so instead of going to first go to last and move to next
                        imageSlider.setCurrentPosition(images.size() - 1, false);
                        imageSlider.moveNextPosition();
                        pagerIndicator.onPageSelected(0);

                        imageSlider.setDuration(5000);
                        imageSlider.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imageSlider.startAutoCycle();
                                } catch (Exception e) {
                                }
                            }
                        }, 5400);
                    } catch (Exception e) {
                        Logger.e("reflection ex", e.getMessage() + " ");
                    }
                }
            }, 900);

        } else {
            imageSlider.setVisibility(View.VISIBLE);
            pagerIndicator.setVisibility(View.GONE);
            imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            imageSlider.stopAutoCycle();
        }
    }

    private void gameListingsTask() {
        progress.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        homeGameListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().gameListingsUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                gameList.clear();
                                String gameImgDir = res.getString("game_image_url");

                                JSONArray gamesArray = res.getJSONArray("games");
                                for (int i = 0; i < gamesArray.length(); i++) {
                                    JSONObject eachGames = gamesArray.getJSONObject(i);

                                    GameMdl gameMdl = new GameMdl();
                                    gameMdl.setGameId(eachGames.getString("game_id"));
                                    gameMdl.setGameName(eachGames.getString("game_name"));
                                    String gameImg = eachGames.getString("game_image");
                                    if (!TextUtils.isEmpty(gameImg)) {
                                        gameMdl.setGameImg(gameImgDir + gameImg);
                                    } else {
                                        gameMdl.setGameImg("");
                                    }
                                    gameMdl.setGameDescription(eachGames.getString("description"));
                                    gameMdl.setGameServersArray(eachGames.getString("server_names"));
                                    gameMdl.setGameUserId(eachGames.getString("game_user_id"));
                                    gameMdl.setGameUserServer(eachGames.getString("game_user_server"));

                                    gameList.add(gameMdl);
                                }
                                gameAdapter.notifyDataSetChanged();
                                homeGameListings.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                                homeGameListings.setVisibility(View.VISIBLE);
                            } else {
                                progress.setVisibility(View.GONE);
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                homeGameListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("gameListingsTask json ex: ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("gameListingsTask error res", e.getMessage() + " ");
                        }
                        progress.setVisibility(View.GONE);
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        homeGameListings.setVisibility(View.GONE);
                    }
                }, "gameListingsTask");
    }


    @Override
    public void onPause() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("gameListingsTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("getBannerImageTask");
        super.onPause();
    }
}
