package com.emts.ugbattle.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.TournamentModel;

import java.util.HashMap;

public class ViewStartGameFragment extends Fragment {
    TournamentModel gameMdl;
    Bundle bundle;
    TextView tabCreateJoin, tabRound, tabFinalRound;
    FrameLayout fragmentContainer;
    FragmentManager fm;
    boolean isFromPush = false;
    boolean isFromLobby = false;
    String notificationId = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle != null) {
            gameMdl = (TournamentModel) bundle.getSerializable("game");
            isFromPush = bundle.getBoolean("from_push", false);
            notificationId = bundle.getString("noti_id");
            isFromLobby = bundle.getBoolean("fromlobby", false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_view_start_game, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabCreateJoin = view.findViewById(R.id.tab_create_join);
        tabRound = view.findViewById(R.id.tab_round);
        tabFinalRound = view.findViewById(R.id.tab_final_round);
        fragmentContainer = view.findViewById(R.id.frag_container);

        fm = getChildFragmentManager();

        if (gameMdl.getCurrentRoundNo() > 0) {
            Fragment round1Frag = new RoundFragment();
            round1Frag.setArguments(bundle);
            if (gameMdl.getCurrentRoundNo() == gameMdl.getTotalRounds()) {
                selectTab(tabFinalRound);
                tabRound.setText("Round " + (gameMdl.getCurrentRoundNo() - 1));
            } else {
                tabRound.setText("Round " + gameMdl.getCurrentRoundNo());
                selectTab(tabRound);
            }
            fm.beginTransaction().replace(R.id.frag_container, round1Frag).commit();
        } else {
            Fragment createJoinFrag = new CreateJoinTeamFragment();
            createJoinFrag.setArguments(bundle);

            fm.beginTransaction().replace(R.id.frag_container, createJoinFrag).commit();
            selectTab(tabCreateJoin);
        }

        if (isFromPush) {
            if (NetworkUtils.isInNetwork(getActivity())) {
                markReadTask(notificationId);
            }
        }

        if(isFromLobby){
            startChallenge(gameMdl);
        }

    }

    void selectTab(View view) {
        switch (view.getId()) {
            case R.id.tab_create_join:
                tabCreateJoin.setBackgroundResource(R.drawable.bg_round_select);
                tabRound.setBackgroundResource(0);
                tabFinalRound.setBackgroundResource(0);
                break;
            case R.id.tab_round:
                tabCreateJoin.setBackgroundResource(0);
                tabRound.setBackgroundResource(R.drawable.bg_round_select);
                tabFinalRound.setBackgroundResource(0);
                break;
            case R.id.tab_final_round:
                tabCreateJoin.setBackgroundResource(0);
                tabRound.setBackgroundResource(0);
                tabFinalRound.setBackgroundResource(R.drawable.bg_round_select);
                break;
        }
    }

    public void startGame() {
        Fragment round1Frag = new RoundFragment();
        gameMdl.setCurrentRoundNo(1);
        Bundle bundle = new Bundle();
        bundle.putSerializable("game", gameMdl);
        round1Frag.setArguments(bundle);

        fm.beginTransaction().replace(R.id.frag_container, round1Frag).commit();
        selectTab(tabRound);
    }

    public void startChallenge(GameMdl gameMdl) {
        Fragment onGoingChallenge = new OnGoingChallengeFrag();
        bundle.putSerializable("game", gameMdl);
        onGoingChallenge.setArguments(bundle);

        fm.beginTransaction().replace(R.id.frag_container, onGoingChallenge).commit();

    }

    public void permissionGrant() {
        Fragment fragment = fm.findFragmentById(R.id.frag_container);
        if (fragment instanceof OnGoingChallengeFrag) {
            ((OnGoingChallengeFrag) fragment).permissionGrant();
        }
    }

    private void markReadTask(String notificationId) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", notificationId);
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }
}