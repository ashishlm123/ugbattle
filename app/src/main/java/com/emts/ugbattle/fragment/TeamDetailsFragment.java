package com.emts.ugbattle.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.PlayerListsAdapter;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TeamDetailsFragment extends Fragment {
    TextView tvTeamNameName;
    RecyclerView playerListView;
    ProgressBar progressBar;
    TextView errorText;
    SwipeRefreshLayout swipeRefreshLayout;

    PlayerListsAdapter playerListsAdapter;
    ArrayList<UserModel> teamPlayerLists;
    Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_team_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bundle = getArguments();
        if (bundle == null) {
            return;
        }
        String teamId = bundle.getString("team_id");

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setEnabled(false);

        tvTeamNameName = view.findViewById(R.id.tv_team_name);
        errorText = view.findViewById(R.id.errorText);
        progressBar = view.findViewById(R.id.progress);

        playerListView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        playerListView.setLayoutManager(linearLayoutManager);

        teamPlayerLists = new ArrayList<>();
        playerListsAdapter = new PlayerListsAdapter(getActivity(), teamPlayerLists);
        playerListView.setAdapter(playerListsAdapter);
        playerListView.setNestedScrollingEnabled(false);

        if (NetworkUtils.isInNetwork(getActivity())) {
            getTeamPlayerLists(teamId);
        } else {
            progressBar.setVisibility(View.GONE);
            playerListView.setVisibility(View.GONE);
            errorText.setText(getResources().getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        VolleyHelper.getInstance(getActivity()).cancelRequest("getTeamPlayerLists");
    }

    private void getTeamPlayerLists(String teamID) {
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("team_id", teamID);

        vHelper.addVolleyRequestListeners(Api.getInstance().getTeamLists, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject teamInfo = res.getJSONObject("team_data");
                                tvTeamNameName.setText(teamInfo.getString("team_name"));

                                String teamLeaderId = teamInfo.getString("creater_id");

                                String defaultImg = res.getString("default_img_path");
                                String userImageBaseUrl = res.getString("user_img_base_url");

                                JSONArray playerArray = teamInfo.getJSONArray("members");

                                for (int i = 0; i < playerArray.length(); i++) {
                                    JSONObject eachPlayer = playerArray.getJSONObject(i);
                                    UserModel playerModel = new UserModel();
                                    playerModel.setUserId(eachPlayer.getString("id"));
                                    playerModel.setGameUserId(eachPlayer.getString("game_user_id"));
                                    playerModel.setUserName(eachPlayer.getString("username"));
                                    playerModel.setUserFullName(eachPlayer.getString("full_name"));
                                    if (playerModel.getUserId().equalsIgnoreCase(teamLeaderId)) {
                                        playerModel.setUserMemberStatus("Leader");
                                    } else {
                                        playerModel.setUserMemberStatus("Member");
                                    }

                                    String playerImg = eachPlayer.getString("profile_img");
                                    if (playerImg.equalsIgnoreCase("null") || TextUtils.isEmpty(playerImg)) {
                                        playerModel.setUserProfilePic(defaultImg);
                                    } else {
                                        playerModel.setUserProfilePic(userImageBaseUrl + playerImg);
                                    }
                                    if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.LEADER)) {
                                        teamPlayerLists.add(0, playerModel);
                                    } else {
                                        teamPlayerLists.add(playerModel);
                                    }
                                }
                                playerListView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                                playerListsAdapter.notifyDataSetChanged();
                            } else {
                                progressBar.setVisibility(View.GONE);
                                playerListView.setVisibility(View.GONE);
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("getTeamPlayerLists json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getTeamPlayerLists error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getTeamPlayerLists");


    }
}
