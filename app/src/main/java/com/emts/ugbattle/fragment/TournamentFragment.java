package com.emts.ugbattle.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.TournamentAdapter;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.TournamentModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.emitter.Emitter;

public class TournamentFragment extends Fragment {
    TournamentAdapter tournamentAdapter;
    ArrayList<TournamentModel> filterLists = new ArrayList<>();
    ArrayList<TournamentModel> allTournamentLists = new ArrayList<>();
    RecyclerView tournamentListings;
    ParentActivityInterface openSpecificFragment;

    ProgressBar progressBar, infiniteProgressBar;
    TextView errorText;
    GameMdl gameMdl;
    boolean isJoinedToTour;

    private int limit = 10000;
    private int offset = 0;
    io.socket.client.Socket socketJs;
    PreferenceHelper helper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            gameMdl = (GameMdl) bundle.getSerializable("game");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VolleyHelper.getInstance(getContext()).cancelRequest("tournamentListingsTask");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tournament, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgressBar = view.findViewById(R.id.infinite_progress_bar);
        errorText = view.findViewById(R.id.error_text);

        tournamentListings = view.findViewById(R.id.listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        tournamentListings.setLayoutManager(linearLayoutManager);
        tournamentAdapter = new TournamentAdapter(getActivity(), filterLists);
        tournamentListings.setAdapter(tournamentAdapter);
        tournamentListings.setNestedScrollingEnabled(false);

        tournamentAdapter.setRecyclerItemClickListener(new TournamentAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position) {
                TournamentModel tMdl = allTournamentLists.get(position);
                openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, tMdl);
            }
        });

        if (TextUtils.isEmpty(gameMdl.getGameUserId()) || gameMdl.getGameUserId().equals("null")) {
            tournamentAdapter.setInfoAdded(false);
        } else {
            tournamentAdapter.setInfoAdded(true);
        }

        if (NetworkUtils.isInNetwork(getActivity())) {
            tournamentListingsTask(offset);
        } else {
            errorText.setText(getActivity().getResources().getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    public void tournamentListingsTask(int offsetValue) {
//        if (offset == 0) {
        progressBar.setVisibility(View.VISIBLE);
        tournamentListings.setVisibility(View.GONE);
//        } else {
//            infiniteProgressBar.setVisibility(View.VISIBLE);
//        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("game_id", gameMdl.getGameId());
        postMap.put("server_name", gameMdl.getGameUserServer());
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offsetValue));

        vHelper.addVolleyRequestListeners(Api.getInstance().tournamentListings, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
//                infiniteProgressBar.setVisibility(View.GONE);
                        errorText.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray trnmntArray = res.getJSONArray("all_tournaments");

                                if (trnmntArray.length() == 0) {
                                    errorText.setText("No tournaments available for this game");
                                    errorText.setVisibility(View.VISIBLE);
                                } else {
//                            if (offset == 0) {
                                    allTournamentLists.clear();
//                            }
                                    for (int i = 0; i < trnmntArray.length(); i++) {
                                        JSONObject echTrnmnt = trnmntArray.getJSONObject(i);

                                        TournamentModel tMdl = new TournamentModel();
                                        tMdl.setGameName(gameMdl.getGameName());
                                        tMdl.setTrnmntId(echTrnmnt.getString("tourn_id"));
                                        tMdl.setGameId(echTrnmnt.getString("game_id"));
                                        tMdl.setGameServersArray(echTrnmnt.getString("server_name"));
                                        tMdl.setTrnmntName(echTrnmnt.getString("tourn_name"));
                                        tMdl.setRequiredTeams(echTrnmnt.getString("req_team_no"));
                                        tMdl.setTotalRounds(Integer.parseInt(echTrnmnt.getString("round_no")));
                                        tMdl.setTcktsPerPerson(echTrnmnt.getString("ticket_per_person"));
                                        tMdl.setNumOfTeamsEntered(echTrnmnt.getString("num_team_entered"));
                                        tMdl.setNumOfTeamsReady(echTrnmnt.getString("num_team_ready"));
                                        tMdl.setPrizePerPerson(echTrnmnt.getString("prize"));
                                        tMdl.setGameUserId(gameMdl.getGameUserId());
                                        tMdl.setTeamId(echTrnmnt.optString("team_id"));
                                        if (!tMdl.getTeamId().equals("0")) {
                                            isJoinedToTour = true;
                                        }
                                        try {
                                            tMdl.setCurrentRoundNo(Integer.parseInt(echTrnmnt.getString("current_round_no")));
                                        } catch (Exception e) {
                                            tMdl.setCurrentRoundNo(0);
                                        }

                                        allTournamentLists.add(tMdl);
                                    }
                                    String serverName = gameMdl.getGameUserServer();
                                    if (!TextUtils.isEmpty(serverName) && !serverName.equalsIgnoreCase("null")) {
                                        setServer(gameMdl.getGameUserServer());
                                    } else {
                                        filterLists.addAll(allTournamentLists);
                                        tournamentAdapter.setJoinedInAnyOneTournament(isJoinedToTour);
                                        tournamentAdapter.notifyDataSetChanged();
                                        tournamentListings.setVisibility(View.VISIBLE);
                                        errorText.setVisibility(View.GONE);
                                    }
                                }
                                try {
                                    connectToServerJs();
                                    Logger.e("socket connection called ", " u r here" + " ");
                                } catch (URISyntaxException e) {
                                    Logger.e("socket connection ex", e.getMessage() + " ");
                                }
                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                tournamentListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("tournamentListingsTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("tournamentListingsTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        tournamentListings.setVisibility(View.GONE);
                    }
                }, "tournamentListingsTask");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificFragment = null;
        super.onDetach();
    }

    public void setServer(String server) {
        filterLists.clear();
        isJoinedToTour = false;
        for (int i = 0; i < allTournamentLists.size(); i++) {
            if (allTournamentLists.get(i).getGameServersArray().equals(server)) {
                TournamentModel tournamentModel = allTournamentLists.get(i);
                if (!tournamentModel.getTeamId().equals("0")) {
                    isJoinedToTour = true;
                }
                filterLists.add(tournamentModel);
            }
        }
        if (filterLists.size() == 0) {
            tournamentListings.setVisibility(View.GONE);
            errorText.setText("No tournaments in " + gameMdl.getGameUserServer() + " Server");
            errorText.setVisibility(View.VISIBLE);
        } else {
            tournamentAdapter.setJoinedInAnyOneTournament(isJoinedToTour);
            tournamentAdapter.notifyDataSetChanged();
            tournamentListings.setVisibility(View.VISIBLE);
            errorText.setVisibility(View.GONE);
        }
    }

    public void setGameMdl(GameMdl gameMdl) {
        this.gameMdl = gameMdl;
        tournamentAdapter.setInfoAdded(true);
        tournamentAdapter.notifyDataSetChanged();
    }

    private void connectToServerJs() throws URISyntaxException {
        if (socketJs != null) {
            if (socketJs.connected()) {
                socketJs.off();
                socketJs.disconnect();
            }
        }

        String nodeServerHostUrl = helper.getString(PreferenceHelper.NODE_SEVER, Api.getInstance().nodeUrl)
                + ":" + helper.getString(PreferenceHelper.NODE_PORT, Api.getInstance().nodePort);
        socketJs = IO.socket(nodeServerHostUrl);
        Logger.e("nodeJs url", nodeServerHostUrl);
        socketJs.on(io.socket.client.Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("nodeJs def connect call", "Object : ");

                if (socketJs.connected()) {
                    Logger.e("nodeJs connected", socketJs.toString() + "");
                    if (filterLists.size() > 0) {
                        emitConnectAndJoinTeamRoom(helper.getUserId(), filterLists.get(0).getGameId());
                    } else {
//                        socketJs.disconnect();
                    }
                    listenForReadyTeam();
                } else {
                    try {
                        connectToServerJs();
                    } catch (URISyntaxException e) {
                        Logger.e("socket_ex", e.getMessage() + " ");
                    }
                }

            }

        }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("nodeJs def. disconnect", "Object " + args.length);
//                connectionConfirm = false;
//                socketJs.off();
            }
        });
        socketJs.connect();
    }

    //    START NODE JS LISTENERS
    public void emitConnectAndJoinTeamRoom(String hostId, String gameId) {
        if (socketJs.connected()) {
            Logger.e("nodeJs emit for connected and join team room", "emitter for socket connected and join to team room");
            //emitter to post data
            try {
                JSONObject roomObj = new JSONObject();
                roomObj.put("user_id", hostId);
                roomObj.put("game_id", gameId);
                Logger.e("nodeJs room emitter data", roomObj.toString() + " **");

                socketJs.emit("connect_to_tournament_viewer_list", roomObj, new Ack() {
                    @Override
                    public void call(Object... args) {
                        Logger.e("nodeJs room emit ack", "Ack received for auction_room :" +
                                (args.length > 0 ? args[0].toString() : "0"));
                    }
                });
            } catch (JSONException e) {
                Logger.e("nodeJs ex15", e.getMessage() + "");
            }
        }
    }

    public void listenForReadyTeam() {
        if (socketJs.connected()) {
            Logger.e("nodeJs listener for number of player joined in team", "listener set for number of player");

//            socketJs.off("get_team_join_information");
            socketJs.on("get_team_join_information", new Emitter.Listener() {
                @SuppressLint("NewApi")
                @Override
                public void call(final Object... args) {
                    Logger.e("nodeJs response for listener -->", "get_team_join_information : " + args.length);

                    if (args.length > 0) {
                        if (getActivity() == null) return;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Logger.printLongLog("nodeJs get_team_join_information", args[0].toString() + " **");
                                try {
                                    JSONObject tournamentObj = new JSONObject(args[0].toString());
                                    for (int i = 0; i < filterLists.size(); i++) {
                                        if (tournamentObj.getString("tourn_id").equals(filterLists.get(i).getTrnmntId())) {
                                            Logger.e("TEAM READY changed for", filterLists.get(i).getTrnmntId() + " name :" + filterLists.get(i).getTrnmntName());
                                            filterLists.get(i).setNumOfTeamsReady(tournamentObj.getString("num_of_team_entered"));
                                            final int position = i;
                                            Logger.e("notifyPosition = ", position + " ");
                                            if (getActivity() != null) {
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Logger.e("Team ready notify", "notify the adapter for the change");
                                                        tournamentAdapter.notifyItemChanged(position);
                                                    }
                                                });
                                            }
                                            break;
                                        }
                                        Logger.e("nodeJs auction_item_finished pos", "Update list position : " + i);
                                    }

                                } catch (JSONException e) {
                                    Logger.e("nodeJs itemFinished listen ex", e.getMessage() + "");
                                }
                            }
                        });
                    }
                }
            });
        } else {
            Logger.e("nodeJs lbl 1", "not connected !!!");
        }
    }


}
