package com.emts.ugbattle.fragment.newhome;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.FragmentPageAdapter;

public class ChatsHomeFragment extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    FragmentPageAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);

        addTabs();
    }

    private void addTabs() {
        adapter = new FragmentPageAdapter(getChildFragmentManager());

        Fragment msgFrag = new MyMessagesFragment();
        adapter.addFrag(msgFrag, "Chats");

        Fragment friendFragment = new PlayerListingFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFriend", true);
        friendFragment.setArguments(bundle);
        adapter.addFrag(friendFragment, "Friend List");

        Fragment playerFragment = new PlayerListingFragment();
        adapter.addFrag(playerFragment, "Player List");

        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
    }
}
