package com.emts.ugbattle.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.PayPalPaymentActivity;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.model.TicketModel;

public class TicketCheckoutFragment extends android.support.v4.app.Fragment {
    TicketModel ticket;
    PreferenceHelper prefHelper;
    ParentActivityInterface openSpecificPageInterface;
    public static final int PAYMENT_ACTIVITY_CODE = 143;
    EditText etIcPassword, etContactNo, etEmail, etName;
    Button btnConfirmOrder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefHelper = PreferenceHelper.getInstance(getActivity());

        if (getArguments() != null) {
            ticket = (TicketModel) getArguments().getSerializable("ticket");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ticket_checkout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etName = view.findViewById(R.id.etName);
        etName.setText(prefHelper.getString(PreferenceHelper.APP_USER_NAME, ""));
        etEmail = view.findViewById(R.id.etEmail);
        etEmail.setText(prefHelper.getString(PreferenceHelper.APP_USER_EMAIL, ""));
        etIcPassword = view.findViewById(R.id.etIcPassword);
        etContactNo = view.findViewById(R.id.etContactNo);
        final EditText etRemarks = view.findViewById(R.id.etRemark);
        btnConfirmOrder = view.findViewById(R.id.btnConfirmOrder);


        final CheckBox cbPaypal = view.findViewById(R.id.checkbox_paypal);
        final CheckBox cbDebitCard = view.findViewById(R.id.checkbox_debit_credit_card);
        cbPaypal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cbDebitCard.setChecked(false);
                    btnConfirmOrder.setEnabled(true);
                    btnConfirmOrder.setBackgroundResource(R.drawable.rounded_blue);
                } else {
                    if (!cbDebitCard.isChecked()) {
                        btnConfirmOrder.setEnabled(false);
                        btnConfirmOrder.setBackgroundResource(R.drawable.rounded_grey);
                    }
                }
            }
        });
        cbDebitCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cbPaypal.setChecked(false);
                    btnConfirmOrder.setEnabled(true);
                    btnConfirmOrder.setBackgroundResource(R.drawable.rounded_blue);
                } else {
                    if (!cbPaypal.isChecked()) {
                        btnConfirmOrder.setEnabled(false);
                        btnConfirmOrder.setBackgroundResource(R.drawable.rounded_grey);
                    }
                }
            }
        });

        TextView tvPrice = view.findViewById(R.id.tvAmount);
        tvPrice.setText(prefHelper.getString(PreferenceHelper.DEF_CURRENCY_CODE, "USD") + " "
                + prefHelper.getString(PreferenceHelper.DEF_CURRENCY_SIGN, "$") + " " + ticket.getTicketPrice());

        btnConfirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etName.getText().toString().trim())) {
                    AlertUtils.showSnack(getActivity(), view, "Name field must not be empty");
                    return;
                }
                if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    AlertUtils.showSnack(getActivity(), view, "Email field must not be empty");
                    return;
                }
//                if (TextUtils.isEmpty(etIcPassword.getText().toString().trim())) {
//                    AlertUtils.showSnack(getActivity(), view, "Ic/Password field must not be empty");
//                    return;
//                }
                if (TextUtils.isEmpty(etContactNo.getText().toString().trim())) {
                    AlertUtils.showSnack(getActivity(), view, "Contact No. field must not be empty");
                    return;
                }

                if (NetworkUtils.isInNetwork(getActivity())) {
                    String postParams = "api_key=" + Api.getInstance().apiKey + "&user_id="
                            + prefHelper.getUserId() + "&user_token=" + prefHelper.getToken()
                            + "&package_id=" + ticket.getTicketId() + "&full_name="
                            + etName.getText().toString().trim() + "&email=" + etEmail.getText().toString().trim()
                            + "&ic_passport=" + etIcPassword.getText().toString().trim() + "&contact_no="
                            + etContactNo.getText().toString().trim() + "&remark="
                            + etRemarks.getText().toString().trim();

                    String paymentMethod = "1";
                    if (cbPaypal.isChecked()) {
                        paymentMethod = "1";
                    } else {
                        paymentMethod = "2";
                    }
                    postParams = postParams + "&payment_method=" + paymentMethod;

                    Intent intent = new Intent(getActivity(), PayPalPaymentActivity.class);
                    intent.putExtra("post_params", postParams);
                    startActivityForResult(intent, PAYMENT_ACTIVITY_CODE);
                } else {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.error_no_internet));
                }
            }
        });

        etContactNo.addTextChangedListener(textWatcher);
        etEmail.addTextChangedListener(textWatcher);
//        etIcPassword.addTextChangedListener(textWatcher);
        etName.addTextChangedListener(textWatcher);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (TextUtils.isEmpty(etName.getText().toString().trim()) ||
                    TextUtils.isEmpty(etEmail.getText().toString().trim()) ||
//                    TextUtils.isEmpty(etIcPassword.getText().toString().trim()) ||
                    TextUtils.isEmpty(etContactNo.getText().toString().trim())) {
                btnConfirmOrder.setBackground(getResources().getDrawable(R.drawable.rounded_grey));
                btnConfirmOrder.setClickable(false);
                btnConfirmOrder.setEnabled(false);

            } else {
                btnConfirmOrder.setBackground(getResources().getDrawable(R.drawable.rounded_blue));
                btnConfirmOrder.setClickable(true);
                btnConfirmOrder.setEnabled(true);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == PAYMENT_ACTIVITY_CODE) {
            if (data.getBooleanExtra("paid", false)) {
                prefHelper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE,
                        String.valueOf(Integer.parseInt(prefHelper.getString(PreferenceHelper.APP_USER_TICKET_BALANCE, "0")) + Integer.parseInt(ticket.getTicketQty()))).commit();

                if (openSpecificPageInterface != null) {
                    openSpecificPageInterface.goBack();
                }
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificPageInterface = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificPageInterface = null;
        super.onDetach();
    }
}
