package com.emts.ugbattle.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FAQsFragment extends Fragment {
    LinearLayout faqLayout;
    TextView errorText;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_faqs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        errorText = view.findViewById(R.id.errorText);
        progressBar = view.findViewById(R.id.progress);
        faqLayout = view.findViewById(R.id.faq_layout);

        if (NetworkUtils.isInNetwork(getActivity())) {
            faqTask();
        } else {
            faqLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void faqTask() {
        errorText.setVisibility(View.GONE);
        faqLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().faqUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray faqArray = res.getJSONArray("faqs");
                                if (faqArray.length() < 1) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    faqLayout.setVisibility(View.GONE);
                                    return;
                                }
                                for (int i = 0; i < faqArray.length(); i++) {
                                    JSONObject eachQuestion = faqArray.getJSONObject(i);
                                    addQsnAnsView(eachQuestion.getString("title"), eachQuestion.getString("description"));
                                }
                                faqLayout.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                faqLayout.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("faqTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("faqTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        faqLayout.setVisibility(View.GONE);
                    }
                }, "faqTask");
    }

    private void addQsnAnsView(String question, String answer) {
        View v;
        v = LayoutInflater.from(getActivity()).inflate(R.layout.inflated_faq_qsn_answer_layout, null);
        final TextView qsn = v.findViewById(R.id.faq_qsn);
        final TextView ans = v.findViewById(R.id.faq_answer);
        final View divider = v.findViewById(R.id.view2);
        qsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans.getVisibility() == View.VISIBLE) {
                    ans.setVisibility(View.GONE);
                    divider.setVisibility(View.GONE);
                    qsn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right_white_24dp, 0);
                } else {
                    ans.setVisibility(View.VISIBLE);
                    divider.setVisibility(View.VISIBLE);
                    qsn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_white_24dp, 0);
                }
            }
        });
        qsn.setText(Html.fromHtml(question));
        ans.setText(Html.fromHtml(answer));
        faqLayout.addView(v);
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("faqTask");
        super.onPause();
    }
}
