package com.emts.ugbattle.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.HistoryAdapter;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.InfiniteScrollList;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.HistoryMdl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {
    RecyclerView gameHistoryListings;
    ArrayList<HistoryMdl> historyLists = new ArrayList<>();
    HistoryAdapter historyAdapter;

    ProgressBar progressBar, infiniteProgress;
    TextView errorText;

    GameMdl gameMdl;
    int limit = 50;
    int offset = 0;

    ParentActivityInterface openSpecificFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            gameMdl = (GameMdl) bundle.getSerializable("game");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VolleyHelper.getInstance(getContext()).cancelRequest("historyListingTask");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);
        errorText = view.findViewById(R.id.error_text);
        gameHistoryListings = view.findViewById(R.id.listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        gameHistoryListings.setLayoutManager(linearLayoutManager);
        gameHistoryListings.addOnScrollListener(new InfiniteScrollList(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    historyListingTask();
                } else {
                    AlertUtils.showToast(getActivity(), getString(R.string.error_no_internet));
                }
            }
        });

        historyAdapter = new HistoryAdapter(getActivity(), historyLists);
        historyAdapter.setRecyclerItemClickListener(new HistoryAdapter.RecyclerItemClickListener() {
            @Override
            public void onItemClickLister(int position, View view) {
                openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.ONGOING_OR_HISTORY_PAGE, historyLists.get(position));
            }
        });
        gameHistoryListings.setAdapter(historyAdapter);
        gameHistoryListings.setNestedScrollingEnabled(false);
        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);

        if (NetworkUtils.isInNetwork(getActivity())) {
            offset = 0;
            historyListingTask();
        } else {
            errorText.setText(getActivity().getResources().getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            gameHistoryListings.setVisibility(View.GONE);
        }
    }

    private void historyListingTask() {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
            gameHistoryListings.setVisibility(View.GONE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("game_id", gameMdl.getGameId());
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().gameHistoryApi, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        errorText.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray historyArray = res.getJSONArray("game_history");

                                if (historyArray.length() == 0) {
                                    if (offset == 0) {
                                        errorText.setText("No game history");
                                        errorText.setVisibility(View.VISIBLE);
                                    } else {
                                        AlertUtils.showSnack(getActivity(), gameHistoryListings, "No more history");
                                    }
                                } else {
                                    if (offset == 0) {
                                        historyLists.clear();
                                    }
                                    for (int i = 0; i < historyArray.length(); i++) {
                                        JSONObject eachHistory = historyArray.getJSONObject(i);

                                        HistoryMdl history = new HistoryMdl();
                                        history.setGameId(eachHistory.getString("game_id"));
                                        history.setTrnmntId(eachHistory.getString("tourn_id"));
                                        history.setTeamId(eachHistory.getString("my_team_id"));
                                        history.setResult(eachHistory.getString("win_status"));
                                        history.setCompletedDate(eachHistory.getString("completed_date"));
                                        try {
                                            history.setCurrentRoundNo(Integer.parseInt(eachHistory.getString("rounds")));
                                        } catch (Exception e) {
                                            history.setCurrentRoundNo(1);
                                        }
                                        history.setGameName(eachHistory.getString("game_name"));

                                        historyLists.add(history);
                                    }

                                    errorText.setVisibility(View.GONE);
                                    historyAdapter.notifyDataSetChanged();
                                    gameHistoryListings.setVisibility(View.VISIBLE);
                                }
                            } else {
                                if (offset == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    gameHistoryListings.setVisibility(View.GONE);
                                } else {
                                    AlertUtils.showSnack(getActivity(), gameHistoryListings, res.getString("message"));
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("historyListingTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("historyListingTask error res", e.getMessage() + " ");
                        }
                        if (offset == 0) {
                            errorText.setText(errorMsg);
                            errorText.setVisibility(View.VISIBLE);
                            gameHistoryListings.setVisibility(View.GONE);
                        } else {
                            AlertUtils.showSnack(getActivity(), gameHistoryListings, errorMsg);
                        }
                    }
                }, "historyListingTask");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        openSpecificFragment = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        openSpecificFragment = null;
        super.onDetach();
    }
}
