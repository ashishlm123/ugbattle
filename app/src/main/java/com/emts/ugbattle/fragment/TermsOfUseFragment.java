package com.emts.ugbattle.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class TermsOfUseFragment extends Fragment {
    TextView termsConditionsContent;
    TextView errorText;
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_terms_of_use, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        errorText = view.findViewById(R.id.errorText);
        progressBar = view.findViewById(R.id.progress);
        termsConditionsContent = view.findViewById(R.id.terms_conditions_text);

        if (NetworkUtils.isInNetwork(getActivity())) {
            tncTask();
        } else {
            termsConditionsContent.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }


    private void tncTask() {
        errorText.setVisibility(View.GONE);
        termsConditionsContent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("slug", "terms-n-conditions");

        vHelper.addVolleyRequestListeners(Api.getInstance().cmsUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                termsConditionsContent.setText(Html.fromHtml(res.getJSONObject("content").getString("content")));
                                termsConditionsContent.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                termsConditionsContent.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("tncTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("tncTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        termsConditionsContent.setVisibility(View.GONE);
                    }
                }, "tncTask");
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("tncTask");
        super.onPause();
    }

}
