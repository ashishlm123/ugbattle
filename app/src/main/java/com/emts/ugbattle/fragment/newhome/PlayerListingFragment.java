package com.emts.ugbattle.fragment.newhome;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.FriendsListAdapter;
import com.emts.ugbattle.customviews.EndlessRecyclerViewScrollListener;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerListingFragment extends Fragment {
    TextView errorText;
    ProgressBar progressBar, infiniteProgress;
    RecyclerView friendsListing;
    FriendsListAdapter friendsListAdapter;
    ArrayList<UserModel> playersLists = new ArrayList<>();
    ArrayList<UserModel> userLists = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    int limit = 50;
    int offSet = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    EditText edtSearch;
    boolean isFriendListing = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isFriendListing = getArguments() != null && getArguments().getBoolean("isFriend", false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_friend_listing, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        errorText = view.findViewById(R.id.error_text);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);
        progressBar = view.findViewById(R.id.progress_bar);

        friendsListing = view.findViewById(R.id.friends_listings);
        edtSearch = view.findViewById(R.id.edt_search);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);

        final LinearLayoutManager friendsLayoutManager = new LinearLayoutManager(getActivity());
        friendsListing.setLayoutManager(friendsLayoutManager);
        friendsListAdapter = new FriendsListAdapter(getActivity(), playersLists, false);
        friendsListAdapter.setUpdatePlayerCallBack(new FriendsListAdapter.UpdatePlayer() {
            @Override
            public void updatePlayer(int position, int type) {
//                if (type == FriendsListAdapter.UpdatePlayer.INVITE) {
//                    userLists.remove(playersLists.get(position));
//                    friendsListAdapter.notifyItemChanged(position);
//                    playersLists.remove(position);
//                    friendsListAdapter.notifyDataSetChanged();
//                } else
                if (type == FriendsListAdapter.UpdatePlayer.ADD_FRIEND) {
                    UserModel userModel = playersLists.get(position);
                    int mainIndex = userLists.indexOf(userModel);
                    userModel.setUserMemberStatus(Config.IS_AWAITING);
                    friendsListAdapter.notifyItemChanged(position);
                    userLists.set(mainIndex, userModel);
                }
            }
        });
        friendsListing.setAdapter(friendsListAdapter);
        friendsListing.setNestedScrollingEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                offSet = 0;
                edtSearch.setText("");
                getUserListTask(offSet, "");
            }
        });

        infiniteScrollListener = new EndlessRecyclerViewScrollListener(friendsLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (userLists.size() >= limit) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        getUserListTask(offSet, "");
                    } else {
                        errorText.setText(getResources().getString(R.string.error_no_internet));
                        errorText.setVisibility(View.VISIBLE);
                        AlertUtils.showSnack(getActivity(), errorText, getResources().getString(R.string.error_no_internet));
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        };
        friendsListing.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            getUserListTask(offSet, "");
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.VISIBLE);
        }

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (keyEvent != null) {
                    if (!keyEvent.isShiftPressed()) {
                        if (!TextUtils.isEmpty(edtSearch.getText().toString())) {
                            offSet = 0;
                            getUserListTask(offSet, edtSearch.getText().toString());
                        } else {
                            getUserListTask(offSet, "");
                        }
                    }
                }
                return false;
            }
        });
        ImageView imgSearch = view.findViewById(R.id.searchImg);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(edtSearch.getText().toString())) {
                    offSet = 0;
                    getUserListTask(offSet, edtSearch.getText().toString());
                } else {
                    getUserListTask(offSet, "");
                }
            }
        });
    }

    private void filterFriendList() {
        playersLists.clear();
        for (int i = 0; i < userLists.size(); i++) {
            if (userLists.get(i).isFriend()) {
                playersLists.add(userLists.get(i));
            }
        }
        if (playersLists.size() == 0) {
            errorText.setText("You don't have any friends in your friend list");
            errorText.setVisibility(View.VISIBLE);
            friendsListing.setVisibility(View.GONE);
        } else {
            errorText.setVisibility(View.GONE);
            friendsListing.setVisibility(View.VISIBLE);
        }
        friendsListAdapter.notifyDataSetChanged();
    }

    private void getUserListTask(final int offsetValue, final String searchValue) {
        AlertUtils.hideInputMethod(getActivity(), errorText);
        if (offSet == 0) {
            friendsListing.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("limit", String.valueOf(limit));
        postParam.put("offset", String.valueOf(offsetValue));
        postParam.put("global_field", searchValue);
        vHelper.addVolleyRequestListeners(Api.getInstance().getAllUserList, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray frnArray = res.getJSONArray("friend_list");
                                if (frnArray.length() == 0) {
                                    if (offSet == 0) {
                                        if (isFriendListing) {
                                            errorText.setText("You don't have any friends in your friend list");
                                        } else {
                                            errorText.setText("There is no any player in this tournament");
                                        }
                                        errorText.setVisibility(View.VISIBLE);
                                    } else {
                                        offSet = userLists.size();
                                        if (isFriendListing) {
                                            filterFriendList();
                                        } else {
                                            filterPlayer();
                                        }
                                    }
                                } else {
                                    if (offSet == 0) {
                                        userLists.clear();
                                    }
                                    for (int i = 0; i < frnArray.length(); i++) {
                                        JSONObject eachObj = frnArray.getJSONObject(i);
                                        UserModel friendsModel = new UserModel();
                                        friendsModel.setUserFullName(eachObj.getString("full_name"));
                                        friendsModel.setUserEmail(eachObj.getString("email"));
                                        friendsModel.setUserId(eachObj.getString("id"));
                                        friendsModel.setUserName(eachObj.getString("username"));
                                        friendsModel.setUserContact(eachObj.getString("contact_no"));

                                        if (eachObj.getString("profile_img") == null ||
                                                eachObj.getString("profile_img").equals("null")) {
                                            friendsModel.setUserProfilePic(res.getString("default_img_path"));
                                        } else {
                                            friendsModel.setUserProfilePic(res.getString("user_img_base_url") + eachObj.getString("profile_img"));
                                        }
                                        if (eachObj.getString("is_friend").equals("0")) {
                                            friendsModel.setFriend(false);
                                            friendsModel.setUserMemberStatus(eachObj.optString("inv_resp_status"));
                                        } else if (eachObj.getString("is_friend").equals("1")) {
                                            friendsModel.setFriend(true);
                                            friendsModel.setUserMemberStatus(""); // for testing only since change in api is remaining
                                        }
                                        userLists.add(friendsModel);
                                    }
                                    offSet = userLists.size();
                                    if (isFriendListing) {
                                        filterFriendList();
                                    } else {
                                        filterPlayer();
                                    }
                                }
                            } else {
                                if (offSet == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    friendsListing.setVisibility(View.GONE);
                                } else {
                                    friendsListing.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(getActivity(), errorText, res.getString("message"));
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getUserListTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        infiniteProgress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getUserListTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getUserListTask");
    }

    private void filterPlayer() {
        playersLists.clear();
        errorText.setVisibility(View.GONE);
        for (int i = 0; i < userLists.size(); i++) {
            if (!userLists.get(i).isFriend()) {
                playersLists.add(userLists.get(i));
            }
        }
        if (playersLists.size() == 0) {
            friendsListing.setVisibility(View.GONE);
            errorText.setText("You don't have any user in your Player list");
            errorText.setVisibility(View.VISIBLE);
        }
        friendsListing.setVisibility(View.VISIBLE);
        friendsListAdapter.notifyDataSetChanged();
    }
}
