package com.emts.ugbattle.fragment.account;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.adapter.OrderListsAdapter;
import com.emts.ugbattle.customviews.EndlessRecyclerViewScrollListener;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.DateUtils;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OrderFragment extends Fragment {
    TextView ticketBalance, errorText;
    Button btnTopUp;
    RecyclerView topupHistoryListings;
    ArrayList<OrderModel> topupOrderLists = new ArrayList<>();
    OrderListsAdapter orderListsAdapter;
    ProgressBar progressBar, infiniteProgress;
    int limit = 100;
    int offset = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    ParentActivityInterface mainPageInterface;
    boolean isFromPush = false;
    MessageModel messageModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            isFromPush = bundle.getBoolean("from_notification", false);
            messageModel = (MessageModel) bundle.getSerializable("mesg");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VolleyHelper.getInstance(getContext()).cancelRequest("");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ticketBalance = view.findViewById(R.id.ticket_balance);
        ticketBalance.setText(PreferenceHelper.getInstance(getActivity()).getString(PreferenceHelper.APP_USER_TICKET_BALANCE, "0"));
        btnTopUp = view.findViewById(R.id.btn_topup);
        btnTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offset = 0;
                mainPageInterface.openSpecificPage(view.getId());
            }
        });


        topupHistoryListings = view.findViewById(R.id.topup_history_listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        topupHistoryListings.setLayoutManager(linearLayoutManager);
        orderListsAdapter = new OrderListsAdapter(getActivity(), topupOrderLists);
        topupHistoryListings.setAdapter(orderListsAdapter);
        topupHistoryListings.setNestedScrollingEnabled(false);
        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgress = view.findViewById(R.id.infinite_progress_bar);
        errorText = view.findViewById(R.id.error_text);

        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (topupOrderLists.size() >= limit) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        getOrderListTask(offset);
                    } else {
                        errorText.setText(getResources().getString(R.string.error_no_internet));
                        errorText.setVisibility(View.VISIBLE);
                        AlertUtils.showSnack(getActivity(), errorText, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        };
        topupHistoryListings.addOnScrollListener(infiniteScrollListener);
        if (NetworkUtils.isInNetwork(getActivity())) {
            getOrderListTask(offset);
        } else {
            errorText.setText(R.string.error_no_internet);
            errorText.setVisibility(View.VISIBLE);
        }

        if (isFromPush) {
            if (NetworkUtils.isInNetwork(getActivity()) && messageModel.getMessageId() != null) {
                markReadTask();
            }
        }
    }

    private void getOrderListTask(final int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgress.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("limit", String.valueOf(limit));
        postParam.put("offset", String.valueOf(offsetValue));
        vHelper.addVolleyRequestListeners(Api.getInstance().getOrderListUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                infiniteProgress.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

                        JSONArray orderArray = res.getJSONArray("topup_history");

                        if (orderArray.length() == 0) {
                            if (offsetValue == 0) {
                                errorText.setText(Html.fromHtml(res.getString("message")));
                                errorText.setVisibility(View.VISIBLE);
                            } else {
                                AlertUtils.showSnack(getActivity(), errorText, "No more order in list");
                            }
                        } else {
                            if (offset == 0) {
                                topupOrderLists.clear();
                            }

                            for (int i = 0; i < orderArray.length(); i++) {
                                JSONObject eachObj = orderArray.getJSONObject(i);
                                OrderModel orderModel = new OrderModel();
                                orderModel.setOrderDate(DateUtils.convertDate(eachObj.getString("transaction_date")));
                                orderModel.setOrderId(eachObj.getString("id"));
                                orderModel.setOrderMessage("Your topup is successful, " + eachObj.getString("num_tickets") + " tickets is added");
                                orderModel.setOrderTime(eachObj.getString("transaction_date"));
                                topupOrderLists.add(orderModel);
                            }

                            offset = topupOrderLists.size();
                            orderListsAdapter.notifyDataSetChanged();
                            topupHistoryListings.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                        }
                    } else {
                        if (offset == 0) {
                            errorText.setText(Html.fromHtml(res.getString("message")));
                            errorText.setVisibility(View.VISIBLE);
                            topupHistoryListings.setVisibility(View.GONE);
                        } else {
                            topupHistoryListings.setVisibility(View.VISIBLE);
                            AlertUtils.showSnack(getActivity(), errorText, " No more Games...");
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("moreGameListingTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                infiniteProgress.setVisibility(View.GONE);
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = String.valueOf(Html.fromHtml(errorObj.getString("message")));
                } catch (Exception e) {
                    Logger.e("getOrderListTask error res", e.getMessage() + " ");
                }

                errorText.setText(errorMsg);
                errorText.setVisibility(View.VISIBLE);
            }
        }, "getOrderListTask");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainPageInterface = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        mainPageInterface = null;
        super.onDetach();
    }

    private void markReadTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("note_id", messageModel.getMessageId());
        vHelper.addVolleyRequestListeners(Api.getInstance().readNotificationUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "markReadTask");
    }
}
