package com.emts.ugbattle.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.RecyclerItemClickListener;
import com.emts.ugbattle.adapter.TicketAdapter;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TicketModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MarketPlaceFragment extends Fragment {
    RecyclerView ticketListings;
    ArrayList<TicketModel> ticketLists = new ArrayList<>();
    TextView errorText;
    ProgressBar progressBar;

    TicketAdapter ticketAdapter;
    int limit = 10000;
    int offset = 0;
    ParentActivityInterface mainInterface;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_market_place, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        errorText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);

        ticketListings = view.findViewById(R.id.ticket_listings);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        ticketListings.setLayoutManager(linearLayoutManager);
        ticketAdapter = new TicketAdapter(getActivity(), ticketLists);
        ticketListings.setAdapter(ticketAdapter);
        ticketListings.setNestedScrollingEnabled(false);
        ticketAdapter.setOnRecyclerItemClickListener(new RecyclerItemClickListener() {
            @Override
            public void onRecyclerItemClicked(int position) {
                if (mainInterface != null) {
                    mainInterface.openSpecificPageWithData(ParentActivityInterface.MARKET_PLACE_PAGE_ID,
                            ticketLists.get(position));
                }
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            marketTask();
        } else {
            ticketListings.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void marketTask() {
        errorText.setVisibility(View.GONE);
        ticketListings.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().marketPlaceUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        ticketLists.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray ticketArray = res.getJSONArray("ticket_packages");
                                for (int i = 0; i < ticketArray.length(); i++) {
                                    JSONObject eachTicket = ticketArray.getJSONObject(i);
                                    TicketModel ticket = new TicketModel();
                                    ticket.setTicketId(eachTicket.getString("package_id"));
                                    ticket.setTicketQty(eachTicket.getString("num_of_tickets"));
                                    ticket.setTicketPrice(eachTicket.getString("price"));

                                    ticketLists.add(ticket);
                                }
                                if (ticketLists.size() > 0) {
                                    ticketAdapter.notifyDataSetChanged();
                                    ticketListings.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    ticketListings.setVisibility(View.GONE);
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                ticketListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("marketTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("marketTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                        ticketListings.setVisibility(View.GONE);
                    }
                }, "marketTask");
    }


    @Override
    public void onPause() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("marketTask");
        super.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainInterface = (ParentActivityInterface) context;
    }

    @Override
    public void onDetach() {
        mainInterface = null;
        super.onDetach();
    }
}
