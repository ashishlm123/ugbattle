package com.emts.ugbattle;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.activity.LoginActivity;
import com.emts.ugbattle.fragment.FAQsFragment;
import com.emts.ugbattle.fragment.GameDetailFragment;
import com.emts.ugbattle.fragment.GameListingFragment;
import com.emts.ugbattle.fragment.HelpCenterFragment;
import com.emts.ugbattle.fragment.HomeFrag;
import com.emts.ugbattle.fragment.InboxFrag;
import com.emts.ugbattle.fragment.LobbyFrag;
import com.emts.ugbattle.fragment.MarketPlaceFragment;
import com.emts.ugbattle.fragment.OnGoingChallengeFrag;
import com.emts.ugbattle.fragment.PrivacyPolicyFragment;
import com.emts.ugbattle.fragment.QuestionsFrag;
import com.emts.ugbattle.fragment.TeamDetailsFragment;
import com.emts.ugbattle.fragment.TermsOfUseFragment;
import com.emts.ugbattle.fragment.TicketCheckoutFragment;
import com.emts.ugbattle.fragment.TwoTeamFragment;
import com.emts.ugbattle.fragment.ViewStartGameFragment;
import com.emts.ugbattle.fragment.account.AccountFrag;
import com.emts.ugbattle.fragment.account.FriendsListFragment;
import com.emts.ugbattle.fragment.account.MyWithdrawalFragment;
import com.emts.ugbattle.fragment.account.OrderFragment;
import com.emts.ugbattle.fragment.account.ProfileFragment;
import com.emts.ugbattle.fragment.account.WithdrawalHistoryFrag;
import com.emts.ugbattle.fragment.newhome.ChatsHomeFragment;
import com.emts.ugbattle.fragment.newhome.MyMessagesFragment;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.TournamentModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ParentActivityInterface {
    LinearLayout holderMessages, holderAccount, holderHome, holderInbox, holderQuestions;
    TextView tvMessages, tvAccount, tvHome, tvInbox, tvQuestions;
    View prevMenuSelect;
    TextView prevSelectText;
    Toolbar toolbar;
    TextView toolbarTitle;
    PreferenceHelper prefHelper;
    boolean isPush = false;
    String notificationType = null;

    TextView tvBadgeMessages, tvBadgeInbox;

    FragmentManager fm;
    MessageModel messageModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);

        //version check
        if (NetworkUtils.isInNetwork(this)) {
            SiteSettingsTask.getSiteSettings(this, new SiteSettingsTask.VersionUpdateCallback() {
                @Override
                public void onVersionUpdated(boolean isVersionUpdated) {
                    if (isVersionUpdated) {
                        SiteSettingsTask.showVersionUpdateDialog(MainActivity.this);
                        SiteSettingsTask.isDialogShow = false;
                        Logger.e("onVersionUpdated isUpdate???", "callback invoked");
                    }
                }
            });
        }

        setContentView(R.layout.activity_main);

        prefHelper = PreferenceHelper.getInstance(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);

        fm = getSupportFragmentManager();

        tvMessages = findViewById(R.id.tvLobby);
        tvAccount = findViewById(R.id.tvAccount);
        tvHome = findViewById(R.id.tvHome);
        tvInbox = findViewById(R.id.tvInbox);
        tvQuestions = findViewById(R.id.tvQuestion);

        holderMessages = findViewById(R.id.holder_lobby);
        holderAccount = findViewById(R.id.holder_account);
        holderHome = findViewById(R.id.holder_home);
        holderInbox = findViewById(R.id.holder_inbox);
        holderQuestions = findViewById(R.id.holder_ques);

        tvBadgeMessages = holderMessages.findViewById(R.id.tvBadgeChat);
        tvBadgeInbox = holderInbox.findViewById(R.id.tvBadgeInbox);

        //default selection
//        bottomNavItemClick(holderHome);
        fm.beginTransaction().replace(R.id.content_frame, new HomeFrag()).commit();
        selectBottomTab(TAB_HOME);
//        setTitle(false);
        toolbar.setVisibility(View.GONE);

        Intent intent = getIntent();
        if (intent != null) {
            isPush = intent.getBooleanExtra("from_push", false);
            if (isPush) {
                notificationType = intent.getStringExtra("type");
                messageModel = (MessageModel) intent.getSerializableExtra("mesg");
            }
        }

        if (notificationType != null) {
            switch (notificationType) {
                case Config.FRIEND_REQUEST_RESP:
                    openSpecificPageWithData(ParentActivityInterface.FRIEND_LIST_FRAG_ID, messageModel);
                    break;
                case Config.PLAYER_INVITATION_REQ:
                    openSpecificPageWithData(ParentActivityInterface.GAME_DETAIL_PAGE_ID, (GameMdl) messageModel.getExtras());
                    break;
                case Config.PLAYER_INVITATION_RES:
                    openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, (TournamentModel) messageModel.getExtras());
                    break;
                case Config.WITHDRAWAL_RES:
                    openSpecificPageWithData(ParentActivityInterface.WITHDRAWAL_HISTORY_ID, messageModel);
                    break;
                case Config.TICKET_PURCHASE_RES:
                    openSpecificPageWithData(ParentActivityInterface.ORDER_PAGE_ID, messageModel);
                    break;
                case Config.MATCH_CHALLENGE_REQUEST:
                    openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, (TournamentModel) messageModel.getExtras());
                    break;
                case Config.MATCH_CHALLENGE_RESP:
                    openSpecificPageWithData(ParentActivityInterface.START_GAME_FRAGMENT_ID, (TournamentModel) messageModel.getExtras());
                    break;

            }
        }

        getBadgeCountTask();
    }

    private void getBadgeCountTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getBadgeCountApi, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                try {
                                    setBadge(true, resObj.getInt("no_of_message"));
                                } catch (Exception e) {
                                }
                                try {
                                    setBadge(false, resObj.getInt("no_of_note"));
                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getBadgeCOuntTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "getBadgeCountTask");
    }

    public void bottomNavItemClick(View view) {
        Fragment fragment = null;
        Intent intent = null;

        switch (view.getId()) {
            case R.id.holder_lobby:
//                fragment = new LobbyFrag();
//                fragment = new MyMessagesFragment();
                fragment = new ChatsHomeFragment();
                selectBottomTab(TAB_MESSAGES);
                break;

            case R.id.holder_account:
                fragment = new AccountFrag();
                selectBottomTab(TAB_ACCOUNT);
                break;

            case R.id.holder_home:
                fragment = new HomeFrag();
                selectBottomTab(TAB_HOME);
                break;

            case R.id.holder_inbox:
                fragment = new InboxFrag();
                selectBottomTab(TAB_INBOX);
                break;

            case R.id.holder_ques:
                fragment = new QuestionsFrag();
                selectBottomTab(TAB_QUESTION);
                break;
        }

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            setTitle(false);
        }
    }

    @Override
    public void openSpecificPage(int id) {
        Fragment fragment = null;
        Intent intent = null;
        Bundle bundle = new Bundle();

        switch (id) {
            case R.id.buy_ticket:
            case R.id.btn_topup:
                fragment = new MarketPlaceFragment();
                break;

            case R.id.btn_help_center:
                fragment = new HelpCenterFragment();
                break;

            case R.id.btn_faqs:
                fragment = new FAQsFragment();
                break;

            case R.id.btn_privacy:
                fragment = new PrivacyPolicyFragment();
                break;

            case R.id.btn_terms_of_use:
                fragment = new TermsOfUseFragment();
                break;

            //for account sections
            case R.id.btn_order:
                fragment = new OrderFragment();
                break;

            case R.id.btn_my_withdrawl:
                fragment = new MyWithdrawalFragment();
                break;

            case R.id.btn_withdrawl_his:
                fragment = new WithdrawalHistoryFrag();
                break;

//            case R.id.btn_message:
//                fragment = new MyMessagesFragment();
//                break;

            case R.id.btn_profile:
                fragment = new ProfileFragment();
                break;

            case R.id.btn_friendLists:
            case R.id.fabFriendList:
                fragment = new FriendsListFragment();
                break;

            case R.id.tvMoreGame:
                fragment = new GameListingFragment();
                break;

            case R.id.btn_logout:
                prefHelper.edit().clear().commit();
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
        }
        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();

            setTitle(false);
        }
    }

    @Override
    public void openSpecificPageWithData(int id, Object obj) {
        Fragment fragment = null;
        Intent intent = null;
        Bundle bundle = new Bundle();
        toolbar.setVisibility(View.VISIBLE);

        switch (id) {
            case R.id.btn_invite_player:
                fragment = new FriendsListFragment();
                bundle.putBoolean("isInviteToTeam", true);
                bundle.putSerializable("team", (TeamModel) obj);
                fragment.setArguments(bundle);
                break;

            case ParentActivityInterface.GAME_DETAIL_PAGE_ID:
                fragment = new GameDetailFragment();
                bundle.putSerializable("game", (Serializable) obj);
                if (isPush) {
                    if (notificationType.equals(Config.PLAYER_INVITATION_REQ)) {
                        bundle.putBoolean("from_push", true);
                    } else {
                        bundle.putBoolean("from_push", false);
                    }
                    bundle.putString("noti_id", messageModel.getMessageId());
                    isPush = false;
                }
                break;
            case ParentActivityInterface.GAME_DETAIL_INVITATION_PAGE_ID:
                fragment = new GameDetailFragment();
                bundle.putSerializable("game", (Serializable) obj);
                bundle.putBoolean("from_inbox", true);
                break;

            case ParentActivityInterface.START_GAME_FRAGMENT_ID:
                fragment = new ViewStartGameFragment();
                bundle.putSerializable("game", (Serializable) obj);
                if (isPush) {
                    bundle.putBoolean("from_push", true);
                    bundle.putString("noti_id", messageModel.getMessageId());
                    isPush = false;
                }
                break;

            case ParentActivityInterface.ROUND_START_FRAG_ID:
                Fragment frag = fm.findFragmentById(R.id.content_frame);
                if (frag instanceof ViewStartGameFragment) {
                    ((ViewStartGameFragment) frag).startGame();
                }
                break;

            case ParentActivityInterface.ONGOING_CHALLENGE_PAGE:
                Fragment frag1 = fm.findFragmentById(R.id.content_frame);
                if (frag1 instanceof ViewStartGameFragment) {
                    ((ViewStartGameFragment) frag1).startChallenge((GameMdl) obj);
                } else if (frag1 instanceof LobbyFrag) {
                    fragment = new ViewStartGameFragment();
                    bundle.putBoolean("fromlobby", true);
                    GameMdl gameMdl = (GameMdl) obj;
                    TournamentModel tMdl = new TournamentModel();
                    tMdl.setGameId(gameMdl.getGameId());
                    tMdl.setGameName(gameMdl.getGameName());
                    tMdl.setCurrentRoundNo(gameMdl.getTeamLists().get(0).getCurrentRoundNo());
                    tMdl.setTrnmntId(gameMdl.getTeamLists().get(0).getTrnmntId());
                    tMdl.setTeamId(gameMdl.getTeamLists().get(0).getMyTeamId());
                    tMdl.setTotalRounds(gameMdl.getTeamLists().get(0).getTotalRounds());
                    bundle.putSerializable("game", tMdl);
                }
                break;

            case ParentActivityInterface.ONGOING_OR_HISTORY_PAGE:
                fragment = new OnGoingChallengeFrag();
                bundle.putSerializable("game", (Serializable) obj);
                bundle.putBoolean("isHistory", true);
                fragment.setArguments(bundle);
                break;
            case ParentActivityInterface.MARKET_PLACE_PAGE_ID:
                fragment = new TicketCheckoutFragment();
                bundle.putSerializable("ticket", (Serializable) obj);
                break;

            case ParentActivityInterface.WITHDRAWAL_HISTORY_ID:
                fragment = new WithdrawalHistoryFrag();
                bundle.putBoolean("from_notification", true);
                bundle.putSerializable("mesg", (MessageModel) obj);
                break;

            case ParentActivityInterface.ORDER_PAGE_ID:
                fragment = new OrderFragment();
                bundle.putBoolean("from_notification", true);
                bundle.putSerializable("mesg", (MessageModel) obj);
                break;

            case ParentActivityInterface.FRIEND_LIST_FRAG_ID:
                fragment = new FriendsListFragment();
                if (isPush) {
                    bundle.putBoolean("from_push", true);
                    bundle.putString("noti_id", messageModel.getMessageId());
                    isPush = false;
                }
                break;

            case ParentActivityInterface.TEAM_LISTS_FRAGMENTS_PAGE:
                fragment = new TeamDetailsFragment();
                bundle.putString("team_id", String.valueOf(obj));
                break;
        }
        if (fragment != null)

        {
            fragment.setArguments(bundle);
            fm.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();

            setTitle(false);
        }
    }

    @Override
    public void goBack() {
        onBackPressed();
    }

    @Override
    public void setTitle(String title) {
        toolbarTitle.setText(title);
    }

    @Override
    public void setBadge(boolean isMessage, int count) {
        if (isMessage) {
            if (count > 0) {
                tvBadgeMessages.setText(String.valueOf(count));
                tvBadgeMessages.setVisibility(View.VISIBLE);
            } else {
                tvBadgeMessages.setVisibility(View.GONE);
            }
        } else {
            if (count > 0) {
                tvBadgeInbox.setText(String.valueOf(count));
                tvBadgeInbox.setVisibility(View.VISIBLE);
            } else {
                tvBadgeInbox.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fm.getBackStackEntryCount() >= 0) {
            setTitle(true);
        }
        VolleyHelper.getInstance(this).cancelRequest("siteSettingTask");
    }

    private void setTitle(final boolean isBackPressed) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof HomeFrag) {
                    toolbar.setVisibility(View.GONE);
                    toolbarTitle.setText("Home");
                    if (isBackPressed) selectBottomTab(TAB_HOME);
                } else {
                    toolbar.setVisibility(View.VISIBLE);
                    if (fragment instanceof ChatsHomeFragment) {
                        toolbarTitle.setText("Chats");
                        if (isBackPressed) selectBottomTab(TAB_MESSAGES);
                    } else if (fragment instanceof MyMessagesFragment) {
                        toolbarTitle.setText("Messages");
                        if (isBackPressed) selectBottomTab(TAB_MESSAGES);
                    } else if (fragment instanceof AccountFrag) {
                        toolbarTitle.setText("Account");
                        if (isBackPressed) selectBottomTab(TAB_ACCOUNT);
                    } else if (fragment instanceof InboxFrag) {
                        toolbarTitle.setText("Inbox");
                        if (isBackPressed) selectBottomTab(TAB_INBOX);
                    } else if (fragment instanceof OnGoingChallengeFrag) {
                        toolbarTitle.setText("Game Result");
                    } else if (fragment instanceof QuestionsFrag) {
                        toolbarTitle.setText("Questions");
                        if (isBackPressed) selectBottomTab(TAB_QUESTION);
                    } else if (fragment instanceof MarketPlaceFragment) {
                        toolbarTitle.setText("Market Place");
                        if (isBackPressed) selectBottomTab(TAB_HOME);
                    } else if (fragment instanceof HelpCenterFragment) {
                        toolbarTitle.setText("Help Center");
                        if (isBackPressed) selectBottomTab(TAB_QUESTION);
                    } else if (fragment instanceof FAQsFragment) {
                        toolbarTitle.setText("FAQ");
                        if (isBackPressed) selectBottomTab(TAB_QUESTION);
                    } else if (fragment instanceof PrivacyPolicyFragment) {
                        toolbarTitle.setText("Privacy Policy");
                        if (isBackPressed) selectBottomTab(TAB_QUESTION);
                    } else if (fragment instanceof TermsOfUseFragment) {
                        toolbarTitle.setText("Terms & Conditions");
                        if (isBackPressed) selectBottomTab(TAB_QUESTION);
                    } else if (fragment instanceof OrderFragment) {
                        toolbarTitle.setText("Order");
                        if (isBackPressed) selectBottomTab(TAB_ACCOUNT);
                    } else if (fragment instanceof MyWithdrawalFragment) {
                        toolbarTitle.setText("My Withdrawal");
                        if (isBackPressed) selectBottomTab(TAB_ACCOUNT);
                    } else if (fragment instanceof WithdrawalHistoryFrag) {
                        toolbarTitle.setText("Withdrawal History");
                        if (isBackPressed) selectBottomTab(TAB_ACCOUNT);
                    } else if (fragment instanceof ProfileFragment) {
                        toolbarTitle.setText("Profile");
                        if (isBackPressed) selectBottomTab(TAB_ACCOUNT);
                    } else if (fragment instanceof FriendsListFragment) {
                        Bundle bundle = fragment.getArguments();
                        if (bundle != null && bundle.getBoolean("isInviteToTeam")) {
                            toolbarTitle.setText("Invite team mates");
                        } else {
                            toolbarTitle.setText("Friends Lists");
                        }
                        if (isBackPressed) selectBottomTab(TAB_ACCOUNT);
                    } else if (fragment instanceof GameDetailFragment) {
                        try {
                            String title = ((GameMdl) fragment.getArguments().getSerializable("game")).getGameName();
                            toolbarTitle.setText(title);
                        } catch (Exception e) {
                            Logger.e(" toolbartitle ex", e.getMessage() + " ");
                        }
                        if (isBackPressed) selectBottomTab(TAB_HOME);
                    } else if (fragment instanceof ViewStartGameFragment) {
                        toolbarTitle.setText("View Start Game");
                        if (isBackPressed) selectBottomTab(TAB_HOME);
                    } else if (fragment instanceof GameListingFragment) {
                        toolbarTitle.setText("Game Center");
                        if (isBackPressed) selectBottomTab(TAB_HOME);
                    }
                }

            }
        }, 200);
    }

    private static final int TAB_MESSAGES = 0;
    private static final int TAB_ACCOUNT = 1;
    private static final int TAB_HOME = 2;
    private static final int TAB_INBOX = 3;
    private static final int TAB_QUESTION = 4;

    private void selectBottomTab(int tabId) {
        switch (tabId) {
            case TAB_MESSAGES:
                if (prevSelectText != null)
                    prevSelectText.setTextColor(Color.WHITE);
                if (prevMenuSelect != null)
                    prevMenuSelect.setBackgroundResource(R.color.loginBg);
                tvMessages.setTextColor(Color.BLACK);
                holderMessages.setBackgroundResource(R.color.bottomMenuSelect);
                prevSelectText = tvMessages;
                prevMenuSelect = holderMessages;
                break;

            case TAB_ACCOUNT:
                if (prevSelectText != null)
                    prevSelectText.setTextColor(Color.WHITE);
                if (prevMenuSelect != null)
                    prevMenuSelect.setBackgroundResource(R.color.loginBg);
                tvAccount.setTextColor(Color.BLACK);
                holderAccount.setBackgroundResource(R.color.bottomMenuSelect);
                prevSelectText = tvAccount;
                prevMenuSelect = holderAccount;
                break;

            case TAB_HOME:
                if (prevSelectText != null)
                    prevSelectText.setTextColor(Color.WHITE);
                if (prevMenuSelect != null)
                    prevMenuSelect.setBackgroundResource(R.color.loginBg);
                holderHome.setBackgroundResource(R.color.bottomMenuSelect);
                tvHome.setTextColor(Color.BLACK);
                prevSelectText = tvHome;
                prevMenuSelect = holderHome;
                break;

            case TAB_INBOX:
                if (prevSelectText != null)
                    prevSelectText.setTextColor(Color.WHITE);
                if (prevMenuSelect != null)
                    prevMenuSelect.setBackgroundResource(R.color.loginBg);
                holderInbox.setBackgroundResource(R.color.bottomMenuSelect);
                tvInbox.setTextColor(Color.BLACK);
                prevSelectText = tvInbox;
                prevMenuSelect = holderInbox;
                break;

            case TAB_QUESTION:
                if (prevSelectText != null)
                    prevSelectText.setTextColor(Color.WHITE);
                if (prevMenuSelect != null)
                    prevMenuSelect.setBackgroundResource(R.color.loginBg);
                holderQuestions.setBackgroundResource(R.color.bottomMenuSelect);
                tvQuestions.setTextColor(Color.BLACK);
                prevSelectText = tvQuestions;
                prevMenuSelect = holderQuestions;
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Fragment fragment = fm.findFragmentById(R.id.content_frame);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case TwoTeamFragment.REQUEST_PICK_PHOTO:
                // If request is cancelled, the result arrays are empty.

                if (permissions[1].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Fragment fragment = fm.findFragmentById(R.id.content_frame);
                    if (fragment instanceof ViewStartGameFragment) {
                        ((ViewStartGameFragment) fragment).permissionGrant();
                    } else if (fragment instanceof ProfileFragment) {
                        ((ProfileFragment) fragment).chooseFrmGallery(false);
                    }
                } else {
                    AlertUtils.showToast(this, "Permission Denied!!!");
                }
                break;

            case ProfileFragment.CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.

                if (permissions[1].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Fragment fragment = fm.findFragmentById(R.id.content_frame);
                    if (fragment instanceof ProfileFragment) {
                        ((ProfileFragment) fragment).takePhoto();
                    }
                } else {
                    AlertUtils.showToast(this, "Permission Denied!!!");
                }
                break;

        }
    }

    public static final String BALANCE_UPDATE_ACTION = "com.emts.ugbattle.USER_BALANCE_UPDATED";

    //balance update receiver
    class BalanceUpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Logger.e("onReceive", "balance update receiver called");
            if (fm != null) {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof HomeFrag) {
                    ((HomeFrag) fragment).updateTicketBalance();
                }
            }
        }

    }

    BalanceUpdateReceiver balanceUpdateReceiver = new BalanceUpdateReceiver();

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(balanceUpdateReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(balanceUpdateReceiver, new IntentFilter(BALANCE_UPDATE_ACTION));
    }


    @Override
    protected void onPause() {
        super.onPause();

        VolleyHelper.getInstance(this).cancelRequest("siteSettingTask");
    }
}
