package com.emts.ugbattle.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.emts.ugbattle.MainActivity;
import com.emts.ugbattle.R;
import com.emts.ugbattle.UserTicketBalanceUpdate;
import com.emts.ugbattle.activity.AddFriendDetailActivity;
import com.emts.ugbattle.activity.ChatActivity;
import com.emts.ugbattle.activity.SplashActivity;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.TournamentModel;
import com.emts.ugbattle.model.UserModel;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Map;

/**
 * Created by User on 2016-09-30.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]


        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Logger.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logger.e(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logger.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
//     [END receive_message]

    private void sendNotification(Map<String, String> data) {

        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(this);

        if (!preferenceHelper.isLogin()) {
            return;
        }
        MessageModel messageModel = null;

        try {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);

            String title = data.get("subject");

            String body = StringEscapeUtils.unescapeJava(data.get("message"));

            String notify_type = data.get("type");
//            int userId = 0;

            String messageFrom = data.get("to_user_type");
            String sendby = data.get("user_id");
//            String fileDir = data.get("dir");

            String jsonData = data.get("description");
            JSONObject eachObj = new JSONObject(jsonData);

            Logger.e("FRM : DATA", "FRM : " + jsonData + "\nDATA : " + jsonData);

            //check if login to app else open splash

            messageModel = new MessageModel();
            messageModel.setMessageId(data.get("note_id"));
            messageModel.setMessageType(data.get("type"));
            messageModel.setMessageTime(data.get("create_date"));

            switch (notify_type) {
                case Config.FRIEND_REQUEST:
                    messageModel.setUserName(eachObj.getString("requested_user"));
                    messageModel.setFriend_id(eachObj.getString("requested_user_id"));
                    messageModel.setRespType(eachObj.getString("inv_status"));
                    if (eachObj.getString("requested_user_img").equals("null")) {
                        messageModel.setUserProfilePic(eachObj.getString("default_profile_img_url"));
                    } else {
                        messageModel.setUserProfilePic(eachObj.getString("profile_img_url") +
                                eachObj.getString("requested_user_img"));
                    }
                    intent = new Intent(getApplicationContext(), AddFriendDetailActivity.class);
                    break;

                case Config.FRIEND_REQUEST_RESP:
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;


                case Config.WITHDRAWAL_RES:
                    PreferenceHelper.getInstance(getApplicationContext()).edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, eachObj.getString("remaining_ticket_balance")).apply();
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.PLAYER_INVITATION_REQ:
                    GameMdl gameMdl = new GameMdl();
                    gameMdl.setGameId(eachObj.getString("game_id"));
                    gameMdl.setGameServersArray(eachObj.getString("server_name"));
                    gameMdl.setGameUserServer(eachObj.getString("receiving_user_server_name"));
                    gameMdl.setGameUserId(eachObj.getString("receiving_user_player_id"));
                    gameMdl.setGameName(eachObj.getString("game_name"));
                    messageModel.setExtras(gameMdl);

                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.PLAYER_INVITATION_RES:
                    TournamentModel tournMdl = new TournamentModel();
                    tournMdl.setGameId(eachObj.getString("game_id"));
                    tournMdl.setTeamId(eachObj.getString("team_id"));
                    tournMdl.setCurrentRoundNo(Integer.parseInt(eachObj.getString("current_round_no")));
                    tournMdl.setTrnmntId(eachObj.getString("tourn_id"));
                    tournMdl.setGameName(eachObj.getString("game_name"));
                    tournMdl.setGameServersArray(eachObj.getString("server_name"));
                    tournMdl.setTrnmntName(eachObj.getString("tourn_name"));
                    messageModel.setExtras(tournMdl);

                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.TICKET_PURCHASE_RES:
                    PreferenceHelper.getInstance(getApplicationContext()).edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, eachObj.getString("new_ticket_balance")).apply();
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.MATCH_CHALLENGE_REQUEST:
                    TournamentModel tournamentModel = new TournamentModel();
                    tournamentModel.setGameId(eachObj.getString("game_id"));
                    tournamentModel.setTeamId(eachObj.getString("team_id"));
                    tournamentModel.setCurrentRoundNo(Integer.parseInt(eachObj.getString("current_round_no")));
                    tournamentModel.setTrnmntId(eachObj.getString("tourn_id"));
                    tournamentModel.setGameName(eachObj.getString("game_name"));
                    messageModel.setExtras(tournamentModel);
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.MATCH_CHALLENGE_RESP:
                    TournamentModel tournamentModel1 = new TournamentModel();
                    tournamentModel1.setGameId(eachObj.getString("game_id"));
                    tournamentModel1.setTeamId(eachObj.getString("team_id"));
                    tournamentModel1.setCurrentRoundNo(Integer.parseInt(eachObj.getString("current_round_no")));
                    tournamentModel1.setTrnmntId(eachObj.getString("tourn_id"));
                    tournamentModel1.setGameName(eachObj.getString("game_name"));
                    messageModel.setExtras(tournamentModel1);
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    break;

                case Config.MESSAGE_NOTI:
                    JSONObject response = new JSONObject();
                    try {
                        JSONObject match_info = new JSONObject();
                        match_info.put("game_id", eachObj.getString("game_id"));
                        match_info.put("tourn_id", eachObj.getString("tourn_id"));
                        match_info.put("match_id", eachObj.getString("match_id"));

                        JSONObject my_team = new JSONObject();
                        my_team.put("team_name", eachObj.getString("team_name"));
                        my_team.put("current_round_no", eachObj.getString("current_round_no"));
                        my_team.put("team_id", eachObj.getString("team_id"));
                        match_info.put("my_team", my_team);

                        JSONObject opponent_team = new JSONObject();
                        opponent_team.put("team_id", eachObj.getString("opponent_team_id"));
                        opponent_team.put("team_name", eachObj.getString("opponent_team_name"));
                        match_info.put("opponent_team", opponent_team);

                        response.put("match_info", match_info);
                    } catch (JSONException e) {
                        Logger.e("Json exception", e.getMessage());
                    }

                    String chattingMatchId = preferenceHelper.getString(ChatActivity.chattingMatchId, "null");

                    if (chattingMatchId.equalsIgnoreCase(eachObj.getString("match_id")) &&
                            !eachObj.getString("match_id").equals("0")) {
                        Logger.e("here", "condition entered.");
                        return;
                    }
                    if (eachObj.getString("game_image").equals("null")) {
                        messageModel.setGameImg(eachObj.getString("default_profile_img_url"));
                    } else {
                        messageModel.setGameImg(eachObj.getString("game_image"));
                    }
                    intent = new Intent(getApplicationContext(), ChatActivity.class);
                    intent.putExtra("response", response.toString());
                    intent.putExtra("fromPush", true);
                    intent.putExtra("noti_id", data.get("note_id"));
                    break;

                case Config.MESSAGE_PLAYER_NOTI:
                    String friendId = preferenceHelper.getString(ChatActivity.chattingMatchId, "null");
                    if (friendId.equalsIgnoreCase(eachObj.getString("friend_user_id")) &&
                            !eachObj.getString("friend_user_id").equals("0")) {
                        Logger.e("here", "condition entered.");
                        return;
                    }

                    if (eachObj.getString("sender_profile").equals("null")) {
                        messageModel.setGameImg(eachObj.getString("default_profile_img_url"));
                    } else {
                        messageModel.setGameImg(eachObj.getString("sender_profile"));
                    }
                    UserModel friendMdl = new UserModel();
                    friendMdl.setUserId(eachObj.getString("friend_user_id"));
                    intent = new Intent(getApplicationContext(), ChatActivity.class);
                    intent.putExtra("friend", friendMdl);
                    intent.putExtra("fromPush", true);
                    intent.putExtra("noti_id", data.get("note_id"));
                    break;

                case Config.WIN_NOTIFICATION:
//                    UserTicketBalanceUpdate.getTicketBalanceTask(MyFirebaseMessagingService.this);
                    preferenceHelper.edit().putString(PreferenceHelper.APP_USER_TICKET_BALANCE, eachObj.getString("balance")).apply();
                    Logger.e("sending Broadcast ***************", "\n BoroadCast Sent !!!!!!!!!!!!!!");
                    sendBroadcast(new Intent(MainActivity.BALANCE_UPDATE_ACTION));
                    return;

                default:
                    return;
            }

            intent.putExtra("mesg", messageModel);
            intent.putExtra("from_push", true);
            intent.putExtra("type", notify_type);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setSmallIcon(R.drawable.logo);
            }

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


//            notificationManager.notify(notiTag, notificationId, notificationBuilder.build());

            int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
//            notificationManager.notify(notify_type, m /* ID of notification */, notificationBuilder.build());
            notificationManager.notify(messageFrom, m /* ID of notification */, notificationBuilder.build());

        } catch (Exception e) {
            Logger.e(TAG + "data2 ex", e.getMessage() + " ");
        }
    }
}