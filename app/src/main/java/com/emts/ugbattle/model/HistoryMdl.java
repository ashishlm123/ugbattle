package com.emts.ugbattle.model;

public class HistoryMdl extends TournamentModel {
    public static final String STATUS_WIN = "won";
    public static final String STATUS_LOSS = "loss";
    private String result;
    private String completedDate;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }
}
