package com.emts.ugbattle.model;

import java.io.Serializable;

/**
 * Created by User on 2018-03-19.
 */

public class UserModel extends GameMdl implements Serializable {
    private String userId;
    private String userName;
    private String userFullName;
    private String userContact;
    private String userEmail;
    private String userProfilePic;
    private String userMemberStatus;
    private boolean isInvited;
    private boolean isFriend;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserProfilePic() {
        return userProfilePic;
    }

    public void setUserProfilePic(String userProfilePic) {
        this.userProfilePic = userProfilePic;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }


    public String getUserContact() {
        return userContact;
    }

    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    public boolean isFriend() {
        return isFriend;
    }

    public void setFriend(boolean friend) {
        isFriend = friend;
    }


    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserMemberStatus() {
        return userMemberStatus;
    }

    public void setUserMemberStatus(String userMemberStatus) {
        this.userMemberStatus = userMemberStatus;
    }
}
