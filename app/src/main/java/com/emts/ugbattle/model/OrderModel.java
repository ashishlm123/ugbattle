package com.emts.ugbattle.model;

import java.io.Serializable;

/**
 * Created by Prabin on 3/20/2018.
 */

public class OrderModel implements Serializable {
    private String orderId;
    private String orderMessage;
    private String orderDate;
    private String orderTime;
    private String orderAmount;
    private String status;
    private String paymentMode;
    private String oneTicketCost;

    public String getOrderMessage() {
        return orderMessage;
    }

    public void setOrderMessage(String orderMessage) {
        this.orderMessage = orderMessage;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOneTicketCost() {
        return oneTicketCost;
    }

    public void setOneTicketCost(String oneTicketCost) {
        this.oneTicketCost = oneTicketCost;
    }
}
