package com.emts.ugbattle.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 2018-03-19.
 */

public class GameMdl implements Serializable {
    private String gameId;
    private String gameName;
    private String noOfTournament;
    private String gameImg;
    private String gameServersArray;
    private String gameUserId;
    private String gameUserServer;
    private String gameDescription;
    private ArrayList<TeamModel> teamLists;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getNoOfTournament() {
        return noOfTournament;
    }

    public void setNoOfTournament(String noOfTournament) {
        this.noOfTournament = noOfTournament;
    }

    public String getGameImg() {
        return gameImg;
    }

    public void setGameImg(String gameImg) {
        this.gameImg = gameImg;
    }

    public String getGameServersArray() {
        return gameServersArray;
    }

    public void setGameServersArray(String gameServersArray) {
        this.gameServersArray = gameServersArray;
    }

    public String getGameUserId() {
        return gameUserId;
    }

    public void setGameUserId(String gameUserId) {
        this.gameUserId = gameUserId;
    }

    public String getGameDescription() {
        return gameDescription;
    }

    public void setGameDescription(String gameDescription) {
        this.gameDescription = gameDescription;
    }


    public ArrayList<TeamModel> getTeamLists() {
        return teamLists;
    }

    public void setTeamLists(ArrayList<TeamModel> teamLists) {
        this.teamLists = teamLists;
    }

    @Override
    public String toString() {
        return this.gameName;
    }

    public String getGameUserServer() {
        return gameUserServer;
    }

    public void setGameUserServer(String gameUserServer) {
        this.gameUserServer = gameUserServer;
    }
}
