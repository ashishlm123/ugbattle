package com.emts.ugbattle.model;

import java.io.Serializable;

public class TeamModel extends TournamentModel implements Serializable {
    private String myTeamId;
    private String myGameUserId;
    private String myTeamName;
    private String myTeamLeaderId;
    private String opponentTeamId;
    private String opponentTeamName;
    private String teamStatus;
    private String currentRound;
    private String opponentTeamLeaderId;
    private String opponentTeamLeaderUserId;
    private String challengingTeamId;
    private int invitationCount;

    public String getOpponentTeamId() {
        return opponentTeamId;
    }

    public void setOpponentTeamId(String opponentTeamId) {
        this.opponentTeamId = opponentTeamId;
    }

    public String getOpponentTeamName() {
        return opponentTeamName;
    }

    public void setOpponentTeamName(String opponentTeamName) {
        this.opponentTeamName = opponentTeamName;
    }

    public String getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(String currentRound) {
        this.currentRound = currentRound;
    }

    public String getOpponentTeamLeaderId() {
        return opponentTeamLeaderId;
    }

    public void setOpponentTeamLeaderId(String opponentTeamLeaderId) {
        this.opponentTeamLeaderId = opponentTeamLeaderId;
    }

    public String getOpponentTeamLeaderUserId() {
        return opponentTeamLeaderUserId;
    }

    public void setOpponentTeamLeaderUserId(String opponentTeamLeaderUserId) {
        this.opponentTeamLeaderUserId = opponentTeamLeaderUserId;
    }

    public String getTeamStatus() {
        return teamStatus;
    }

    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus;
    }

    public String getMyTeamId() {
        return myTeamId;
    }

    public void setMyTeamId(String myTeamId) {
        this.myTeamId = myTeamId;
    }

    public String getMyGameUserId() {
        return myGameUserId;
    }

    public void setMyGameUserId(String myGameUserId) {
        this.myGameUserId = myGameUserId;
    }

    public String getMyTeamName() {
        return myTeamName;
    }

    public void setMyTeamName(String myTeamName) {
        this.myTeamName = myTeamName;
    }

    public String getMyTeamLeaderId() {
        return myTeamLeaderId;
    }

    public void setMyTeamLeaderId(String myTeamLeaderId) {
        this.myTeamLeaderId = myTeamLeaderId;
    }

    public int getInvitationCount() {
        return invitationCount;
    }

    public void setInvitationCount(int invitationCount) {
        this.invitationCount = invitationCount;
    }

    public String getChallengingTeamId() {
        return challengingTeamId;
    }

    public void setChallengingTeamId(String challengingTeamId) {
        this.challengingTeamId = challengingTeamId;
    }
}
