package com.emts.ugbattle.model;

import java.io.Serializable;

/**
 * Created by Prabin on 3/23/2018.
 */

public class TournamentModel extends GameMdl implements Serializable {
    private String trnmntId;
    private String trnmntName;
    private String requiredTeams;
    private String numOfTeamsEntered;
    private String numOfTeamsReady;
    private String prizePerPerson;
    private String tcktsPerPerson;
    private int totalRounds;
    private String teamId;
    private int currentRoundNo;

    public String getTrnmntId() {
        return trnmntId;
    }

    public void setTrnmntId(String trnmntId) {
        this.trnmntId = trnmntId;
    }

    public String getTrnmntName() {
        return trnmntName;
    }

    public void setTrnmntName(String trnmntName) {
        this.trnmntName = trnmntName;
    }

    public String getRequiredTeams() {
        return requiredTeams;
    }

    public void setRequiredTeams(String requiredTeams) {
        this.requiredTeams = requiredTeams;
    }

    public String getNumOfTeamsEntered() {
        return numOfTeamsEntered;
    }

    public void setNumOfTeamsEntered(String numOfTeamsEntered) {
        this.numOfTeamsEntered = numOfTeamsEntered;
    }

    public String getPrizePerPerson() {
        return prizePerPerson;
    }

    public void setPrizePerPerson(String prizePerPerson) {
        this.prizePerPerson = prizePerPerson;
    }

    public String getTcktsPerPerson() {
        return tcktsPerPerson;
    }

    public void setTcktsPerPerson(String tcktsPerPerson) {
        this.tcktsPerPerson = tcktsPerPerson;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public int getCurrentRoundNo() {
        return currentRoundNo;
    }

    public void setCurrentRoundNo(int currentRoundNo) {
        this.currentRoundNo = currentRoundNo;
    }

    public int getTotalRounds() {
        return totalRounds;
    }

    public void setTotalRounds(int totalRounds) {
        this.totalRounds = totalRounds;
    }

    public String getNumOfTeamsReady() {
        return numOfTeamsReady;
    }

    public void setNumOfTeamsReady(String numOfTeamsReady) {
        this.numOfTeamsReady = numOfTeamsReady;
    }
}
