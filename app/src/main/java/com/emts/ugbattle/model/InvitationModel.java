package com.emts.ugbattle.model;

import java.io.Serializable;

/**
 * Created by Prabin on 3/23/2018.
 */

public class InvitationModel implements Serializable {
    public static final String INVITE_RECEIVED = "received";
    public static final String INVITE_ACCEPTED = "accepted";
    private String invitationId;
    private UserModel inviteBy;
    private TeamModel inviteToTeam;
    private String invitationStatus;

    public String getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(String invitationId) {
        this.invitationId = invitationId;
    }

    public UserModel getInviteBy() {
        return inviteBy;
    }

    public void setInviteBy(UserModel inviteBy) {
        this.inviteBy = inviteBy;
    }

    public TeamModel getInviteToTeam() {
        return inviteToTeam;
    }

    public void setInviteToTeam(TeamModel inviteToTeam) {
        this.inviteToTeam = inviteToTeam;
    }

    public String getInvitationStatus() {
        return invitationStatus;
    }

    public void setInvitationStatus(String invitationStatus) {
        this.invitationStatus = invitationStatus;
    }
}
