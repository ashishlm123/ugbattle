package com.emts.ugbattle.model;

import java.io.Serializable;

/**
 * Created by Prabin on 3/20/2018.
 */

public class ChatModel extends UserModel implements Serializable {
    public static final String MESSAGE_TYPE_TEXT = "text";
    public static final String MESSAGE_TYPE_ATTACHMENT = "file";
    public static final String GROUP_MSG = "player_msg";
    public static final String FRIEND_MSG = "friend_msg";

    public static final int MESSAGE_SENDING = 1;
    public static final int MESSAGE_FAILED = 2;
    public static final int MESSAGE_SENT = 3;

    private String messageId = "0";
    private String messagePreview;
    private String messageTime;
    private String messageType;
    private int msgStatus;

    //    //if senderTeamId == "ROW_FORMAT_DATE" then its date type
    public static final String rowTypeDate = "ROW_FORMAT_DATE";
    private String senderTeamId;
    private String myTeamId;

    public int getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(int msgStatus) {
        this.msgStatus = msgStatus;
    }

    public String getMessagePreview() {
        return messagePreview;
    }

    public void setMessagePreview(String messagePreview) {
        this.messagePreview = messagePreview;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSenderTeamId() {
        return senderTeamId;
    }

    public void setSenderTeamId(String senderTeamId) {
        this.senderTeamId = senderTeamId;
    }

    public String getMyTeamId() {
        return myTeamId;
    }

    public void setMyTeamId(String myTeamId) {
        this.myTeamId = myTeamId;
    }
}
