package com.emts.ugbattle.model;

import java.io.Serializable;

/**
 * Created by Prabin on 3/20/2018.
 */

public class TicketModel implements Serializable {
    private String ticketId;
    private String ticketQty;
    private String ticketPrice;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketQty() {
        return ticketQty;
    }

    public void setTicketQty(String ticketQty) {
        this.ticketQty = ticketQty;
    }

    public String getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(String ticketPrice) {
        this.ticketPrice = ticketPrice;
    }
}
