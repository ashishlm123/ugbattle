package com.emts.ugbattle.helper;

import android.support.design.widget.TabLayout;
import android.view.View;
import android.view.ViewGroup;

public class TabLayoutUtils {
    public static void enableTabs(TabLayout tabLayout, Boolean enable, int index) {
        ViewGroup viewGroup = getTabViewGroup(tabLayout, index);
        if (viewGroup != null)
            for (int childIndex = 0; childIndex < viewGroup.getChildCount(); childIndex++) {
                View tabView = viewGroup.getChildAt(childIndex);
                if (tabView != null)
                    tabView.setEnabled(enable);
            }
    }

    public static View getTabView(TabLayout tabLayout, int position) {
        View tabView = null;
        ViewGroup viewGroup = getTabViewGroup(tabLayout, position);
        if (viewGroup != null && viewGroup.getChildCount() > position)
            tabView = viewGroup.getChildAt(position);

        return tabView;
    }

    private static ViewGroup getTabViewGroup(TabLayout tabLayout, int index) {
        ViewGroup viewGroup = null;

        if (tabLayout != null && tabLayout.getChildCount() > 0) {
            View view = tabLayout.getChildAt(index);
            if (view != null && view instanceof ViewGroup)
                viewGroup = (ViewGroup) view;
        }
        return viewGroup;
    }


}
