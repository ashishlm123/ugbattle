package com.emts.ugbattle.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {

    private static Api api;

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }
    }

    public final String apiKey = "sdkfhiuews3549835";

    //**********************************************************************************************
    //**************************CHANGE FOR SERVER CHANGE********************************************
    //**********************************************************************************************
    //server node socket
    public final String nodeUrl = "http://13.229.126.174";
    public final String nodePort = "8888";
//    public final String nodeUrl = "http://202.166.198.151";
//    public final String nodePort = "3030";

    public final String successUrl = getHost() + "/ugbattle/public_html/home/success";
    public final String cancelUrl = getHost() + "/ugbattle/public_html/home/cancel";

    private String getHost() {
        return "http://13.229.126.174";
//        return "http://202.166.198.151:8888";
    }

    private String getApi() {
        return getHost() + "/ugbattle/public_html/app/"; //server api loc
//        return getHost() + "/ugbattle/app/"; //local api loc
    }
    //**********************************************************************************************
    //**************************CHANGE FOR SERVER CHANGE********************************************
    //**********************************************************************************************

    public final String siteSettingsApi = getApi() + "home/get_setting";
    //Login, Register and Forgot Password
    public final String loginUrl = getApi() + "users/login";
    public final String registerUrl = getApi() + "users/signup";
    public final String socialLoginUrl = getApi() + "users/social_login";
    public final String forgetPasswordUrl = getApi() + "users/forget_password";

    //Home Page
    public final String gameListingsUrl = getApi() + "game";
    public final String getBannerImage = getApi() + "home/get_banner_images";

    //AccountPage
    public final String getOrderListUrl = getApi() + "playeraccount/order";
    public final String getUserDetailUrl = getApi() + "playeraccount/get_user_profile";
    public final String updateProfileUrl = getApi() + "playeraccount/post_user_profile";
    public final String updateProfileImage = getApi() + "playeraccount/profile_image_upload";
    public final String getAllUserList = getApi() + "playeraccount/get_all_user_friend_list ";
    public final String sendFriendRequestUrl = getApi() + "playeraccount/user_friend_invitation";
    public final String getWithDrawal = getApi() + "playeraccount/my_with_drawal";
    public final String getWithDrawalHistoryUrl = getApi() + "playeraccount/withdraw_history";
    public final String withDrawalUsingCard = getApi() + "playeraccount/my_with_drawal_post";
    public final String getNotificationUrl = getApi() + "playeraccount/get_notification";
    public final String readNotificationUrl = getApi() + "playeraccount/set_notification_seen";
    public final String acceptFriendRequestUrl = getApi() + "playeraccount/accept_user_friend_invitation";
    public final String rejectFriendRequestUrl = getApi() + "playeraccount/reject_user_friend_invitation";

    //GameDetailPage
    public final String getGameServerLists = getApi() + "game/get_player_profile";
    public final String addGameUserIdUrl = getApi() + "game/save_game_profile";
    public final String tournamentListings = getApi() + "game/game_tournaments";
    public final String createJoinTeam = getApi() + "game/create_team_post";
    public final String showTeamName = getApi() + "game/show_team_name";
    public final String getTeamLists = getApi() + "playeraccount/get_team_info";
    public final String statGameUrl = getApi() + "playeraccount/start_the_game";
    public final String getRoundTeamListUrl = getApi() + "game/get_team_list";
    public final String deleteGameProfileUrl = getApi() + "game/delete_game_profile";

    //invite user to team
    public final String listUserToInviteToTeamUrl = getApi() + "playeraccount/get_friend_list";
    public final String inviteToTeamUrl = getApi() + "playeraccount/friend_invitation";
    public final String invitationListings = getApi() + "playeraccount/list_friend_invitation";
    public final String acceptInvitation = getApi() + "playeraccount/accept_friend_invitation";
    public final String rejectInvitation = getApi() + "playeraccount/reject_friend_invitation";
    public final String cancelTeamInvitation = getApi() + "playeraccount/cancel_friend_invitation";
    //team invitation to round
    public final String teamChallenge = getApi() + "game/match_invitation";
    public final String acceptTeamChallenge = getApi() + "game/accept_match_invitation";
    public final String rejectTeamChallenge = getApi() + "game/reject_match_invitation";
    //match started
    public final String getMatchInfoUrl = getApi() + "playeraccount/get_match_info";
    public final String getHistoryMatchInfoUrl = getApi() + "playeraccount/get_match_info_history";
    public final String uploadScreenshotApi = getApi() + "game/upload_team_image";
    public final String gameHistoryApi = getApi() + "game/game_history";

    //questions page
    public final String helpUrl = getApi() + "contact/post_help_center_message";
    public final String faqUrl = getApi() + "home/get_faq_datas";
    public final String cmsUrl = getApi() + "home/get_content";

    //market place
    public final String marketPlaceUrl = getApi() + "playeraccount/market_place";
    public final String ticketCheckOut = getApi() + "playeraccount/buy_ticket_checkout_post";


    //lobby
    public final String lobbyUrl = getApi() + "playeraccount/game_lobby";

    //device token update
    public final String upgradeDevicetoken = getApi() + "game/update_device_id";

    //all chat lists
    public final String getAllChats = getApi() + "playeraccount/get_message";
    public final String sendMessage = getApi() + "playeraccount/post_message";
    public final String getIndividualChat = getApi() + "game/load_individual_messages";
    public final String sendIndividualChat = getApi() + "game/send_individual_messages";
    public final String getMessageLists = getApi() + "game/get_message_list";
    public final String readMessageApi = getApi() + "game/set_message_seen";
    public final String getBadgeCountApi = getApi() + "game/count_unseen_message_and_notification";

    //leave team
    public final String leaveTeam = getApi() + "playeraccount/remove_from_the_team";

}