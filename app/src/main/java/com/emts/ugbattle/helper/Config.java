package com.emts.ugbattle.helper;

/**
 * Created by User on 2016-07-07.
 */
public class Config {
    public static String IS_REQUEST_RECEIVED = "requested_by_other_team";
    public static String IS_AWAITING = "awaiting";
    public static String IS_ACCEPTED = "challenge_accepted";
    public static String IS_REJECTED = "rejected";
    public static final String LEADER = "leader";
    public static final String Accepted = "accepted";
    public static final String STATUS_AWAITING = "awaiting";
    public static final String BANK = "1";
    public static final String PAYPAL = "2";
    public static final String PENDING = "0";
    public static final String APPROVED = "1";
    public static final String REJECTED = "2";
    public static final String FRIEND_REQUEST = "friend_inv_req";
    public static final String FRIEND_REQUEST_RESP = "friend_inv_resp";
    public static final String MATCH_CHALLENGE_REQUEST = "match_inv_req";
    public static final String MATCH_CHALLENGE_RESP = "match_inv_resp";
    public static final String MESSAGE_NOTI = "player_msg";
    public static final String MESSAGE_PLAYER_NOTI = "friend_msg";
    public static final String WIN_NOTIFICATION = "winner_selection_resp";
    public static final String PLAYER_INVITATION_REQ = "player_inv_req";
    public static final String PLAYER_INVITATION_RES = "player_inv_resp";
    public static final String WITHDRAWAL_RES = "withdrawal_resp";
    public static final String TICKET_PURCHASE_RES = "ticket_purchase_resp";

}
