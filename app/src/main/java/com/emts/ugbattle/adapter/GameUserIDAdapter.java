package com.emts.ugbattle.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.GameMdl;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prabin on 3/21/2018.
 */

public class GameUserIDAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<GameMdl> gameLists;
    private Context context;
    private RecyclerItemClickListener recyclerItemClickListener;

    public GameUserIDAdapter(Context context, ArrayList<GameMdl> gameLists) {
        this.context = context;
        this.gameLists = gameLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_game_user_id, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        GameMdl gameMdl = gameLists.get(position);

        viewHolder.gameName.setText("Game : " + gameMdl.getGameName());
        viewHolder.gameServer.setText("Game User ID : " + gameMdl.getGameUserId());
        viewHolder.gameUserId.setText("Server : " + gameMdl.getGameServersArray());

        if (!TextUtils.isEmpty(gameMdl.getGameImg())) {
            Picasso.with(context).load(gameMdl.getGameImg()).into(viewHolder.gameIcon);
        }

    }

    @Override
    public int getItemCount() {
        return gameLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView gameIcon, editInfo;
        Button deleteItem;
        TextView gameName, gameUserId, gameServer;

        ViewHolder(View itemView) {
            super(itemView);

            gameIcon = itemView.findViewById(R.id.game_icon);
            editInfo = itemView.findViewById(R.id.edit_info);
            deleteItem = itemView.findViewById(R.id.delete_item);

            gameName = itemView.findViewById(R.id.game_name);
            gameUserId = itemView.findViewById(R.id.game_user_id);
            gameServer = itemView.findViewById(R.id.game_server);

            deleteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteGameUserId(getLayoutPosition());
                }
            });

            editInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerItemClickListener.onItemClickLister(getLayoutPosition());
                }
            });
        }
    }

    private void deleteGameUserId(final int layoutPosition) {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(context, " Please wait..");
        progressDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("game_id", gameLists.get(layoutPosition).getGameId());
        postParam.put("game_user_id", gameLists.get(layoutPosition).getGameUserId());
        postParam.put("server_name", gameLists.get(layoutPosition).getGameServersArray());
        vHelper.addVolleyRequestListeners(Api.getInstance().deleteGameProfileUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        gameLists.remove(layoutPosition);
                        notifyDataSetChanged();
                        AlertUtils.showAlertMessage(context, "Delete Success", res.getString("message"));
                    } else {
                        AlertUtils.showAlertMessage(context, "Delete Error",
                                res.getString("message"));
                    }
                } catch (Exception e) {
                    Logger.e("updateProfileTask ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("deleteGameUserId error res", e.getMessage() + " ");
                }
                AlertUtils.showAlertMessage(context, "Error !!!", errorMsg);

            }
        }, "deleteGameUserId");
    }

    public interface RecyclerItemClickListener {
        void onItemClickLister(int position);
    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }
}
