package com.emts.ugbattle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.DateUtils;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.model.ChatModel;
import com.emts.ugbattle.service.DownloadService;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;

/**
 * Created by Prabin on 9/13/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ChatModel> chatList;
    private final int CHAT_MY_TEAM = 1;
    private final int CHAT_MY_IMG_FILE = 1;
    private final int CHAT_OPPONENT_TEAM = 2;
    private final int CHAT_TYPE_DATE = 3;

    private boolean isGroupChat;
    private PreferenceHelper preferenceHelper;


    public ChatAdapter(Context context, ArrayList<ChatModel> chatList) {
        this.context = context;
        this.chatList = chatList;
        preferenceHelper = PreferenceHelper.getInstance(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CHAT_MY_TEAM) {
            return new ViewHolderMe(LayoutInflater.from(context).inflate(R.layout.item_your_message, parent, false));
        } else if (viewType == CHAT_TYPE_DATE) {
            return new ViewHolderDate(LayoutInflater.from(context).inflate(R.layout.item_chat_date, parent, false));
        } else {
            return new ViewHolderFriends(LayoutInflater.from(context).inflate(R.layout.item_friends_message, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChatModel chatModel = chatList.get(position);
        int viewType = getItemViewType(position);
        if (viewType == CHAT_MY_TEAM) {
            ViewHolderMe holderMe = (ViewHolderMe) holder;
            holderMe.yourSentMessage.setText(StringEscapeUtils.unescapeJava(chatModel.getMessagePreview()));
            holderMe.tvUserName.setText(chatModel.getUserName());
            if (chatModel.getMessageType().equals(ChatModel.MESSAGE_TYPE_TEXT)) {
                holderMe.yourSentMessage.setVisibility(View.VISIBLE);
                holderMe.mySentImage.setVisibility(View.GONE);
                if (chatModel.getMsgStatus() == ChatModel.MESSAGE_SENDING) {
                    holderMe.progress.setVisibility(View.VISIBLE);
                    holderMe.errorImage.setVisibility(View.GONE);
                    holderMe.yourMessageTime.setVisibility(View.GONE);
                } else if (chatModel.getMsgStatus() == ChatModel.MESSAGE_SENT) {
                    holderMe.yourMessageTime.setText(DateUtils.getMessageSendTimeOnly(chatModel.getMessageTime()));
                    holderMe.progress.setVisibility(View.GONE);
                    holderMe.errorImage.setVisibility(View.GONE);
                    holderMe.yourMessageTime.setVisibility(View.VISIBLE);
                } else if (chatModel.getMsgStatus() == ChatModel.MESSAGE_FAILED) {
                    holderMe.progress.setVisibility(View.GONE);
                    holderMe.errorImage.setVisibility(View.VISIBLE);
                    holderMe.yourMessageTime.setVisibility(View.GONE);
                } else {
                    holderMe.progress.setVisibility(View.GONE);
                    holderMe.errorImage.setVisibility(View.GONE);
                }
            } else {
                holderMe.yourSentMessage.setVisibility(View.GONE);
                holderMe.mySentImage.setVisibility(View.VISIBLE);
                if (chatModel.getMsgStatus() == ChatModel.MESSAGE_SENDING) {
                    holderMe.progress.setVisibility(View.VISIBLE);
                    holderMe.errorImage.setVisibility(View.GONE);
                    holderMe.yourMessageTime.setVisibility(View.GONE);
                } else if (chatModel.getMsgStatus() == ChatModel.MESSAGE_SENT) {
                    holderMe.yourMessageTime.setText(DateUtils.getMessageSendTimeOnly(chatModel.getMessageTime()));
                    holderMe.progress.setVisibility(View.GONE);
                    holderMe.errorImage.setVisibility(View.GONE);
                    holderMe.yourMessageTime.setVisibility(View.VISIBLE);
                } else if (chatModel.getMsgStatus() == ChatModel.MESSAGE_FAILED) {
                    holderMe.progress.setVisibility(View.GONE);
                    holderMe.errorImage.setVisibility(View.VISIBLE);
                    holderMe.yourMessageTime.setVisibility(View.GONE);
                } else {
                    holderMe.progress.setVisibility(View.GONE);
                    holderMe.errorImage.setVisibility(View.GONE);
                }
            }

        } else if (viewType == CHAT_OPPONENT_TEAM) {
            ViewHolderFriends holderFriends = (ViewHolderFriends) holder;
            if (chatModel.getMessageType().equals(ChatModel.MESSAGE_TYPE_TEXT)) {
                holderFriends.friendsReceivedMessage.setVisibility(View.VISIBLE);
                holderFriends.friendsReceivedImg.setVisibility(View.GONE);
                holderFriends.friendsMessageTime.setText(DateUtils.getMessageSendTimeOnly(chatModel.getMessageTime()));
                holderFriends.friendsReceivedMessage.setText(StringEscapeUtils.unescapeJava(chatModel.getMessagePreview()));
                holderFriends.tvUsername.setText(chatModel.getUserName());
            } else {
                holderFriends.friendsReceivedMessage.setVisibility(View.GONE);
                holderFriends.friendsReceivedImg.setImageResource(R.drawable.ic_file_download_white_24dp);
                holderFriends.friendsReceivedImg.setColorFilter(R.color.com_facebook_blue);
                holderFriends.friendsReceivedImg.setVisibility(View.VISIBLE);
                holderFriends.friendsMessageTime.setText(DateUtils.getMessageSendTimeOnly(chatModel.getMessageTime()));
                holderFriends.friendsReceivedMessage.setText(StringEscapeUtils.unescapeJava(chatModel.getMessagePreview()));
                holderFriends.tvUsername.setText(chatModel.getUserName());
            }
        } else {
            ViewHolderDate holderDate = (ViewHolderDate) holder;
            holderDate.tvDate.setText(DateUtils.getMessageSendDateOnly(chatModel.getMessageTime()));
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        ChatModel chatModel = chatList.get(position);
        if (!isGroupChat) {
            if (chatModel.getSenderTeamId().equals(preferenceHelper.getUserId())) {
                return CHAT_MY_TEAM;
            } else {
                return CHAT_OPPONENT_TEAM;
            }
        }
        if (chatModel.getSenderTeamId().equals(chatModel.getMyTeamId())) {
            return CHAT_MY_TEAM;
        } else if (chatModel.getSenderTeamId().equals(ChatModel.rowTypeDate)) {
            return CHAT_TYPE_DATE;
        } else {
            return CHAT_OPPONENT_TEAM;
        }
    }

    public void setGroupChat(boolean groupChat) {
        isGroupChat = groupChat;
    }

    private class ViewHolderMe extends RecyclerView.ViewHolder {
        TextView yourMessageTime, yourSentMessage, tvUserName;
        ImageView errorImage, mySentImage;
        ProgressBar progress;

        ViewHolderMe(View itemView) {
            super(itemView);

            yourMessageTime = itemView.findViewById(R.id.your_message_time);
            yourSentMessage = itemView.findViewById(R.id.your_sent_message);
            tvUserName = itemView.findViewById(R.id.tv_yourteam_user_name);

            progress = itemView.findViewById(R.id.progress);
            errorImage = itemView.findViewById(R.id.error);
            mySentImage = itemView.findViewById(R.id.your_sent_image);
            mySentImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertUtils.showAlertMessage(context, "Download", "Are you sure you want to download this file?",
                            "Yes", "No", new AlertUtils.OnAlertButtonClickListener() {
                                @Override
                                public void onAlertButtonClick(boolean isPositiveButton) {
                                    if (isPositiveButton) {
                                        if (!preferenceHelper.getUserId().equals(chatList.get(getLayoutPosition()).getUserId())) {
                                            Logger.e("fileDirectory", chatList.get(getLayoutPosition()).getMessagePreview());
                                            Intent intent = new Intent(context, DownloadService.class);
                                            intent.putExtra("filePath", chatList.get(getLayoutPosition()).getMessagePreview());
                                            intent.putExtra("nId", chatList.get(getLayoutPosition()).getMessageId());
                                            ((Activity) context).startService(intent);
                                        }
                                    }
                                }
                            });
                }
            });
        }
    }

    private class ViewHolderFriends extends RecyclerView.ViewHolder {
        TextView friendsMessageTime, friendsReceivedMessage, tvUsername;
        ImageView friendsReceivedImg;

        ViewHolderFriends(View itemView) {
            super(itemView);

            tvUsername = itemView.findViewById(R.id.friends_name);
            friendsMessageTime = itemView.findViewById(R.id.friends_message_time);
            friendsReceivedMessage = itemView.findViewById(R.id.friends_received_message);

            friendsReceivedImg = itemView.findViewById(R.id.friends_sent_image);
            friendsReceivedImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertUtils.showAlertMessage(context, "Download", "Are you sure you want to download this file?",
                            "Yes", "No", new AlertUtils.OnAlertButtonClickListener() {
                                @Override
                                public void onAlertButtonClick(boolean isPositiveButton) {
                                    if (isPositiveButton) {
                                        Logger.e("fileDirectory", chatList.get(getLayoutPosition()).getMessagePreview());
                                        Intent intent = new Intent(context, DownloadService.class);
                                        intent.putExtra("filePath", chatList.get(getLayoutPosition()).getMessagePreview());
                                        intent.putExtra("nId", chatList.get(getLayoutPosition()).getMessageId());
                                        context.startService(intent);
                                    }
                                }
                            });
                }
            });
        }
    }

    private class ViewHolderDate extends RecyclerView.ViewHolder {
        TextView tvDate;

        ViewHolderDate(View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }


}
