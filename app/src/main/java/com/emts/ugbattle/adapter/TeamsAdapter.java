package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TeamModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TeamsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    PreferenceHelper helper = PreferenceHelper.getInstance(context);
    private ArrayList<TeamModel> teamLists;

    public TeamsAdapter(Context context, ArrayList<TeamModel> teamLists) {
        this.context = context;
        this.teamLists = teamLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_team, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        TeamModel teamModel = teamLists.get(position);

        viewHolder.tvTeamName.setText(teamModel.getOpponentTeamName());
        if (helper.getUserId().equals(teamModel.getMyTeamLeaderId())) {
//                && teamModel.getMyTeamId().equals(teamModel.getChallengingTeamId())) {
            if (Config.IS_REQUEST_RECEIVED.equals(teamModel.getTeamStatus())) {
                viewHolder.layAcceptRejectHolder.setVisibility(View.VISIBLE);
                viewHolder.btnChallenge.setVisibility(View.GONE);
                viewHolder.btnViewGame.setVisibility(View.GONE);
                viewHolder.tvAwaitingAndChallengeAccepted.setVisibility(View.GONE);

            } else if (Config.IS_ACCEPTED.equals(teamModel.getTeamStatus())) {
                viewHolder.btnViewGame.setVisibility(View.VISIBLE);
                viewHolder.layAcceptRejectHolder.setVisibility(View.GONE);
                viewHolder.btnChallenge.setVisibility(View.GONE);
                viewHolder.tvAwaitingAndChallengeAccepted.setVisibility(View.GONE);

            } else if (Config.IS_AWAITING.equals(teamModel.getTeamStatus())) {
                viewHolder.tvAwaitingAndChallengeAccepted.setText("Awaiting");
                viewHolder.tvAwaitingAndChallengeAccepted.setVisibility(View.VISIBLE);
                viewHolder.btnViewGame.setVisibility(View.GONE);
                viewHolder.layAcceptRejectHolder.setVisibility(View.GONE);
                viewHolder.btnChallenge.setVisibility(View.GONE);

            } else if (Config.IS_REJECTED.equals(teamModel.getTeamStatus())) {
                viewHolder.tvAwaitingAndChallengeAccepted.setText("Challenge Rejected");
                viewHolder.tvAwaitingAndChallengeAccepted.setVisibility(View.VISIBLE);
                viewHolder.btnViewGame.setVisibility(View.GONE);
                viewHolder.layAcceptRejectHolder.setVisibility(View.GONE);
                viewHolder.btnChallenge.setVisibility(View.GONE);

            } else {
                viewHolder.btnChallenge.setVisibility(View.VISIBLE);
                viewHolder.tvAwaitingAndChallengeAccepted.setVisibility(View.GONE);
                viewHolder.btnViewGame.setVisibility(View.GONE);
                viewHolder.layAcceptRejectHolder.setVisibility(View.GONE);
            }
        } else {
            viewHolder.btnChallenge.setVisibility(View.GONE);
            viewHolder.tvAwaitingAndChallengeAccepted.setVisibility(View.GONE);
            if (teamModel.getTeamStatus().equalsIgnoreCase(Config.IS_ACCEPTED)) {
                viewHolder.btnViewGame.setVisibility(View.VISIBLE);
            } else {
                viewHolder.btnViewGame.setVisibility(View.GONE);
            }
            viewHolder.layAcceptRejectHolder.setVisibility(View.GONE);
        }

        viewHolder.tvTeamName.setText(teamModel.getOpponentTeamName());
    }

    @Override
    public int getItemCount() {
        return teamLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName;

        LinearLayout layAcceptRejectHolder;
        Button btnAccept, btnReject;

        Button btnChallenge;
        Button btnViewGame;

        TextView tvAwaitingAndChallengeAccepted;


        public ViewHolder(final View itemView) {
            super(itemView);
            layAcceptRejectHolder = itemView.findViewById(R.id.lay_accept_reject_holder);

            tvTeamName = itemView.findViewById(R.id.tv_team_name);

            btnAccept = itemView.findViewById(R.id.btn_accept);
            btnReject = itemView.findViewById(R.id.btn_reject);

            btnChallenge = itemView.findViewById(R.id.btn_challenge);
            btnChallenge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(context)) {
                        challengeTeamTask(getLayoutPosition(), itemView);
                    } else {
                        AlertUtils.showAlertMessage(context, "Error", context.getString(R.string.error_no_internet));
                    }
                }
            });

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(context)) {
                        acceptTeamChallengeTask(getLayoutPosition(), itemView);
                    } else {
                        AlertUtils.showAlertMessage(context, "Error", context.getString(R.string.error_no_internet));
                    }
                }
            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(context)) {
                        rejectTeamChallengeTask(getLayoutPosition(), itemView);
                    } else {
                        AlertUtils.showAlertMessage(context, "Error", context.getString(R.string.error_no_internet));
                    }
                }
            });

            btnViewGame = itemView.findViewById(R.id.btn_view_game);
            btnViewGame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.onRecyclerViewItemClick(view, getLayoutPosition());
                    }
                }
            });

            tvAwaitingAndChallengeAccepted = itemView.findViewById(R.id.tv_awaiting_and_challenge_accepted);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        //Argument position ko thau ma chai team id nai pathako chha sidhai
                        clickListener.onRecyclerViewItemClick(view, Integer.parseInt(teamLists.get(getLayoutPosition()).getOpponentTeamId()));
                    }
                }
            });
        }
    }

    private void challengeTeamTask(final int pos, final View view) {
        AlertUtils.showAlertMessage(context, "Challenge Team !!!", context.getString(R.string.are_you_sure) + " challenge this team?", "Yes",
                "No", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            VolleyHelper vHelper = VolleyHelper.getInstance(context);
                            HashMap<String, String> postMap = vHelper.getPostParams();

                            postMap.put("game_id", teamLists.get(pos).getGameId());
                            postMap.put("tourn_id", teamLists.get(pos).getTrnmntId());
                            postMap.put("team_id", teamLists.get(pos).getMyTeamId());
                            postMap.put("creater_id", teamLists.get(pos).getMyTeamLeaderId());
                            postMap.put("creater_id_of_other_team", teamLists.get(pos).getOpponentTeamLeaderUserId());
                            postMap.put("other_team_id", teamLists.get(pos).getOpponentTeamId());

                            vHelper.addVolleyRequestListeners(Api.getInstance().teamChallenge,
                                    Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
                                        @Override
                                        public void onSuccess(String response) {
                                            try {
                                                JSONObject res = new JSONObject(response);
                                                if (res.getBoolean("status")) {
                                                    teamLists.get(pos).setTeamStatus(Config.IS_AWAITING);
                                                    notifyItemChanged(pos);
                                                } else {
                                                    AlertUtils.showAlertMessage(context, "Challenge Error !!!", res.getString("message"));
                                                }
                                            } catch (JSONException e) {
                                                Logger.e("challengeTeamTask json ex:", e.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onError(String errorResponse, VolleyError volleyError) {
                                            String errorMsg = "Unexpected error !!! Please try again with internet access.";
                                            try {
                                                JSONObject errorObj = new JSONObject(errorResponse);
                                                errorMsg = errorObj.getString("message");
                                            } catch (Exception e) {
                                                Logger.e("challengeTeamTask error res", e.getMessage() + " ");
                                            }
                                            AlertUtils.showSnack(context, view, errorMsg);
                                        }
                                    }, "challengeTeamTask");
                        }
                    }
                });
    }

    private void rejectTeamChallengeTask(final int pos, final View view) {
        AlertUtils.showAlertMessage(context, "Reject Challenge Invitation?",
                context.getString(R.string.are_you_sure) + " reject this team?", "Yes",
                "No", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            VolleyHelper vHelper = VolleyHelper.getInstance(context);
                            HashMap<String, String> postMap = vHelper.getPostParams();

                            postMap.put("game_id", teamLists.get(pos).getGameId());
                            postMap.put("tourn_id", teamLists.get(pos).getTrnmntId());
                            postMap.put("team_id", teamLists.get(pos).getMyTeamId());
                            postMap.put("creater_id", teamLists.get(pos).getMyTeamLeaderId());
                            postMap.put("creater_id_of_other_team", teamLists.get(pos).getOpponentTeamLeaderUserId());
                            postMap.put("other_team_id", teamLists.get(pos).getOpponentTeamId());

                            vHelper.addVolleyRequestListeners(Api.getInstance().rejectTeamChallenge,
                                    Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
                                        @Override
                                        public void onSuccess(String response) {
                                            try {
                                                JSONObject res = new JSONObject(response);
                                                if (res.getBoolean("status")) {
                                                    teamLists.get(pos).setTeamStatus(Config.IS_REJECTED);
                                                    notifyItemChanged(pos);
                                                }
                                            } catch (JSONException e) {
                                                Logger.e("rejectTeamChallengeTask json ex: ", e.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onError(String errorResponse, VolleyError volleyError) {
                                            String errorMsg = "Unexpected error !!! Please try again with internet access.";
                                            try {
                                                JSONObject errorObj = new JSONObject(errorResponse);
                                                errorMsg = errorObj.getString("message");
                                            } catch (Exception e) {
                                                Logger.e("rejectTeamChallengeTask error res", e.getMessage() + " ");
                                            }
                                            AlertUtils.showSnack(context, view, errorMsg);
                                        }
                                    }, "rejectTeamChallengeTask");
                        }
                    }
                });
    }

    private void acceptTeamChallengeTask(final int pos, final View view) {
        AlertUtils.showAlertMessage(context, "Accept Challenge?", context.getString(R.string.are_you_sure)
                        + " accept this team?", "Yes",
                "No", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            VolleyHelper vHelper = VolleyHelper.getInstance(context);
                            HashMap<String, String> postMap = vHelper.getPostParams();

                            postMap.put("game_id", teamLists.get(pos).getGameId());
                            postMap.put("tourn_id", teamLists.get(pos).getTrnmntId());
                            postMap.put("team_id", teamLists.get(pos).getMyTeamId());
                            postMap.put("creater_id", teamLists.get(pos).getMyTeamLeaderId());
                            postMap.put("creater_id_of_other_team", teamLists.get(pos).getOpponentTeamLeaderUserId());
                            postMap.put("other_team_id", teamLists.get(pos).getOpponentTeamId());

                            vHelper.addVolleyRequestListeners(Api.getInstance().acceptTeamChallenge,
                                    Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
                                        @Override
                                        public void onSuccess(String response) {
                                            try {
                                                JSONObject res = new JSONObject(response);
                                                if (res.getBoolean("status")) {
                                                    TeamModel teamModel = teamLists.get(pos);
                                                    teamModel.setTeamStatus(Config.IS_ACCEPTED);
                                                    teamLists.clear();
                                                    teamLists.add(teamModel);
                                                    notifyDataSetChanged();
                                                } else {
                                                    AlertUtils.showAlertMessage(context, "Error !!!", res.getString("message"));
                                                }
                                            } catch (JSONException e) {
                                                Logger.e("acceptTeamChallengeTask json ex: ", e.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onError(String errorResponse, VolleyError volleyError) {
                                            String errorMsg = "Unexpected error !!! Please try again with internet access.";
                                            try {
                                                JSONObject errorObj = new JSONObject(errorResponse);
                                                errorMsg = errorObj.getString("message");
                                            } catch (Exception e) {
                                                Logger.e("acceptTeamChallengeTask error res", e.getMessage() + " ");
                                            }
                                            AlertUtils.showSnack(context, view, errorMsg);
                                        }
                                    }, "acceptTeamChallengeTask");
                        }
                    }
                });
    }

    private RecyclerViewItemClickListener clickListener;

    public interface RecyclerViewItemClickListener {
        void onRecyclerViewItemClick(View view, int position);
    }

    public void setClickListener(RecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public static boolean checkNull(String string) {
        return (string.equalsIgnoreCase("null") || TextUtils.isEmpty(string));
    }
}
