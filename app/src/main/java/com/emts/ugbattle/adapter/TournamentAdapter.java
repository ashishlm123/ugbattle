package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.model.TournamentModel;

import java.util.ArrayList;

/**
 * Created by Prabin on 3/23/2018.
 */

public class TournamentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<TournamentModel> tournamentLists;
    private Context context;

    private boolean isInfoAdded, joinedInAnyOneTournament;
    private RecyclerItemClickListener recyclerItemClickListener;

    public TournamentAdapter(Context context, ArrayList<TournamentModel> tournamentLists) {
        this.context = context;
        this.tournamentLists = tournamentLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_tournament_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        TournamentModel tournamentModel = tournamentLists.get(position);

        viewHolder.tvTrnmntNameSrvrPlayerQty.setText(tournamentModel.getTrnmntName());
        viewHolder.tvRequiredTeams.setText(tournamentModel.getNumOfTeamsReady());
        viewHolder.tvTicketPerPersonQty.setText(tournamentModel.getTcktsPerPerson() + " " + " tickets");
        viewHolder.tvNumOfTeamsEnterd.setText(tournamentModel.getNumOfTeamsEntered());
        viewHolder.tvTotalRounds.setText(String.valueOf(tournamentModel.getTotalRounds()));
        viewHolder.tvTicketPrizePerPerson.setText(tournamentModel.getPrizePerPerson() + " " + " tickets");

        if (!isInfoAdded || joinedInAnyOneTournament) {
            if (!tournamentModel.getTeamId().equalsIgnoreCase("0")) {
                viewHolder.btnJoin.setEnabled(true);
                viewHolder.btnJoin.setBackgroundResource(R.drawable.rounded_blue);
            } else {
                viewHolder.btnJoin.setEnabled(false);
                viewHolder.btnJoin.setBackgroundResource(R.drawable.rounded_grey);
            }
        } else {
            viewHolder.btnJoin.setEnabled(true);
            viewHolder.btnJoin.setBackgroundResource(R.drawable.rounded_blue);
        }

        if (!tournamentModel.getTeamId().equals("0")) {
            viewHolder.btnJoin.setText("View");
        } else {
            viewHolder.btnJoin.setText("Join");
        }
    }

    @Override
    public int getItemCount() {
        return tournamentLists.size();
    }

    public void setInfoAdded(boolean isInfoAdded) {
        this.isInfoAdded = isInfoAdded;
    }

    public void setJoinedInAnyOneTournament(boolean joinedInAnyOneTournament) {
        this.joinedInAnyOneTournament = joinedInAnyOneTournament;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTrnmntNameSrvrPlayerQty, tvRequiredTeams, tvTicketPerPersonQty,
                tvNumOfTeamsEnterd, tvTotalRounds, tvTicketPrizePerPerson;
        Button btnJoin;

        private ViewHolder(View itemView) {
            super(itemView);

            tvTrnmntNameSrvrPlayerQty = itemView.findViewById(R.id.tv_trnmnt_name_srvr_player_qty);
            tvRequiredTeams = itemView.findViewById(R.id.tv_required_teams);
            tvTicketPerPersonQty = itemView.findViewById(R.id.tv_ticket_per_person_qty);
            tvNumOfTeamsEnterd = itemView.findViewById(R.id.tv_number_of_teams);
            tvTotalRounds = itemView.findViewById(R.id.tv_total_rounds);
            tvTicketPrizePerPerson = itemView.findViewById(R.id.tv_ticket_prize_per_person);

            btnJoin = itemView.findViewById(R.id.btn_join_trnmnt);
            btnJoin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerItemClickListener.onItemClickLister(getLayoutPosition());
                }
            });
        }
    }

    public interface RecyclerItemClickListener {
        void onItemClickLister(int position);
    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


}
