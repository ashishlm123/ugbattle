package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.DateUtils;
import com.emts.ugbattle.model.OrderModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Prabin on 3/20/2018.
 */

public class OrderListsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<OrderModel> topUpOrderLists;

    public OrderListsAdapter(Context context, ArrayList<OrderModel> topUpOrderLists) {
        this.context = context;
        this.topUpOrderLists = topUpOrderLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_topup_order_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        OrderModel orderModel = topUpOrderLists.get(position);

        viewHolder.orderSuccessMessage.setText(orderModel.getOrderMessage());
        viewHolder.orderDate.setText(orderModel.getOrderDate());
//        DateFormat timeFormat = SimpleDateFormat.getTimeInstance();
        if (orderModel.getOrderTime() != null || orderModel.getOrderTime().equals("null")) {
            viewHolder.orderTime.setText(DateUtils.getTime(orderModel.getOrderTime()));
        }
    }

    @Override
    public int getItemCount() {
        return topUpOrderLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView orderSuccessMessage, orderDate, orderTime;

        private ViewHolder(View itemView) {
            super(itemView);

            orderSuccessMessage = itemView.findViewById(R.id.order_success_message);
            orderDate = itemView.findViewById(R.id.order_date);
            orderTime = itemView.findViewById(R.id.order_time);
        }
    }
}
