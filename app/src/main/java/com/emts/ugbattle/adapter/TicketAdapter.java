package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.RecyclerItemClickListener;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.model.TicketModel;

import java.util.ArrayList;

/**
 * Created by Prabin on 3/20/2018.
 */

public class TicketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<TicketModel> ticketLists;
    private RecyclerItemClickListener clickListener;
    PreferenceHelper prefsHelper;

    public void setOnRecyclerItemClickListener(RecyclerItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public TicketAdapter(Context context, ArrayList<TicketModel> ticketLists) {
        this.context = context;
        this.ticketLists = ticketLists;
        prefsHelper = PreferenceHelper.getInstance(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_tickets, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        TicketModel ticketModel = ticketLists.get(position);

        viewHolder.tvQuantity.setText(ticketModel.getTicketQty() + " Tickets");
        viewHolder.tvPrice.setText(prefsHelper.getString(PreferenceHelper.DEF_CURRENCY_CODE, "USD")
                + " " + prefsHelper.getString(PreferenceHelper.DEF_CURRENCY_SIGN, "$") + " " +
                ticketModel.getTicketPrice());
    }

    @Override
    public int getItemCount() {
        return ticketLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvQuantity, tvPrice, buy;

        ViewHolder(View itemView) {
            super(itemView);

            tvQuantity = itemView.findViewById(R.id.ticket_qty);
            tvPrice = itemView.findViewById(R.id.total_ticket_price);

            buy = itemView.findViewById(R.id.tv_buy);
            buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.onRecyclerItemClicked(getLayoutPosition());
                    }
                }
            });
        }
    }
}
