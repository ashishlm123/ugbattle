package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.ugbattle.ParentActivityInterface;
import com.emts.ugbattle.R;
import com.emts.ugbattle.model.GameMdl;
import com.emts.ugbattle.model.TeamModel;

import java.util.ArrayList;

public class GameChallengeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<GameMdl> gameChallengeLists;
    private ArrayList<TeamModel> myTeamLists;

    public GameChallengeAdapter(Context context, ArrayList<GameMdl> gameChallengeLists) {
        this.context = context;
        this.gameChallengeLists = gameChallengeLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_game_lobby, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        GameMdl gameMdl = gameChallengeLists.get(position);
        viewHolder.tvGameName.setText(gameMdl.getGameName() + ": ");
        viewHolder.tvMyTeamName.setText(myTeamLists.get(position).getMyTeamName());
        ArrayList<TeamModel> teamLists = gameMdl.getTeamLists();
        if (teamLists.size() == 0) {
            viewHolder.teamListings.setVisibility(View.GONE);
            viewHolder.tvErrorText.setText("No activity in this game.");
            viewHolder.tvErrorText.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tvRoundNo.setText("Round " + teamLists.get(0).getCurrentRoundNo());

            viewHolder.tvErrorText.setVisibility(View.GONE);
            TeamsAdapter teamsAdapter = new TeamsAdapter(context, gameMdl.getTeamLists());
            setRecyclerClickListener(teamsAdapter, position);
            viewHolder.teamListings.setAdapter(teamsAdapter);
            viewHolder.teamListings.setVisibility(View.VISIBLE);

        }


    }

    @Override
    public int getItemCount() {
        return gameChallengeLists.size();
    }

    public void setMyTeamLists(ArrayList<TeamModel> myTeamLists) {
        this.myTeamLists = myTeamLists;
    }


    private class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layNameRoundHolder;
        TextView tvGameName, tvRoundNo, tvErrorText, tvMyTeamName;
        Button btnViewMyTeam;

        EditText filterTeams;

        RecyclerView teamListings;

        public ViewHolder(View itemView) {
            super(itemView);

            layNameRoundHolder = itemView.findViewById(R.id.lay_name_round_holder);
            tvGameName = itemView.findViewById(R.id.tv_game_name);
            tvRoundNo = itemView.findViewById(R.id.tv_round_name);

            tvMyTeamName = itemView.findViewById(R.id.my_team_name);
            btnViewMyTeam = itemView.findViewById(R.id.btn_view_team);
            btnViewMyTeam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.TEAM_LISTS_FRAGMENTS_PAGE,
                            myTeamLists.get(getLayoutPosition()).getTeamId());

                }
            });

            tvErrorText = itemView.findViewById(R.id.error_text);

            filterTeams = itemView.findViewById(R.id.edt_filter_teams);
            filterTeams.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (TextUtils.isEmpty(charSequence)) {
                        TeamsAdapter teamsAdapter = new TeamsAdapter(context, gameChallengeLists.get(getLayoutPosition()).getTeamLists());
                        setRecyclerClickListener(teamsAdapter, getLayoutPosition());
                        teamListings.setAdapter(teamsAdapter);
                    } else {
                        ArrayList<TeamModel> allTeamLists = gameChallengeLists.get(getLayoutPosition()).getTeamLists();
                        ArrayList<TeamModel> filterLists = new ArrayList<>();
                        for (int j = 0; j < allTeamLists.size(); j++) {
                            if (allTeamLists.get(j).getOpponentTeamName().startsWith(charSequence.toString())) {
                                filterLists.add(allTeamLists.get(j));
                            }
                        }
                        TeamsAdapter teamsAdapter = new TeamsAdapter(context, filterLists);
                        setRecyclerClickListener(teamsAdapter, getLayoutPosition());
                        teamListings.setAdapter(teamsAdapter);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            teamListings = itemView.findViewById(R.id.team_listings);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            teamListings.setLayoutManager(linearLayoutManager);
            teamListings.setNestedScrollingEnabled(false);
        }
    }

    public void setRecyclerClickListener(TeamsAdapter teamsAdapter, final int pos) {
        teamsAdapter.setClickListener(new TeamsAdapter.RecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClick(View view, int position) {
                if (view.getId() == R.id.team_holder) {
                    openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.TEAM_LISTS_FRAGMENTS_PAGE, position);
                } else {
                    openSpecificFragment.openSpecificPageWithData(ParentActivityInterface.ONGOING_CHALLENGE_PAGE, gameChallengeLists.get(pos));
                }
            }
        });
    }

    private ParentActivityInterface openSpecificFragment;

    public void setOpenSpecificFragment(ParentActivityInterface openSpecificFragment) {
        this.openSpecificFragment = openSpecificFragment;
    }

}
