package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Prabin on 3/26/2018.
 */

public class PlayerListsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<UserModel> playerLists;
    private RecyclerItemClickListener recyclerItemClickListener;
    private TeamModel team;
    private PreferenceHelper prefsHelper;
    private boolean isGameStarted = false;

    public void setGameStarted(boolean isGameStarted) {
        this.isGameStarted = isGameStarted;
    }

    public void setTeam(TeamModel team) {
        this.team = team;
    }

    public PlayerListsAdapter(Context context, ArrayList<UserModel> playerLists) {
        this.context = context;
        this.playerLists = playerLists;
        prefsHelper = PreferenceHelper.getInstance(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_player_lists, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        UserModel playerModel = playerLists.get(position);

        viewHolder.tvUserName.setText(playerModel.getUserName());
        viewHolder.tvGameUserId.setText(playerModel.getGameUserId());

        String status = playerModel.getUserMemberStatus().substring(0, 1).toUpperCase() + playerModel.getUserMemberStatus().substring(1);
        viewHolder.tvStatus.setText(status);

        viewHolder.imgSwipe.setVisibility(View.GONE);

        if (playerModel.getUserMemberStatus().equalsIgnoreCase(Config.LEADER)) {
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.yellow));
            viewHolder.imgStatus.setImageResource(R.drawable.icon_accepted);
            viewHolder.imgStatus.setEnabled(false);
        } else {
            if (prefsHelper.getUserId().equals(playerModel.getUserId())) {
                if (recyclerItemClickListener == null){
                    viewHolder.imgSwipe.setVisibility(View.GONE);
                }else {
                    viewHolder.imgSwipe.setVisibility(View.VISIBLE);
                }
            }
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.white));
            if (playerModel.getUserMemberStatus().equals(Config.Accepted)) {
                viewHolder.imgStatus.setImageResource(R.drawable.btn_accepted);
            } else {
                if (team != null) {
                    if (prefsHelper.getUserId().equals(team.getMyTeamLeaderId())) {
                        viewHolder.imgStatus.setImageResource(R.drawable.btn_cancel);
                        viewHolder.imgStatus.setEnabled(true);
                    } else {
                        viewHolder.imgStatus.setImageDrawable(null);
                        viewHolder.imgStatus.setEnabled(false);
                    }
                }
            }
        }
        Picasso.with(context).load(playerModel.getUserProfilePic()).into(viewHolder.tvProfileIcon);
    }

    @Override
    public int getItemCount() {
        return playerLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView tvProfileIcon, imgStatus, imgSwipe;
        TextView tvUserName, tvGameUserId, tvStatus;

        private ViewHolder(View itemView) {
            super(itemView);
            tvProfileIcon = itemView.findViewById(R.id.profile_icon);
            tvUserName = itemView.findViewById(R.id.user_name);
            tvGameUserId = itemView.findViewById(R.id.game_user_id);
            tvStatus = itemView.findViewById(R.id.tv_status);
            imgStatus = itemView.findViewById(R.id.img_status);
            imgSwipe = itemView.findViewById(R.id.imgSwipe);

            imgStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!playerLists.get(getLayoutPosition()).getUserMemberStatus().equals(Config.Accepted)
                            && recyclerItemClickListener != null) {
                        recyclerItemClickListener.onItemClickLister(getLayoutPosition());
                    }
                }
            });

            if (isGameStarted) {
                imgStatus.setVisibility(View.GONE);
            }
        }
    }

    public interface RecyclerItemClickListener {
        void onItemClickLister(int position);
    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }
}
