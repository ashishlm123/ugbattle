package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.model.InvitationModel;

import java.util.ArrayList;

/**
 * Created by Prabin on 3/23/2018.
 */

public class InvitationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<InvitationModel> invitationLists;
    private RecyclerItemClickListener recyclerItemClickListener;

    public InvitationAdapter(Context context, ArrayList<InvitationModel> invitationLists) {
        this.context = context;
        this.invitationLists = invitationLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_invitation, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        InvitationModel invitationModel = invitationLists.get(position);

        viewHolder.tvInvitesBy.setText(invitationModel.getInviteBy().getUserName());
        viewHolder.tvTeamName.setText(invitationModel.getInviteToTeam().getMyTeamName());
        viewHolder.tvRequiredTickets.setText(invitationModel.getInviteToTeam().getTcktsPerPerson());
        viewHolder.tvPlayersSoFar.setText(invitationModel.getInviteToTeam().getNumOfTeamsEntered());

//        if(invitationModel.getInvitationStatus().equalsIgnoreCase(InvitationModel.INVITE_ACCEPTED)){
//            viewHolder.tvAccept.setVisibility(View.VISIBLE);
//            viewHolder.btnAccept.setVisibility(View.GONE);
//            viewHolder.btnReject.setText("Leave");
//        }

        if (invitationModel.getInviteToTeam().getTeamStatus().equalsIgnoreCase(InvitationModel.INVITE_ACCEPTED)) {
            viewHolder.tvAccept.setVisibility(View.VISIBLE);
            viewHolder.btnAccept.setVisibility(View.GONE);
            viewHolder.btnReject.setText("Leave");
        } else {
            viewHolder.tvAccept.setVisibility(View.GONE);
            viewHolder.btnAccept.setVisibility(View.VISIBLE);
            viewHolder.btnReject.setText("Reject");
        }
    }

    @Override
    public int getItemCount() {
        return invitationLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvInvitesBy, tvTeamName, tvRequiredTickets, tvPlayersSoFar, tvMaxPlayers;
        TextView tvAccept;
        Button btnAccept, btnReject;

        private ViewHolder(View itemView) {
            super(itemView);

            tvInvitesBy = itemView.findViewById(R.id.user_name);
            tvTeamName = itemView.findViewById(R.id.team_name);
            tvRequiredTickets = itemView.findViewById(R.id.tv_required_tckts);
            tvPlayersSoFar = itemView.findViewById(R.id.participated_player_qty);
            tvMaxPlayers = itemView.findViewById(R.id.allowed_player_qty);

            tvAccept = itemView.findViewById(R.id.tv_accept);
            btnAccept = itemView.findViewById(R.id.btn_accept_invtn);
            btnReject = itemView.findViewById(R.id.btn_reject_invtn);

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerItemClickListener.onItemClickLister(getLayoutPosition(), view);
//                    btnAccept.setVisibility(View.GONE);
//                    tvAccept.setVisibility(View.VISIBLE);
                }
            });

            tvAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerItemClickListener.onItemClickLister(getLayoutPosition(), view);
                }
            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        recyclerItemClickListener.onItemClickLister(getLayoutPosition(), view);
                }
            });

        }
    }

    public interface RecyclerItemClickListener {
        void onItemClickLister(int position, View view);
    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }

}
