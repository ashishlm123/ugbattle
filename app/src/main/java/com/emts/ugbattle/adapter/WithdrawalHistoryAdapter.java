package com.emts.ugbattle.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.model.OrderModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WithdrawalHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<OrderModel> withDrawalList;
    Context context;

    public WithdrawalHistoryAdapter(ArrayList<OrderModel> withDrawalList, Context context) {
        this.withDrawalList = withDrawalList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_withdrawal_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        OrderModel orderModel = withDrawalList.get(position);

        viewHolder.tvDate.setText(orderModel.getOrderDate());
        if (orderModel.getPaymentMode().equals(Config.PAYPAL)) {
            viewHolder.tvWithdrawalTO.setText("WithDrawal to: Paypal");
        } else if (orderModel.getPaymentMode().equals(Config.BANK)) {
            viewHolder.tvWithdrawalTO.setText("WithDrawal to: Bank");
        }

        switch (orderModel.getStatus()) {
            case Config.PENDING:
                viewHolder.tvStatus.setText("Status: Pending");
                break;
            case Config.APPROVED:
                viewHolder.tvStatus.setText("Status: Approved");
                break;
            case Config.REJECTED:
                viewHolder.tvStatus.setText("Status: Rejected");
                break;
        }

        int numberOfTickets = (Integer.parseInt(orderModel.getOrderAmount()) / Integer.parseInt(orderModel.getOneTicketCost()));

        viewHolder.tvAmount.setText("Amount: " + String.valueOf(numberOfTickets) +
                " Tickets(USD " + orderModel.getOrderAmount() + ")");

    }

    @Override
    public int getItemCount() {
        return withDrawalList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate, tvAmount, tvWithdrawalTO, tvStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvAmount = itemView.findViewById(R.id.tv_amount);
            tvWithdrawalTO = itemView.findViewById(R.id.tv_to);
            tvStatus = itemView.findViewById(R.id.tv_status);

        }
    }
}
