package com.emts.ugbattle.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.ChatActivity;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.TeamModel;
import com.emts.ugbattle.model.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prabin on 3/20/2018.
 */

public class FriendsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<UserModel> friendsLists;
    private boolean isInviteToTeam;
    private TeamModel team;
    private int invitationCount;

    public void setTeam(TeamModel team) {
        this.team = team;
    }

    public void setInvitationCount(int invitationCount) {
        this.invitationCount = invitationCount;
    }

    public FriendsListAdapter(Context context, ArrayList<UserModel> friendsLists, boolean isInviteToTeam) {
        this.context = context;
        this.friendsLists = friendsLists;
        this.isInviteToTeam = isInviteToTeam;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_friends, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        UserModel friendsModel = friendsLists.get(position);

        if (!isInviteToTeam) {
            if (!friendsModel.isFriend()) {
                viewHolder.btnInvite.setText("Add Friend");
                if (!friendsModel.getUserMemberStatus().equals("null")) {
                    viewHolder.inviteStatus.setVisibility(View.VISIBLE);
                    viewHolder.inviteStatus.setText(friendsModel.getUserMemberStatus());
                    viewHolder.btnInvite.setVisibility(View.GONE);
                } else {
                    viewHolder.inviteStatus.setVisibility(View.GONE);
                    viewHolder.btnInvite.setVisibility(View.VISIBLE);
                }
            } else {
                viewHolder.btnInvite.setText("Chat");
                viewHolder.btnInvite.setVisibility(View.VISIBLE);
            }
        } else {
            viewHolder.progressInvite.setVisibility(View.GONE);
            viewHolder.btnInvite.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(friendsModel.getUserName())) {
            viewHolder.friendsName.setText(friendsModel.getUserFullName());
        } else {
            viewHolder.friendsName.setText(friendsModel.getUserName());
        }
        Picasso.with(context).load(friendsModel.getUserProfilePic()).into(viewHolder.friendsProfilePic);
    }

    @Override
    public int getItemCount() {
        return friendsLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView friendsProfilePic;
        TextView friendsName;
        Button btnInvite;
        TextView inviteStatus;
        ProgressBar progressInvite;

        public ViewHolder(View itemView) {
            super(itemView);

            progressInvite = itemView.findViewById(R.id.progress_invite);
            friendsProfilePic = itemView.findViewById(R.id.profile_icon);
            friendsName = itemView.findViewById(R.id.user_name);
            btnInvite = itemView.findViewById(R.id.btn_invite_friend);
            if (!isInviteToTeam) {
                btnInvite.setText("Add Friend");
            } else {
                btnInvite.setVisibility(View.VISIBLE);
            }
            inviteStatus = itemView.findViewById(R.id.invite_status);

            btnInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (btnInvite.getText().toString().equalsIgnoreCase("chat")) {
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra("friend", friendsLists.get(getLayoutPosition()));
                        context.startActivity(intent);
                        return;
                    }
                    if (!isInviteToTeam) {
                        sendFriendRequestTask(getLayoutPosition(), btnInvite, inviteStatus);
                    } else {
                        if (team != null) {
                            if (invitationCount >= 11) {
                                AlertUtils.showAlertMessage(context, " Error Message", "Maximum people to be invite is 10 and you have already send 10 invitation", "Ok", "", null);
                            } else {
                                inviteToTeamTask(getLayoutPosition(), ViewHolder.this);
                            }
                        }
                    }
                }
            });
        }
    }

    private void inviteToTeamTask(final int position, final ViewHolder viewHolder) {
        viewHolder.progressInvite.setVisibility(View.VISIBLE);
        viewHolder.btnInvite.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("friend_user_id", friendsLists.get(position).getUserId());
        postParam.put("team_id", team.getTeamId());
        postParam.put("tourn_id", team.getTrnmntId());
        postParam.put("game_user_id", team.getMyGameUserId());
        postParam.put("game_id", team.getGameId());
//        postParam.put("")

//        game_user_id(compulsary) ->leader id ? QWQW
//        player_game_user_id(compulsary) ->game user id of the friend "


        vHelper.addVolleyRequestListeners(Api.getInstance().inviteToTeamUrl, Request.Method.POST,
                postParam, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
//                                friendsLists.remove(position);
//                                notifyItemRemoved(position);
                                if (updatePlayer != null) {
                                    updatePlayer.updatePlayer(position, UpdatePlayer.INVITE);
                                }
                                invitationCount = invitationCount + 1;
//                                viewHolder.btnInvite.setVisibility(View.GONE);
//                                viewHolder.progressInvite.setVisibility(View.GONE);
                            } else {
                                viewHolder.btnInvite.setVisibility(View.VISIBLE);
                                viewHolder.progressInvite.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            Logger.e("inviteToTeamTask error res", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("inviteToTeamTask error res", e.getMessage() + " ");
                        }
                        viewHolder.btnInvite.setVisibility(View.VISIBLE);
                        viewHolder.progressInvite.setVisibility(View.GONE);
                    }
                }, "inviteToTeamTask");
    }

    private void sendFriendRequestTask(final int position, final Button btnInvite,
                                       final TextView inviteStatus) {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(context, "Please wait...");
        progressDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("friend_id", friendsLists.get(position).getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().sendFriendRequestUrl, Request.Method.POST, postParam,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                if (updatePlayer != null) {
                                    updatePlayer.updatePlayer(position, UpdatePlayer.ADD_FRIEND);
                                }
//                                inviteStatus.setVisibility(View.VISIBLE);
//                                inviteStatus.setText("Pending");
//                                btnInvite.setVisibility(View.GONE);
                            } else {
                                AlertUtils.showAlertMessage(context, "Error Message", res.getString("message"));
                            }
                        } catch (Exception e) {
                            Logger.e("sendFriendRequestTask error res", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("sendFriendRequestTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAlertMessage(context, "Error Message", errorMsg);

                    }
                }, "sendFriendRequestTask");
    }

    public interface UpdatePlayer {
        int INVITE = 1;
        int ADD_FRIEND = 2;

        void updatePlayer(int position, int type);
    }

    private UpdatePlayer updatePlayer;

    public void setUpdatePlayerCallBack(UpdatePlayer updatePlayer) {
        this.updatePlayer = updatePlayer;
    }
}
