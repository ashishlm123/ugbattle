package com.emts.ugbattle.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.fragment.GameDetailFragment;
import com.emts.ugbattle.helper.RoundedCornersTransformation;
import com.emts.ugbattle.model.GameMdl;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 2018-03-19.
 */

public class GameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<GameMdl> gameList;
    private LayoutInflater inflater;
    private boolean isGrid;
    private RecyclerItemClickListener recyclerItemClickListener;

    public void setGrid(boolean grid) {
        isGrid = grid;
    }

    public GameAdapter(Context context, ArrayList<GameMdl> gameList) {
        this.context = context;
        this.gameList = gameList;

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isGrid) {
            return new ViewHolder(inflater.inflate(R.layout.item_game_grid, parent, false));
        } else {
            return new ViewHolder(inflater.inflate(R.layout.item_game_list, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        GameMdl gameMdl = gameList.get(position);

        if (!TextUtils.isEmpty(gameMdl.getGameImg())) {
            Picasso.with(context).load(gameMdl.getGameImg()).transform(new RoundedCornersTransformation())
                    .into(viewHolder.imgGame);
        }
        viewHolder.tvGameName.setText(gameMdl.getGameName());

        if (!isGrid) {
            if (TextUtils.isEmpty(gameMdl.getGameUserId()) || gameMdl.getGameUserId().equals("null")) {
                viewHolder.btnAddGameUserId.setVisibility(View.VISIBLE);
            } else {
                viewHolder.btnAddGameUserId.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return gameList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgGame;
        TextView tvGameName;
        Button btnAddGameUserId;

        ViewHolder(View itemView) {
            super(itemView);

            imgGame = itemView.findViewById(R.id.gameImage);
            tvGameName = itemView.findViewById(R.id.gameName);
            btnAddGameUserId = itemView.findViewById(R.id.btnAddUserId);
            if (btnAddGameUserId != null) {
                btnAddGameUserId.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        recyclerItemClickListener.onItemClickLister(getLayoutPosition());
                    }
                });
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerItemClickListener.onItemClickLister(getLayoutPosition());
                }
            });
        }
    }

    public interface RecyclerItemClickListener {
        void onItemClickLister(int position);
    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }
}
