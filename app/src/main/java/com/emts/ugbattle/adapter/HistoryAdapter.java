package com.emts.ugbattle.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.DateUtils;
import com.emts.ugbattle.model.HistoryMdl;

import java.util.ArrayList;

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<HistoryMdl> historyList;

    public HistoryAdapter(Context context, ArrayList<HistoryMdl> historyList) {
        this.context = context;
        this.historyList = historyList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_topup_order_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        HistoryMdl historyMdl = historyList.get(position);

        if (historyMdl.getResult().equalsIgnoreCase(HistoryMdl.STATUS_WIN)) {
            viewHolder.winLooseHistoryTxt.setText("You win, victorious");
        } else {
            viewHolder.winLooseHistoryTxt.setText("You loose, defeated");
        }

        viewHolder.winLooseDate.setText(DateUtils.convertDate1(historyMdl.getCompletedDate()));
        viewHolder.winLooseTime.setText(DateUtils.getTime(historyMdl.getCompletedDate()));
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView winLooseHistoryTxt, winLooseDate, winLooseTime;

        private ViewHolder(View itemView) {
            super(itemView);

            winLooseHistoryTxt = itemView.findViewById(R.id.order_success_message);
            winLooseDate = itemView.findViewById(R.id.order_date);
            winLooseTime = itemView.findViewById(R.id.order_time);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerItemClickListener.onItemClickLister(getLayoutPosition(), view);
                }
            });
        }
    }

    private RecyclerItemClickListener recyclerItemClickListener;

    public interface RecyclerItemClickListener {
        void onItemClickLister(int position, View view);
    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }
}
