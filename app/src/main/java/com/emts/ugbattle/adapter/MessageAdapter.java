package com.emts.ugbattle.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.ugbattle.R;
import com.emts.ugbattle.activity.ChatActivity;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Config;
import com.emts.ugbattle.helper.DateUtils;
import com.emts.ugbattle.helper.VolleyHelper;
import com.emts.ugbattle.model.ChatModel;
import com.emts.ugbattle.model.MessageModel;
import com.emts.ugbattle.model.UserModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Prabin on 3/20/2018.
 */

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<MessageModel> conversationLists;
    private RecyclerItemClickListener recyclerItemClickListener;
    private boolean isMessageFragment;

    public MessageAdapter(Context context, ArrayList<MessageModel> conversationLists) {
        this.context = context;
        this.conversationLists = conversationLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        MessageModel messageModel = conversationLists.get(position);

        if (!messageModel.isRead()) {
            viewHolder.messageHolder.setBackgroundColor(context.getResources().getColor(R.color.notreadcolor));
            viewHolder.userName.setTypeface(viewHolder.userName.getTypeface(), Typeface.BOLD);
            viewHolder.messagePreview.setTypeface(viewHolder.messagePreview.getTypeface(), Typeface.BOLD);
        } else {
            viewHolder.userName.setTypeface(viewHolder.userName.getTypeface(), Typeface.NORMAL);
            viewHolder.messageHolder.setBackgroundColor((context.getResources().getColor(R.color.isreadcolor)));
        }

        //date time
        if (isMessageFragment) {
            if (android.text.format.DateUtils.isToday(DateUtils.getDate(messageModel.getMessageTime()).getTime())) {
                viewHolder.messageDate.setText("Today");
            } else {
                viewHolder.messageDate.setText(DateUtils.convertDate1(messageModel.getMessageTime()));
            }
        } else {
            viewHolder.messageDate.setText(DateUtils.convertDate1(messageModel.getMessageTime()));
        }
        viewHolder.messageTime.setText(DateUtils.getTime(messageModel.getMessageTime()));


        if (isMessageFragment) {
            viewHolder.userName.setText(messageModel.getUserName());
            viewHolder.messagePreview.setText(messageModel.getMessagePreview());
//            viewHolder.messageTime.setText(DateUtils.getTime(messageModel.getMessageTime()));
            if (!TextUtils.isEmpty(messageModel.getUserProfilePic())) {
                Glide.with(context).load(messageModel.getUserProfilePic()).into(viewHolder.profileIcon);
            }
        } else {
            viewHolder.userName.setText(messageModel.getMesgTitle());
            viewHolder.messagePreview.setText(messageModel.getMessagePreview());
            switch (messageModel.getMessageType()) {
                case Config.FRIEND_REQUEST:
                case Config.FRIEND_REQUEST_RESP:
                    Glide.with(context).load(messageModel.getUserProfilePic()).into(viewHolder.profileIcon);
                    break;
                case Config.WITHDRAWAL_RES:
                    viewHolder.profileIcon.setImageResource(R.drawable.icon_withdrawal);
                    break;
                case Config.PLAYER_INVITATION_REQ:
                    viewHolder.profileIcon.setImageResource(R.drawable.icon_email_1);
                    break;
                case Config.PLAYER_INVITATION_RES:
                    viewHolder.profileIcon.setImageResource(R.drawable.icon_open_email);
                    break;
                case Config.MATCH_CHALLENGE_REQUEST:
                    viewHolder.profileIcon.setImageResource(R.drawable.ic_challenge);
                    break;
                case Config.MATCH_CHALLENGE_RESP:
                    viewHolder.profileIcon.setImageResource(R.drawable.icon_accepted_massage);
                    break;
                case Config.MESSAGE_NOTI:
                case Config.MESSAGE_PLAYER_NOTI:
                    if (messageModel.getGameImg().equals("null") || TextUtils.isEmpty(messageModel.getGameImg())) {
                        viewHolder.profileIcon.setImageResource(R.drawable.ic_notifications_none_black_24dp);
                    } else {
                        Glide.with(context).load(messageModel.getGameImg()).into(viewHolder.profileIcon);
                    }
                    break;
                case Config.TICKET_PURCHASE_RES:
                    viewHolder.profileIcon.setImageResource(R.drawable.btn_ticket1);
                    break;


            }
        }


    }

    @Override
    public int getItemCount() {
        return conversationLists.size();
    }

    public void setMessageFragment(boolean messageFragment) {
        isMessageFragment = messageFragment;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout messageHolder;
        ImageView profileIcon;
        TextView userName, messagePreview, messageTime, messageDate, viewMore;

        private ViewHolder(View itemView) {
            super(itemView);

            messageHolder = itemView.findViewById(R.id.message_holder);
            profileIcon = itemView.findViewById(R.id.profile_icon);
            userName = itemView.findViewById(R.id.user_name);
            messagePreview = itemView.findViewById(R.id.message_preview);
            messageTime = itemView.findViewById(R.id.message_time);
            messageDate = itemView.findViewById(R.id.message_date);
            viewMore = itemView.findViewById(R.id.view_more);

            if (isMessageFragment) {
                viewMore.setVisibility(View.GONE);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        recyclerItemClickListener.onItemClickLister(getLayoutPosition());
                    }
                });

            } else {
                viewMore.setVisibility(View.VISIBLE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        recyclerItemClickListener.onItemClickLister(getLayoutPosition());
                    }
                });
            }
        }
    }

    public interface RecyclerItemClickListener {
        void onItemClickLister(int position);

    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }
}
