package com.emts.ugbattle;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Api;
import com.emts.ugbattle.helper.Logger;
import com.emts.ugbattle.helper.NetworkUtils;
import com.emts.ugbattle.helper.PreferenceHelper;
import com.emts.ugbattle.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SiteSettingsTask {
    public static boolean isDialogShow = false;

    public static void getSiteSettings(Context context, VersionUpdateCallback callback) {
        if (NetworkUtils.isInNetwork(context)) {
            siteSettingTask(context, callback);
        }
    }

    private static void siteSettingTask(final Context context, final VersionUpdateCallback callback) {
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().siteSettingsApi, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                PreferenceHelper prefHelper = PreferenceHelper.getInstance(context);
                                prefHelper.edit().putString(PreferenceHelper.NODE_SEVER, res.getString("node_host")).apply();
                                prefHelper.edit().putString(PreferenceHelper.NODE_PORT, res.getString("node_port")).apply();
                                prefHelper.edit().putString(PreferenceHelper.DEF_CURRENCY_CODE, res.getString("default_currency_code")).apply();
                                prefHelper.edit().putString(PreferenceHelper.DEF_CURRENCY_SIGN, res.getString("default_currency_sign")).apply();


                                int versionCode = BuildConfig.VERSION_CODE;
                                String versionName = BuildConfig.VERSION_NAME;
                                String appVersion = res.getString("android_version");
                                Logger.e("VERSION app:api", versionName + " :: " + appVersion);
                                if (!versionName.trim().equals(appVersion.trim())) {
                                    callback.onVersionUpdated(true);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("siteSettingApi json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("siteSettingApi error res", e.getMessage() + " ");
                        }
                        Logger.e("siteSettingApi error ex", errorMsg);
                    }
                }, "siteSettingApi");
    }

    public interface VersionUpdateCallback {
        void onVersionUpdated(boolean isVersionUpdated);
    }

    public static void showVersionUpdateDialog(final Context context) {
        if (isDialogShow) return;
        AlertUtils.showAlertMessage(context, false, "New App Update",
                "There is a new version of UGBattle available in Play Store. Do you want to download it now??",
                "OK", "Cancel", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    }
                });
        isDialogShow = true;
    }
}
