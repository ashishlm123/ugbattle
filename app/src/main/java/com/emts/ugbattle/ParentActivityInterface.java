package com.emts.ugbattle;

/**
 * Created by Prabin on 3/20/2018.
 */

public interface ParentActivityInterface {
    int GAME_DETAIL_PAGE_ID = 123;
    int GAME_DETAIL_INVITATION_PAGE_ID = 169;
    int START_GAME_FRAGMENT_ID = 134;
    int MARKET_PLACE_PAGE_ID = 145;
    int ROUND_START_FRAG_ID = 165;
    int ONGOING_CHALLENGE_PAGE = 185;
    int WITHDRAWAL_HISTORY_ID = 194;
    int ORDER_PAGE_ID = 214;
    int FRIEND_LIST_FRAG_ID = 256;
    int ONGOING_OR_HISTORY_PAGE = 195;
    int TEAM_LISTS_FRAGMENTS_PAGE = 292;

    void openSpecificPage(int id);

    void openSpecificPageWithData(int id, Object obj);

    void goBack();

    void setTitle(String title);

    void setBadge(boolean isMessage, int count);
}
