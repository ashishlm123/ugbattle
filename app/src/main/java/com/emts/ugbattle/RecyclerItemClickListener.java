package com.emts.ugbattle;

public interface RecyclerItemClickListener {
    void onRecyclerItemClicked(int position);
}
