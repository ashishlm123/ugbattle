package com.emts.ugbattle.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.emts.ugbattle.R;
import com.emts.ugbattle.helper.AlertUtils;
import com.emts.ugbattle.helper.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadService extends IntentService {
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    public DownloadService() {
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Logger.e("Service Started", "yes");
        String notificationId = "";
        String downloadPath = "";
        if (intent != null) {
            notificationId = intent.getStringExtra("nId");
            downloadPath = intent.getStringExtra("filePath");
        }


        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this);
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(
                getApplicationContext()).setSmallIcon(R.drawable.ic_file_download_white_24dp)
                .setContentTitle("Downloading File");

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;

        try {
            URL downloadUrl = new URL(downloadPath);
            connection = (HttpURLConnection) downloadUrl.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                if (connection.getResponseCode() == 404) {
                    AlertUtils.showToast(getApplicationContext(), "File not found");
                } else {
                    AlertUtils.showToast(getApplicationContext(), "Unknown Error Occured");
                }
                return;
            }

            int fileLength = connection.getContentLength();

            String fileName = "";
            fileName = downloadPath.substring(downloadPath.lastIndexOf("/") + 1);
            input = connection.getInputStream();
            File file = new File(Environment.getExternalStorageDirectory() + "/UgBattle/");
            if (!file.exists()) {
                if (file.mkdirs()) {
                    file = new File(file.getPath() + "/" + fileName);
                    if (!file.exists()) {
                        if (!file.createNewFile()) {
                            Logger.e("file", "File not created");
                            return;
                        }
                    }
                }
            } else {
                file = new File(file.getPath() + "/" + fileName);
                if (!file.exists()) {
                    if (!file.createNewFile()) {
                        Logger.e("file", "File not created");
                        return;
                    }
                }
            }

            output = new FileOutputStream(file);
            byte data[] = new byte[1024];
            long total = 0;
            int count;

            while ((count = input.read(data)) != -1) {
                total += count;
                if (fileLength > 0) {
                    mBuilder.setProgress(100, (int) (total * 100 / fileLength), false);
                    mNotifyManager.notify(0, mBuilder.build());
                }
//                output.write(data, 0, count);
                output.write(data);

            }
            Intent notifyIntent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                    + "/UgBattle/");
            notifyIntent.setDataAndType(uri, "*/*");
            notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(DownloadService.this, 0, notifyIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setContentTitle("Download Complete.");
            mBuilder.setProgress(0, 0, false);
            mBuilder.setContentText("Touch here to open file directory.");
            mNotifyManager.notify(0, mBuilder.build());

        } catch (Exception e) {
            Logger.e("download exception", e.toString());
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
                Logger.e("ignored exception", ignored.toString());
            }

            if (connection != null)
                connection.disconnect();
        }
    }
}

